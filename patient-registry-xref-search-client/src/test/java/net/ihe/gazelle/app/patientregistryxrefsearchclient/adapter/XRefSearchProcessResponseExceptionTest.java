package net.ihe.gazelle.app.patientregistryxrefsearchclient.adapter;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class XRefSearchProcessResponseExceptionTest {
    /**
     * Dummy Test for coverage
     *
     * @throws XRefSearchProcessResponseException testException
     */
    @Test
     void generateException() {
        assertThrows(XRefSearchProcessResponseException.class, () -> {
            throw new XRefSearchProcessResponseException("Test");
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws XRefSearchProcessResponseException testException
     */
    @Test
     void generateException1() {
        assertThrows(XRefSearchProcessResponseException.class, () -> {
            throw new XRefSearchProcessResponseException("Test", new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws XRefSearchProcessResponseException testException
     */
    @Test
     void generateException2() {
        assertThrows(XRefSearchProcessResponseException.class, () -> {
            throw new XRefSearchProcessResponseException(new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws XRefSearchProcessResponseException testException
     */
    @Test
     void generateException4() {
        assertThrows(XRefSearchProcessResponseException.class, () -> {
            throw new XRefSearchProcessResponseException("Test", new IllegalArgumentException("test"), false, true);
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws XRefSearchProcessResponseException testException
     */
    @Test
     void generateException3() {
        assertThrows(XRefSearchProcessResponseException.class, () -> {
            throw new XRefSearchProcessResponseException();
        });
    }

}