package net.ihe.gazelle.app.patientregistrysearchclient.adapter;

import com.gitb.core.AnyContent;
import com.gitb.ps.ProcessRequest;
import com.gitb.ps.ProcessResponse;
import com.gitb.ps.ProcessingService;
import com.gitb.tr.BAR;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import net.ihe.gazelle.app.patientregistryapi.adapter.ws.PatientProcessingWebserviceConstants;
import net.ihe.gazelle.app.patientregistryapi.application.PatientSearchService;
import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.lib.gitbprocessingclient.adapter.GITBClientProcessImpl;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperAnyContentToObject;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperObjectToAnyContent;
import net.ihe.gazelle.lib.gitbutils.adapter.MappingException;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static net.ihe.gazelle.app.patientregistryapi.adapter.ws.PatientProcessingWebserviceConstants.PATIENT_PROCESSING_SERVICE;
import static net.ihe.gazelle.app.patientregistryapi.adapter.ws.PatientProcessingWebserviceConstants.PATIENT_PROCESSING_SERVICE_PORT;

/**
 * PatientFeed Client to be used by any application that wants to feed a patient to a remote Patient Registry.
 * Given an url, this client will be able to feed the remote registry using GITB Process Service while exposing the
 * PatientFeedService interface.
 *
 * @author wbars
 */
public class PatientSearchClient implements PatientSearchService {

    private ProcessingService processingClient;

    /**
     * Package-private constructor used for test purposes.
     *
     * @param processingService : processing service to be used by the PatientSearchClient.
     */
    public PatientSearchClient(ProcessingService processingService) {
        this.processingClient = processingService;
    }

    /**
     * Default constructor for the class. The Client will create the GITB Processing Service client based on the URL.
     *
     * @param processingServiceURL : URL of the remote Processing Service.
     */
    public PatientSearchClient(URL processingServiceURL) {
        this.processingClient = new GITBClientProcessImpl(processingServiceURL, PATIENT_PROCESSING_SERVICE, PATIENT_PROCESSING_SERVICE_PORT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Patient> search(SearchCriteria searchCriteria) throws SearchException {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_SEARCH_OPERATION);
        try {
            AnyContent searchCriteriaAnyContent = new MapperObjectToAnyContent()
                    .getAnyContent(PatientProcessingWebserviceConstants.PATIENT_SEARCH_CRITERIA_INPUT_NAME, searchCriteria);
            processRequest.getInput().add(searchCriteriaAnyContent);
            ProcessResponse processResponse = processingClient.process(processRequest);
            return extractPatientsFromProcessResponse(processResponse);
        } catch (MappingException e) {
            throw new SearchException("Exception while Mapping with GITB elements !", e);
        } catch (PatientSearchProcessResponseException e) {
            throw new SearchException("Invalid Response from distant PatientFeedProcessingService !", e);
        } catch (UnsupportedOperationException e) {
            throw new SearchException("Invalid operation used on distant PatientFeedProcessingService !", e);
        } catch (IllegalArgumentException e) {
            throw new SearchException("Invalid Request sent to distant PatientFeedProcessingService !", e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Patient> parametrizedSearch(List<SearchParameter> parameters) throws SearchException {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_PARAMETERS_SEARCH_OPERATION);
        try {
            for(SearchParameter searchParameter : parameters){
                AnyContent searchParameterAnyContent = new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_PARAMETERS_SEARCH_INPUT_NAME, searchParameter);
           processRequest.getInput().add(searchParameterAnyContent);
            }
            ProcessResponse processResponse = processingClient.process(processRequest);
            return extractPatientsFromProcessResponse(processResponse);
        } catch (MappingException e) {
            throw new SearchException("Exception while Mapping with GITB elements !", e);
        } catch (PatientSearchProcessResponseException e) {
            throw new SearchException("Invalid Response from distant PatientFeedProcessingService !", e);
        } catch (UnsupportedOperationException e) {
            throw new SearchException("Invalid operation used on distant PatientFeedProcessingService !", e);
        } catch (IllegalArgumentException e) {
            throw new SearchException("Invalid Request sent to distant PatientFeedProcessingService !", e);
        }
    }


    /**
     * Extract the list of Patients from the ProcessResponse received from the remote ProcessingService.
     *
     * @param processResponse : response received.
     * @return list of {@link Patient} returned by the ProcessingService.
     * @throws PatientSearchProcessResponseException : if the ProcessResponse is not valid for a PatientSearch request.
     * @throws SearchException                       : if the Patient Search raised an exception in the remote Repository.
     */
    private List<Patient> extractPatientsFromProcessResponse(ProcessResponse processResponse) throws PatientSearchProcessResponseException,
            SearchException {
        if (processResponse == null) {
            throw new PatientSearchProcessResponseException("Empty Response from the distant PatientFeedProcessingService !");
        } else {
            if (processResponse.getReport() != null) {
                if (TestResultType.SUCCESS.equals(processResponse.getReport().getResult())) {
                    return extractPatientListFromOutputs(processResponse);
                } else if (TestResultType.FAILURE.equals(processResponse.getReport().getResult())) {
                    throw createExceptionFromProcessResponseReport(processResponse.getReport());
                } else {
                    throw new PatientSearchProcessResponseException(String.format("Processing response with unexpected type %s ! Expected type is " +
                            "either %s or %s.", processResponse.getReport().getResult(), TestResultType.SUCCESS, TestResultType.FAILURE));
                }
            } else {
                throw new PatientSearchProcessResponseException("Response from the distant PatientFeedProcessingService shall contain a report with" +
                        " " +
                        "the success status of the operation !");
            }
        }
    }

    private List<Patient> extractPatientListFromOutputs(ProcessResponse processResponse) throws PatientSearchProcessResponseException {
        if (processResponse.getOutput().size() == 1) {
            try {
                List<Patient> outputPatients = new MapperAnyContentToObject().getObjectCollection(processResponse.getOutput().get(0), List.class,
                        Patient.class);
                return outputPatients != null ? outputPatients : new ArrayList<>();
            } catch (MappingException e) {
                throw new PatientSearchProcessResponseException("Error while mapping processing output from distant PatientFeedProcessingService " +
                        "to Patient List !");
            }
        } else {
            throw new PatientSearchProcessResponseException("Response from the distant PatientFeedProcessingService shall contain a single " +
                    "output for Patient List !");
        }
    }

    /**
     * Extract PatientFeedException from ProcessResponse report.
     *
     * @param report : report found in the received response.
     * @return {@link SearchException} to be thrown by the {@link PatientSearchClient}
     * @throws PatientSearchProcessResponseException : if the report is not well formed.
     */
    private SearchException createExceptionFromProcessResponseReport(TAR report) throws PatientSearchProcessResponseException {
        if (report.getReports() == null || report.getReports().getInfoOrWarningOrError().size() != 1) {
            throw new PatientSearchProcessResponseException("The report from the ProcessResponse shall not be null and shall contain a single error" +
                    ".");
        } else {
            try {
                BAR error = (BAR) report.getReports().getInfoOrWarningOrError().get(0).getValue();
                if (error.getDescription() != null && !error.getDescription().isEmpty()) {
                    return new SearchException(error.getDescription());
                } else {
                    throw new PatientSearchProcessResponseException("Error from ProcessResponse report must have a valid description !");
                }
            } catch (ClassCastException e) {
                throw new PatientSearchProcessResponseException("Cannot decode error from ProcessResponse report !");
            }
        }
    }
}
