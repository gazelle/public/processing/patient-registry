Feature: Search Patient information

  Background:
    Given the following patients exist
      | tmpuuid  | gender | active | dateOfBirth                  | dateOfDeath                  | multipleBirthOrder |
      | tmpuuid  | MALE   | true   | 1984-03-12T17:25:04.017+0100 | 2020-03-12T17:25:04.017+0100 | 134                |
      | tmpuuid1 | FEMALE | true   | 1984-03-12T17:25:04.017+0100 | 2020-03-12T17:25:04.017+0100 | 134                |
      | tmpuuid2 | FEMALE | true   | 2019-01-19T17:25:04.017+0100 | 2020-08-28T17:25:04.017+0100 | 134                |
      | tmpuuid3 | MALE   | false  | 1984-09-29T17:25:04.017+0100 | 2020-03-12T17:15:04.017+0100 | 134                |
    And patients have the following identifiers
      | tmpuuid  | systemIdentifier | value       | systemName | type |
      | tmpuuid  | urn:oid:1.2.3.4  | identifier0 | system0    | PI   |
      | tmpuuid  | urn:oid:5.6.7.8  | identifier0 | system2    | PI   |
      | tmpuuid1 | urn:oid:1.2.3.4  | identifier1 | system0    | PI   |
      | tmpuuid2 | urn:oid:1.2.3.4  | identifier2 | system0    | PI   |
      | tmpuuid3 | urn:oid:1.2.3.5  | identifier3 | system3    | PI   |
      | tmpuuid3 | urn:oid:9.8.7.6  | identifier0 | system4    | PI   |
    And patient is fed with provisional uuid "tmpuuid"
    And patient is fed with provisional uuid "tmpuuid1"
    And patient is fed with provisional uuid "tmpuuid2"
    And patient is fed with provisional uuid "tmpuuid3"

  Scenario Outline: Nominal Patient search with one criterion
    Given search criterion "<parameter>" "<verb>" "<value>"
    When search is done
    Then response is "OK"
    And received <resultSize> patients
    And all received patients "<parameter>" "<verb>" "<value>"

    Examples: Simple cases
      | parameter                   | verb | value           | resultSize |
      | tmpuuid                     | is   | tmpuuid1        | 1          |
      | uuid                        | is   | uuid123         | 0          |
      | identifier.systemIdentifier | is   | urn:oid:1.2.3.4 | 3          |
      | identifier.value            | is   | identifier0     | 2          |


  #Multiple And criteria (nominal, multiple and, member, child member)
  Scenario: Nominal patient search with AND criteria
    Given search criteria for and conditions
      | parameter                   | verb | value           |
      | tmpuuid                     | is   | tmpuuid         |
      | identifier.systemIdentifier | is   | urn:oid:1.2.3.4 |
      | identifier.value            | is   | identifier0     |
    When search is done
    Then response is "OK"
    And received 1 patients
    And all received patients verify all
      | parameter                   | verb | value           |
      | tmpuuid                     | is   | tmpuuid         |
      | identifier.systemIdentifier | is   | urn:oid:1.2.3.4 |
      | identifier.value            | is   | identifier0     |

  Scenario: Patient search with AND criteria no match
    Given search criteria for and conditions
      | parameter                   | verb | value            |
      | tmpuuid                     | is   | tmpuuid          |
      | identifier.systemIdentifier | is   | urn:oid:1.2.3.12 |
      | identifier.value            | is   | identifier47     |
    When search is done
    Then response is "OK"
    And received 0 patients

  #Multiple OR criteria (nominal, multiple OR, member, child member)
  Scenario: Patient search with 2 matching criteria OR
    Given search criteria for or conditions
      | parameter        | verb | value       |
      | tmpuuid          | is   | tmpuuid     |
      | identifier.value | is   | identifier0 |
    When search is done
    Then response is "OK"
    And received 2 patients
    And all received patients verify one
      | parameter        | verb | value       |
      | tmpuuid          | is   | tmpuuid     |
      | identifier.value | is   | identifier0 |


  Scenario: Patient search with 2 matching criteria OR no result
    Given search criteria for or conditions
      | parameter        | verb | value  |
      | identifier.value | is   | Tarte  |
      | uuid             | is   | uuid42 |
    When search is done
    Then response is "OK"
    But received 0 patients

  #Multiple OR criteria in AND (nominal, multiple OR, member, child member)
  Scenario: Patient search with OR Criterion in AND criteria
    Given root criteria is "and"
    And search criteria for or conditions
      | parameter | verb | value    |
      | tmpuuid   | is   | tmpuuid  |
      | tmpuuid   | is   | tmpuuid1 |
    And  search criteria for or conditions
      | parameter        | verb | value        |
      | identifier.value | is   | identifier0  |
      | identifier.value | is   | identifier18 |
    When search is done
    Then response is "OK"
    And received 1 patients
    And all received patients verify one
      | parameter | verb | value    |
      | tmpuuid   | is   | tmpuuid  |
      | tmpuuid   | is   | tmpuuid1 |
    And all received patients verify one
      | parameter        | verb | value        |
      | identifier.value | is   | identifier0  |
      | identifier.value | is   | identifier18 |

  #Multiple AND criteria in OR (nominal, multiple OR, member, child member)
  Scenario: Patient search with AND Criterion in OR Criteria
    Given root criteria is "or"
    And search criteria for and conditions
      | parameter        | verb | value       |
      | tmpuuid          | is   | tmpuuid     |
      | identifier.value | is   | identifier0 |
    And  search criteria for and conditions
      | parameter        | verb | value       |
      | tmpuuid          | is   | tmpuuid1    |
      | identifier.value | is   | identifier1 |
    When search is done
    Then response is "OK"
    And received 2 patients
    And some patient verifies all
      | parameter        | verb | value       |
      | tmpuuid          | is   | tmpuuid     |
      | identifier.value | is   | identifier0 |
    And some patient verifies all
      | parameter        | verb | value       |
      | tmpuuid          | is   | tmpuuid1    |
      | identifier.value | is   | identifier1 |
    And all return patients match criteria