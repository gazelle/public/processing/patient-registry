package net.ihe.gazelle.app.patientregistryservice.adapter.ws;

import com.gitb.core.AnyContent;
import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.ps.ProcessRequest;
import com.gitb.ps.ProcessResponse;
import com.gitb.tr.BAR;
import com.gitb.tr.TestResultType;
import net.ihe.gazelle.app.patientregistryapi.adapter.ws.PatientProcessingWebserviceConstants;
import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import net.ihe.gazelle.app.patientregistryservice.application.TestPatientSearchServiceImpl;
import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperObjectToAnyContent;
import net.ihe.gazelle.lib.gitbutils.adapter.MappingException;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link PatientSearchProcessingService}
 */
public class PatientSearchProcessingServiceTest {

    private PatientSearchProcessingService patientSearchProcessingService;
    private TestPatientSearchServiceImpl testPatientSearchService;

    /**
     * Initialize implementations to be tested before each test.
     */
    @BeforeEach
    public void setUp() {
        this.testPatientSearchService = new TestPatientSearchServiceImpl();
        this.patientSearchProcessingService = new PatientSearchProcessingService(testPatientSearchService);
    }

    /**
     * Test Processing a patient search, nominal case.
     */
    @Test
    @Covers(requirements = {"PATREG-50", "PATREG-56", "PATREG-57", "PATREG-59"})
    public void process() {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_SEARCH_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_SEARCH_CRITERIA_INPUT_NAME,
                    new SearchCriteria()));
        } catch (MappingException e) {
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        ProcessResponse processResponse = patientSearchProcessingService.processPatientSearch(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
        assertEquals(1, processResponse.getOutput().size(), "Response shall contain a unique output which is the value of the" +
                " UUID assigned to the fed patient.");
        assertEquals("list", processResponse.getOutput().get(0).getType(), "Returned UUID shall be stored in a string GITB AnyContent");
        assertEquals(PatientProcessingWebserviceConstants.PATIENT_LIST_OUTPUT_NAME, processResponse.getOutput().get(0).getName(), "Name of returned GITB AnyContent shall be 'uuid'");
        assertEquals(ValueEmbeddingEnumeration.BASE_64, processResponse.getOutput().get(0).getEmbeddingMethod(), "Embedding method of returned " +
                "GITB AnyContent shall be 'STRING'");
        assertNotNull(processResponse.getReport(), "ProcessResponse.report shall not be null !");
        assertEquals(TestResultType.SUCCESS, processResponse.getReport().getResult(), "Result of the processing shall be success !");
    }

    /**
     * Test Processing a patient search, service throwing an exception.
     */
    @Test
    @Covers(requirements = {"PATREG-55", "PATREG-58", "PATREG-59"})
    public void process_with_exception() {
        testPatientSearchService.setSearchResult(TestPatientSearchServiceImpl.SearchResult.KO);
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_SEARCH_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_SEARCH_CRITERIA_INPUT_NAME,
                    new SearchCriteria()));
        } catch (MappingException e) {
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        ProcessResponse processResponse = patientSearchProcessingService.processPatientSearch(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
        assertNotNull(processResponse.getReport(), "ProcessResponse.report shall not be null !");
        assertEquals(TestResultType.FAILURE, processResponse.getReport().getResult(), "Result of the processing shall be success !");
        assertNotNull(processResponse.getReport().getReports(), "Report shall contain error");
        assertNotNull(processResponse.getReport().getReports().getInfoOrWarningOrError(), "Report shall contain error");
        assertEquals(1, processResponse.getReport().getReports().getInfoOrWarningOrError().size(), "Report shall contain a single error");
        assertNotNull(processResponse.getReport().getReports().getInfoOrWarningOrError().get(0), "Report shall contain error");
        assertNotNull(((BAR) processResponse.getReport().getReports().getInfoOrWarningOrError().get(0).getValue()).getDescription(),
                "Report shall contain error with valued description !");
    }

    /**
     * Test Processing a patient search, service returning null.
     */
    @Test
    @Covers(requirements = {"PATREG-51"})
    public void process_with_null_return() {
        testPatientSearchService.setSearchResult(TestPatientSearchServiceImpl.SearchResult.NULL);
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_SEARCH_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_SEARCH_CRITERIA_INPUT_NAME,
                    new SearchCriteria()));
        } catch (MappingException e) {
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        assertThrows(IllegalArgumentException.class, () -> patientSearchProcessingService.processPatientSearch(processRequest));
    }

    /**
     * Test Processing a patient search, error case using and AnyContent input that is not convertible to a Patient.
     */
    @Test
    @Covers(requirements = {"PATREG-52", "PATREG-53"})
    public void process_Incorrect_Patient() {
        AnyContent anyContent = new AnyContent();
        anyContent.setName("Test");
        anyContent.setValue("TarteAuxPommesDuVergerDeMonEnfance");
        anyContent.setEmbeddingMethod(ValueEmbeddingEnumeration.STRING);
        anyContent.setType("string");
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_FEED_OPERATION);
        processRequest.getInput().add(anyContent);
        assertThrows(IllegalArgumentException.class, () -> patientSearchProcessingService.processPatientSearch(processRequest));
    }

    /**
     * Test Processing a patient search, error case using no input in ProcessRequest
     */
    @Test
    @Covers(requirements = {"PATREG-53", "PATREG-54"})
    public void process_no_input() {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_FEED_OPERATION);
        assertThrows(IllegalArgumentException.class, () -> patientSearchProcessingService.processPatientSearch(processRequest));
    }

    @Test
    @Covers(requirements = {"PATREG-50", "PATREG-56", "PATREG-57", "PATREG-59"})
    public void parametrizedSearchProcess() {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_PARAMETERS_SEARCH_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_PARAMETERS_SEARCH_INPUT_NAME,
                    new SearchParameter()));
        } catch (MappingException e) {
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        ProcessResponse processResponse = patientSearchProcessingService.processPatientParametersSearch(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
        assertEquals(1, processResponse.getOutput().size(), "Response shall contain a unique output which is the value of the" +
                " UUID assigned to the fed patient.");
        assertEquals("list", processResponse.getOutput().get(0).getType(), "Returned UUID shall be stored in a string GITB AnyContent");
        assertEquals(PatientProcessingWebserviceConstants.PATIENT_LIST_OUTPUT_NAME, processResponse.getOutput().get(0).getName(), "Name of returned GITB AnyContent shall be 'uuid'");
        assertEquals(ValueEmbeddingEnumeration.BASE_64, processResponse.getOutput().get(0).getEmbeddingMethod(), "Embedding method of returned " +
                "GITB AnyContent shall be 'STRING'");
        assertNotNull(processResponse.getReport(), "ProcessResponse.report shall not be null !");
        assertEquals(TestResultType.SUCCESS, processResponse.getReport().getResult(), "Result of the processing shall be success !");
    }

    @Test
    @Covers(requirements = {"PATREG-55", "PATREG-58", "PATREG-59"})
    public void parametrizedSearchProcess_with_exception() {
        testPatientSearchService.setSearchResult(TestPatientSearchServiceImpl.SearchResult.KO);
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_PARAMETERS_SEARCH_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_PARAMETERS_SEARCH_INPUT_NAME,
                    new SearchParameter()));
        } catch (MappingException e) {
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        ProcessResponse processResponse = patientSearchProcessingService.processPatientParametersSearch(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
        assertNotNull(processResponse.getReport(), "ProcessResponse.report shall not be null !");
        assertEquals(TestResultType.FAILURE, processResponse.getReport().getResult(), "Result of the processing shall be success !");
        assertNotNull(processResponse.getReport().getReports(), "Report shall contain error");
        assertNotNull(processResponse.getReport().getReports().getInfoOrWarningOrError(), "Report shall contain error");
        assertEquals(1, processResponse.getReport().getReports().getInfoOrWarningOrError().size(), "Report shall contain a single error");
        assertNotNull(processResponse.getReport().getReports().getInfoOrWarningOrError().get(0), "Report shall contain error");
        assertNotNull(((BAR) processResponse.getReport().getReports().getInfoOrWarningOrError().get(0).getValue()).getDescription(),
                "Report shall contain error with valued description !");
    }

    @Test
    @Covers(requirements = {"PATREG-51"})
    public void parametrizedSearchProcess_with_null_return() {
        testPatientSearchService.setSearchResult(TestPatientSearchServiceImpl.SearchResult.NULL);
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_PARAMETERS_SEARCH_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_PARAMETERS_SEARCH_INPUT_NAME,
                    new SearchParameter()));
        } catch (MappingException e) {
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        assertThrows(IllegalArgumentException.class, () -> patientSearchProcessingService.processPatientParametersSearch(processRequest));
    }

    @Test
    @Covers(requirements = {"PATREG-52", "PATREG-53"})
    public void parametrizedSearchProcess_Incorrect_Patient() {
        AnyContent anyContent = new AnyContent();
        anyContent.setName("Test");
        anyContent.setValue("TarteAuxPommesDuVergerDeMonEnfance");
        anyContent.setEmbeddingMethod(ValueEmbeddingEnumeration.STRING);
        anyContent.setType("string");
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_FEED_OPERATION);
        processRequest.getInput().add(anyContent);
        assertThrows(IllegalArgumentException.class, () -> patientSearchProcessingService.processPatientParametersSearch(processRequest));
    }


}