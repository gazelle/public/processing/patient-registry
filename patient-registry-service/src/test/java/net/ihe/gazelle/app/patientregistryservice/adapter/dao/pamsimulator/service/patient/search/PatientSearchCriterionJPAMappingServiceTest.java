package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search;

import net.ihe.gazelle.app.patientregistryapi.business.PatientSearchCriterionKey;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.key.SearchCriterionKey;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.operator.SearchCriterionOperator;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.option.SearchCriterionOptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;

import static org.junit.jupiter.api.Assertions.*;

public class PatientSearchCriterionJPAMappingServiceTest {

    private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitTest";

    private EntityManager entityManager;
    private From originalFrom;

    /**
     * Database init
     */
    @BeforeEach
    public void initializeDatabase(){
        entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST).createEntityManager();
        entityManager.getTransaction().begin();
        originalFrom = entityManager.getCriteriaBuilder().createQuery(PatientDB.class).from(PatientDB.class);
    }

    /**
     * Test from returned for uuid is not null
     */
    @Test
    public void fromUuid(){
        From from = new PatientSearchCriterionJPAMappingService(entityManager).from(PatientSearchCriterionKey.UUID, originalFrom);

        assertEquals(originalFrom, from);
    }

    /**
     * Test from returned for domain is not null
     */
    @Test
    public void fromDomain(){
        From from = new PatientSearchCriterionJPAMappingService(entityManager).from(PatientSearchCriterionKey.DOMAIN, originalFrom);

        assertNotNull(from, "from should not be null for domain");
    }

    /**
     * Test from returned for domain is not null and is not created multiple times
     */
    @Test
    public void fromDomainMultiple() {
        PatientSearchCriterionJPAMappingService patientSearchCriterionJPAMappingService = new PatientSearchCriterionJPAMappingService(entityManager);
        patientSearchCriterionJPAMappingService.from(PatientSearchCriterionKey.DOMAIN, originalFrom);
        patientSearchCriterionJPAMappingService.from(PatientSearchCriterionKey.DOMAIN, originalFrom);

        assertEquals(1, originalFrom.getJoins().size());
        assertEquals(1, ((Join) originalFrom.getJoins().iterator().next()).getJoins().size());
    }

    /**
     * Test from returned for identifier is not null
     */
    @Test
    public void fromIdentifier(){
        From from = new PatientSearchCriterionJPAMappingService(entityManager).from(PatientSearchCriterionKey.IDENTIFIER, originalFrom);

        assertNotNull(from, "from should not be null for identifier");
    }

    /**
     * Test from returned for identifier is not null and is not created multiple times
     */
    @Test
    public void fromIdentifierMultiple() {
        PatientSearchCriterionJPAMappingService patientSearchCriterionJPAMappingService = new PatientSearchCriterionJPAMappingService(entityManager);
        patientSearchCriterionJPAMappingService.from(PatientSearchCriterionKey.IDENTIFIER, originalFrom);
        patientSearchCriterionJPAMappingService.from(PatientSearchCriterionKey.IDENTIFIER, originalFrom);

        assertEquals(1, originalFrom.getJoins().size());
    }

    /**
     * Test unknown key generate an exception
     */
    @Test
    public void from_unknown_key(){
        assertThrows(IllegalArgumentException.class, () -> new PatientSearchCriterionJPAMappingService(entityManager)
                .from(new SearchCriterionKey() {
                    @Override
                    public SearchCriterionOptions getDefaultOptions() {
                        return null;
                    }

                    @Override
                    public SearchCriterionOperator getDefaultOperator() {
                        return null;
                    }

                    @Override
                    public Class getValueClass() {
                        return null;
                    }
                },null));
    }

    /**
     * Test the getter for the property name for uuid
     */
    @Test
    public void testGetEntityPropertyUUID() {
        assertEquals("uuid", new PatientSearchCriterionJPAMappingService(entityManager).getEntityProperty(PatientSearchCriterionKey.UUID));
    }

    /**
     * Test the getter for the property name for Identifier
     */
    @Test
    public void testGetEntityPropertyIdentifier() {
        assertEquals("identifier",
                new PatientSearchCriterionJPAMappingService(entityManager).getEntityProperty(PatientSearchCriterionKey.IDENTIFIER));
    }

    /**
     * Test the getter for the property name for domain
     */
    @Test
    public void testGetEntityPropertyDomain() {
        assertEquals("universalID", new PatientSearchCriterionJPAMappingService(entityManager).getEntityProperty(PatientSearchCriterionKey.DOMAIN));
    }

    /**
     * Test the getter for the property name for unknown criterion
     */
    @Test
    public void testGetEntityPropertyUnknown() {
        assertThrows(IllegalArgumentException.class,
                () -> new PatientSearchCriterionJPAMappingService(entityManager).getEntityProperty(new SearchCriterionKey() {
            @Override
            public SearchCriterionOptions getDefaultOptions() {
                return null;
            }

            @Override
            public SearchCriterionOperator getDefaultOperator() {
                return null;
            }

            @Override
            public Class getValueClass() {
                return null;
            }
        }));
    }
}
