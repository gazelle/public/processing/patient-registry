package net.ihe.gazelle.app.patientregistryservice.application.exceptions;

import io.qameta.allure.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

@Feature("Coverage on exception")
@Story("PatientCreationException")
class PatientCreationExceptionTest {
    /**
     * Dummy Test for coverage
     *
     * @throws PatientCreationException testException
     */
    @Test
    @Description("PatientCreationException")
    @Severity(SeverityLevel.MINOR)
    void generateException() {

        assertThrows(PatientCreationException.class, () -> {
            throw new PatientCreationException("Test");
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientCreationException testException
     */
    @Test
    @Description("PatientCreationException")
    @Severity(SeverityLevel.MINOR)
    void generateException1() {
        assertThrows(PatientCreationException.class, () -> {
            throw new PatientCreationException("Test", new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientCreationException testException
     */
    @Test
    @Description("PatientCreationException")
    @Severity(SeverityLevel.MINOR)
    void generateException2() {
        assertThrows(PatientCreationException.class, () -> {
            throw new PatientCreationException(new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientCreationException testException
     */
    @Test
    @Description("PatientCreationException")
    @Severity(SeverityLevel.MINOR)
    void generateException4() {
        assertThrows(PatientCreationException.class, () -> {
            throw new PatientCreationException("Test", new IllegalArgumentException("test"), false, true);
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientCreationException testException
     */
    @Test
    @Description("PatientCreationException")
    @Severity(SeverityLevel.MINOR)
    void generateException3() {
        assertThrows(PatientCreationException.class, () -> {
            throw new PatientCreationException();
        });
    }

}