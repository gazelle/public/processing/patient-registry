package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.converter;

import net.ihe.gazelle.app.patientregistryapi.business.Observation;
import net.ihe.gazelle.app.patientregistryapi.business.ObservationType;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PersonName;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test conversion of Patient from business to Database model or the opposite
 */
class PatientConverterTest {

    /**
     * test uuid mapping
     */
    @Test
    public void mapUuidTest() {
        Patient patient = new Patient();
        String uuid = "uuid";
        patient.setUuid(uuid);
        PatientDB patientDB = PatientConverter.toPatientDB(patient);

        assertEquals(uuid, patientDB.getUuid(), "uuid should be mapped to db uuid");
    }

    /**
     * test multiple birth mapping
     */
    @Test
    public void mapMultipleBirthTest() {
        Patient patient = new Patient();
        Integer multipleBirth = Integer.valueOf("1");
        patient.setMultipleBirthOrder(multipleBirth);
        PatientDB patientDB = PatientConverter.toPatientDB(patient);


        assertTrue(patientDB.getMultipleBirthIndicator(), "birth indicator should be true");
        assertEquals(multipleBirth.intValue(), patientDB.getBirthOrder(), "birth order should be mapped");
    }

    /**
     * test patient name
     */
    @Test
    public void mapNameTest() {
        Patient patient = new Patient();
        PersonName name = new PersonName();
        String family = "family";
        name.setFamily(family);
        patient.addName(name);

        PatientDB patientDB = PatientConverter.toPatientDB(patient);

        assertEquals(family, patientDB.getLastName(), "name should be mapped");
    }

    /**
     * test dead patient
     */
    @Test
    public void mapDeathDateTest() {
        Patient patient = new Patient();
        Date death = new Date();
        patient.setDateOfDeath(death);
        PatientDB patientDB = PatientConverter.toPatientDB(patient);

        assertEquals(death, patientDB.getPatientDeathTime(), "death date should be mapped");
        assertTrue(patientDB.isPatientDead());
    }

    /**
     * Test Map race observation to patientDB
     */
    @Test
    public void map_race_observation_to_patientDB(){
        Patient patient = new Patient();
        patient.addObservation(new Observation(ObservationType.RACE, "Race"));

        PatientDB patientDB = PatientConverter.toPatientDB(patient);

        assertEquals("Race", patientDB.getRaceCode());
    }

    /**
     * Test Map weight observation to patientDB
     */
    @Test
    public void map_weight_observation_to_patientDB(){
        Patient patient = new Patient();
        patient.addObservation(new Observation(ObservationType.WEIGHT, "120"));

        PatientDB patientDB = PatientConverter.toPatientDB(patient);

        assertEquals(120, patientDB.getWeight());
    }

    /**
     * Test Map height observation to patientDB
     */
    @Test
    public void map_height_observation_to_patientDB(){
        Patient patient = new Patient();
        patient.addObservation(new Observation(ObservationType.HEIGHT, "232"));

        PatientDB patientDB = PatientConverter.toPatientDB(patient);

        assertEquals(232, patientDB.getSize());
    }

    /**
     * Test Map religion observation to patientDB
     */
    @Test
    public void map_religion_observation_to_patientDB(){
        Patient patient = new Patient();
        patient.addObservation(new Observation(ObservationType.RELIGION, "Toupiste"));

        PatientDB patientDB = PatientConverter.toPatientDB(patient);

        assertEquals("Toupiste", patientDB.getReligionCode());
    }

    /**
     * Test Map blood group observation to patientDB
     */
    @Test
    public void map_blood_group_observation_to_patientDB(){
        Patient patient = new Patient();
        patient.addObservation(new Observation(ObservationType.BLOOD_GROUP, "O+"));

        PatientDB patientDB = PatientConverter.toPatientDB(patient);

        assertEquals("O+", patientDB.getBloodGroup());
    }

    /**
     * Test Map marital Status observation to patientDB
     */
    @Test
    public void map_marital_status_observation_to_patientDB(){
        Patient patient = new Patient();
        patient.addObservation(new Observation(ObservationType.MARITAL_STATUS, "Single"));

        PatientDB patientDB = PatientConverter.toPatientDB(patient);

        assertEquals("Single", patientDB.getMaritalStatus());
    }

    /**
     * Test Map mother maiden name observation to patientDB
     */
    @Test
    public void map_mother_maiden_name_observation_to_patientDB(){
        Patient patient = new Patient();
        patient.addObservation(new Observation(ObservationType.MOTHERS_MAINDEN_NAME, "Guilluy"));

        PatientDB patientDB = PatientConverter.toPatientDB(patient);

        assertEquals("Guilluy", patientDB.getMotherMaidenName());
    }

    /**
     * Test Map vip indicator name observation to patientDB
     */
    @Test
    public void map_vip_indicator_observation_to_patientDB(){
        Patient patient = new Patient();
        patient.addObservation(new Observation(ObservationType.VIP_INDICATOR, "VIP"));

        PatientDB patientDB = PatientConverter.toPatientDB(patient);

        assertEquals("VIP", patientDB.getVipIndicator());
    }

    /**
     * Test Map identity reliability code name observation to patientDB
     */
    @Test
    public void map_identity_reliability_code_observation_to_patientDB(){
        Patient patient = new Patient();
        patient.addObservation(new Observation(ObservationType.IDENTITY_RELIABILITY_CODE, "DontTrustThisDude"));

        PatientDB patientDB = PatientConverter.toPatientDB(patient);

        assertEquals("DontTrustThisDude", patientDB.getIdentityReliabilityCode());
    }
}