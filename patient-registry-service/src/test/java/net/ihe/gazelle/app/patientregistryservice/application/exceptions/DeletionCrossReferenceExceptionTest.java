package net.ihe.gazelle.app.patientregistryservice.application.exceptions;

import io.qameta.allure.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

@Feature("Coverage on exception")
@Story("DeletionCrossReferenceException")
class DeletionCrossReferenceExceptionTest {
    /***
     * Dummy Test for coverage
     * @throws DeletionCrossReferenceException testException
     */
    @Test
    @Description(" DeletionCrossReferenceException")
    @Severity(SeverityLevel.MINOR)
    void generateException() {
        assertThrows(DeletionCrossReferenceException.class, () -> {
            throw new DeletionCrossReferenceException("Test");
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws DeletionCrossReferenceException testException
     */
    @Test
    @Description(" DeletionCrossReferenceException")
    @Severity(SeverityLevel.MINOR)
    void generateException1() {
        assertThrows(DeletionCrossReferenceException.class, () -> {
            throw new DeletionCrossReferenceException("Test", new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws DeletionCrossReferenceException testException
     */
    @Test
    @Description(" DeletionCrossReferenceException")
    @Severity(SeverityLevel.MINOR)
    void generateException2() {
        assertThrows(DeletionCrossReferenceException.class, () -> {
            throw new DeletionCrossReferenceException(new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws DeletionCrossReferenceException testException
     */
    @Test
    @Description(" DeletionCrossReferenceException")
    @Severity(SeverityLevel.MINOR)
    void generateException4() {
        assertThrows(DeletionCrossReferenceException.class, () -> {
            throw new DeletionCrossReferenceException("Test", new IllegalArgumentException("test"), false, true);
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws DeletionCrossReferenceException testException
     */
    @Test
    @Description(" DeletionCrossReferenceException")
    @Severity(SeverityLevel.MINOR)
    void generateException3() {
        assertThrows(DeletionCrossReferenceException.class, () -> {
            throw new DeletionCrossReferenceException();
        });
    }
}