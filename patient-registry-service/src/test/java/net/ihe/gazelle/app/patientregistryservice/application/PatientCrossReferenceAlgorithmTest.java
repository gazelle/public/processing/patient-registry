package net.ihe.gazelle.app.patientregistryservice.application;

import io.qameta.allure.*;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.GenderCodeDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.references.CrossReferenceDB;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.CrossReferenceApplicationException;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;


@Feature("XRefAlgorithm")
class PatientCrossReferenceAlgorithmTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientCrossReferenceAlgorithmTest.class);

    @Test
    @Description("Test on creating/updating a PatientDB if it is added to an existing CrossReferenceDB deemed close enough")
    @Severity(SeverityLevel.CRITICAL)
    @Story("core")
    void crossReferencingSuccessful1DiffTest() {
        PatientDB patientToCompare = createPatient("Alain", "Allal", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        PatientDB patient1 = createPatient("Alain", "Allan", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        PatientDB patient2 = createPatient("test", "test", GenderCodeDB.M, LocalDate.of(1991, 6, 19));
        CrossReferenceDB crossReferenceDB = new CrossReferenceDB();
        crossReferenceDB.setId(2);
        patient1.setPixReference(crossReferenceDB);
        CrossReferenceDB crossReferenceDB2 = new CrossReferenceDB();
        crossReferenceDB2.setId(4);
        patient2.setPixReference(crossReferenceDB2);
        List<PatientDB> patientsList = new ArrayList<>();
        patientsList.add(patient2);
        patientsList.add(patient1);
        Assertions.assertEquals(2, patientsList.size());
        try {
            int crossReferenceId = PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList);
            Assertions.assertEquals(2, crossReferenceId);
        } catch (CrossReferenceApplicationException e) {
            Assert.fail();
            LOGGER.error(e.getMessage());
        }

    }

    @Test
    @Description("Test on creating/updating a PatientDB if it is added to an existing CrossReferenceDB deemed close enough")
    @Severity(SeverityLevel.CRITICAL)
    @Story("core")
    void crossReferencingSuccessfulExactlySameTest() {
        PatientDB patientToCompare = createPatient("Alain", "Allal", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        PatientDB patient1 = createPatient("Alain", "Allal", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        PatientDB patient2 = createPatient("test", "test", GenderCodeDB.M, LocalDate.of(1991, 6, 19));
        CrossReferenceDB crossReferenceDB = new CrossReferenceDB();
        crossReferenceDB.setId(4);
        patient1.setPixReference(crossReferenceDB);
        CrossReferenceDB crossReferenceDB2 = new CrossReferenceDB();
        crossReferenceDB2.setId(2);
        patient2.setPixReference(crossReferenceDB2);
        List<PatientDB> patientsList = new ArrayList<>();
        patientsList.add(patient2);
        patientsList.add(patient1);
        Assertions.assertEquals(2, patientsList.size());
        try {
            int crossReferenceId = PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList);
            Assertions.assertEquals(4, crossReferenceId);
        } catch (CrossReferenceApplicationException e) {
            LOGGER.error(e.getMessage());
            Assert.fail();
        }

    }


    @Test
    @Description("Test on creating/updating a PatientDB if it is added to an existing CrossReferenceDB deemed close enough")
    @Severity(SeverityLevel.CRITICAL)
    @Story("core")
    void crossReferencingSuccessfulLongNameTest() {
        PatientDB patientToCompare = createPatient("Ceciestunprenomextremementlong", "Etceciestunprenomextremementlong", GenderCodeDB.M,
                LocalDate.of(1990, 6, 19));
        PatientDB patient1 = createPatient("Ceciestunprenomextremementlong12", "Etceciestunprenomextremementlong12", GenderCodeDB.M,
                LocalDate.of(1990,
                6, 19));
        PatientDB patient2 = createPatient("test", "test", GenderCodeDB.M, LocalDate.of(1991, 6, 19));
        CrossReferenceDB crossReferenceDB = new CrossReferenceDB();
        crossReferenceDB.setId(2);
        patient1.setPixReference(crossReferenceDB);
        CrossReferenceDB crossReferenceDB2 = new CrossReferenceDB();
        crossReferenceDB2.setId(4);
        patient2.setPixReference(crossReferenceDB2);
        List<PatientDB> patientsList = new ArrayList<>();
        patientsList.add(patient2);
        patientsList.add(patient1);
        Assertions.assertEquals(2, patientsList.size());
        try {
            int crossReferenceId = PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList);
            Assertions.assertEquals(2, crossReferenceId);
        } catch (CrossReferenceApplicationException e) {
            LOGGER.error(e.getMessage());
            Assert.fail();
        }
    }

    @Test
    @Description("Test on creating/updating a PatientDB if it is not added to an existing CrossReferenceDB deemed close enough because the " +
            "levenshtein " +
            "gate of 3 is not valid.")
    @Severity(SeverityLevel.CRITICAL)
    @Story("core")
    void crossReferencingUnsuccessfulLongNameTest() throws CrossReferenceApplicationException {
        PatientDB patientToCompare = createPatient("Ceciestunprenomextremementlong", "Etceciestunprenomextremementlong", GenderCodeDB.M,
                LocalDate.of(1990, 6, 19));
        PatientDB list1 = createPatient("Ceciestunprenomextremementlong123", "Etceciestunprenomextremementlong123", GenderCodeDB.M,
                LocalDate.of(1990, 6, 19));
        CrossReferenceDB crossReferenceDB = new CrossReferenceDB();

        List<PatientDB> patientsList = new ArrayList<>();
        patientsList.add(list1);
        crossReferenceDB.setPatients(patientsList);

        Assertions.assertEquals(1, crossReferenceDB.getPatients().size());
        Assert.assertEquals(0, PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList));
    }

    @Test
    @Description("Test on creating/updating a PatientDB if it not is added to an existing CrossReferenceDB deemed close enough due to less than 80%" +
            " " +
            "similarity")
    @Severity(SeverityLevel.CRITICAL)
    @Story("core")
    void crossReferencingUnsuccessful80PercentDiffSameTest() throws CrossReferenceApplicationException {
        PatientDB patientToCompare = createPatient("Alain", "Allal", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        PatientDB list1 = createPatient("Alain", "All", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        CrossReferenceDB crossReferenceDB = new CrossReferenceDB();

        List<PatientDB> patientsList = new ArrayList<>();
        patientsList.add(list1);
        crossReferenceDB.setPatients(patientsList);
        Assertions.assertEquals(1, crossReferenceDB.getPatients().size());
        Assert.assertEquals(0, PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList));
    }

    @Test
    @Description("Test on creating/updating a PatientDB if it is added to an existing CrossReferenceDB deemed close enough even without accentuation")
    @Severity(SeverityLevel.CRITICAL)
    @Story("core")
    void crossReferencingSuccessfulAccentTest() {
        PatientDB patientToCompare = createPatient("Alàîn", "Allàl", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        PatientDB patient1 = createPatient("Alain", "Allal", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        PatientDB patient2 = createPatient("test", "test", GenderCodeDB.M, LocalDate.of(1991, 6, 19));
        CrossReferenceDB crossReferenceDB = new CrossReferenceDB();
        crossReferenceDB.setId(2);
        patient1.setPixReference(crossReferenceDB);
        CrossReferenceDB crossReferenceDB2 = new CrossReferenceDB();
        crossReferenceDB2.setId(4);
        patient2.setPixReference(crossReferenceDB2);
        List<PatientDB> patientsList = new ArrayList<>();
        patientsList.add(patient2);
        patientsList.add(patient1);
        Assertions.assertEquals(2, patientsList.size());
        try {
            int crossReferenceId = PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList);
            Assertions.assertEquals(2, crossReferenceId);
        } catch (CrossReferenceApplicationException e) {
            LOGGER.error(e.getMessage());
            Assert.fail();
        }

    }

    @Test
    @Description("Test on creating/updating a PatientDB if it is added to an existing CrossReferenceDB deemed close enough even with Special " +
            "Characters" +
            " added")
    @Severity(SeverityLevel.CRITICAL)
    @Story("core")
    void crossReferencingSuccessfulSpecialCharacterTest() {
        PatientDB patientToCompare = createPatient("Alain#?./^", "Allal", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        PatientDB list1 = createPatient("Alain", "llal", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        CrossReferenceDB crossReferenceDB = new CrossReferenceDB();
        crossReferenceDB.setId(2);
        List<PatientDB> patientsList = new ArrayList<>();
        patientsList.add(list1);
        list1.setPixReference(crossReferenceDB);
        Assertions.assertEquals(1, crossReferenceDB.getPatients().size());
        try {
            int crossReferenceId = PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList);
            Assertions.assertEquals(2, crossReferenceId);
        } catch (CrossReferenceApplicationException e) {
            LOGGER.error(e.getMessage());
            Assert.fail();
        }

    }


    @Test
    @Description("Testing two Patients are not in the same CrossReferenceDB if their birth date are different")
    @Severity(SeverityLevel.CRITICAL)
    @Story("core")
    void crossReferencingBadBirthDate() throws CrossReferenceApplicationException {
        PatientDB patientToCompare = createPatient("Alain", "Allal", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        PatientDB list1 = createPatient("Alain", "Allal", GenderCodeDB.M, LocalDate.of(1990, 6, 20));
        CrossReferenceDB crossReferenceDB = new CrossReferenceDB();

        List<PatientDB> patientsList = new ArrayList<>();
        patientsList.add(list1);
        crossReferenceDB.setPatients(patientsList);
        Assert.assertEquals(0, PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList));
    }

    @Test
    @Description("Testing two Patients are not in the same CrossReferenceDB if their gender are different")
    @Severity(SeverityLevel.CRITICAL)
    @Story("core")
    void crossReferencingBadGender() throws CrossReferenceApplicationException {
        PatientDB patientToCompare = createPatient("Alain", "Allal", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        PatientDB list1 = createPatient("Alain", "Allal", GenderCodeDB.F, LocalDate.of(1990, 6, 19));
        CrossReferenceDB CrossReferenceDB = new CrossReferenceDB();

        List<PatientDB> patientsList = new ArrayList<>();
        patientsList.add(list1);
        CrossReferenceDB.setPatients(patientsList);
        Assert.assertEquals(0, PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList));
    }

    @Test
    @Description("Testing two Patients are not in the same CrossReferenceDB if their family name are too different")
    @Severity(SeverityLevel.CRITICAL)
    @Story("core")
    void crossReferencingBadFamilyName() throws CrossReferenceApplicationException {
        PatientDB patientToCompare = createPatient("Alain", "Annan", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        PatientDB list1 = createPatient("Alain", "Allal", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        CrossReferenceDB CrossReferenceDB = new CrossReferenceDB();

        List<PatientDB> patientsList = new ArrayList<>();
        patientsList.add(list1);
        CrossReferenceDB.setPatients(patientsList);
        Assert.assertEquals(0, PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList));
    }

    @Test
    @Description("Testing two Patients are not in the same CrossReferenceDB if their given name are too different")
    @Severity(SeverityLevel.CRITICAL)
    @Story("core")
    void crossReferencingBadGivenName() throws CrossReferenceApplicationException {
        PatientDB patientToCompare = createPatient("Alain", "Allal", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        PatientDB list1 = createPatient("Silvain", "Allal", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        CrossReferenceDB CrossReferenceDB = new CrossReferenceDB();

        List<PatientDB> patientsList = new ArrayList<>();
        patientsList.add(list1);
        CrossReferenceDB.setPatients(patientsList);
        Assert.assertEquals(0, PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList));
    }

    @Test
    @Description("Testing when first PatientDB has empty name")
    @Severity(SeverityLevel.CRITICAL)
    @Story("core")
    void crossReferencingPatientNameBlank() throws CrossReferenceApplicationException {
        PatientDB patientToCompare = createPatient("", "Allal", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        PatientDB list1 = createPatient("Silvain", "Allal", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        CrossReferenceDB CrossReferenceDB = new CrossReferenceDB();

        List<PatientDB> patientsList = new ArrayList<>();
        patientsList.add(list1);
        CrossReferenceDB.setPatients(patientsList);
        assertThrows(CrossReferenceApplicationException.class,
                () -> PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList));
        try {
            PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList);
            Assert.fail();
        } catch (CrossReferenceApplicationException e) {
            Assertions.assertEquals("PatientDB to compare does not have the minimum required fields to be crossreferenced", e.getMessage());
        }
    }

    @Test
    @Description("Testing when first PatientDB has empty first name")
    @Severity(SeverityLevel.CRITICAL)
    @Story("core")
    void crossReferencingPatientFirstNameBlank() throws CrossReferenceApplicationException {
        PatientDB patientToCompare = createPatient("", "Allal", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        PatientDB list1 = createPatient("Silvain", "Allal", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        CrossReferenceDB CrossReferenceDB = new CrossReferenceDB();

        List<PatientDB> patientsList = new ArrayList<>();
        patientsList.add(list1);
        CrossReferenceDB.setPatients(patientsList);
        assertThrows(CrossReferenceApplicationException.class,
                () -> PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList));
        try {
            PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList);
            Assert.fail();
        } catch (CrossReferenceApplicationException e) {
            Assertions.assertEquals("PatientDB to compare does not have the minimum required fields to be crossreferenced",
                    e.getMessage());
        }
    }

    @Test
    @Description("Testing when second PatientDB has empty gender")
    @Severity(SeverityLevel.CRITICAL)
    @Story("core")
    void crossReferencingSecondPatientGenderNull() throws CrossReferenceApplicationException {
        PatientDB patientToCompare = createPatient("Allal", "Allal", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        PatientDB list1 = createPatient("Silvain", "Allal", null, LocalDate.of(1990, 6, 19));
        CrossReferenceDB CrossReferenceDB = new CrossReferenceDB();

        List<PatientDB> patientsList = new ArrayList<>();
        patientsList.add(list1);
        CrossReferenceDB.setPatients(patientsList);
        assertThrows(CrossReferenceApplicationException.class,
                () -> PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList));
        try {
            PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList);
            Assert.fail();
        } catch (CrossReferenceApplicationException e) {
            Assertions.assertEquals("PatientDB from Database does not have the minimum required fields to be crossreferenced", e.getMessage());
        }
    }

    @Test
    @Description("Testing when second PatientDB has empty birth date")
    @Severity(SeverityLevel.CRITICAL)
    @Story("core")
    void crossReferencingSecondPatientBirthDateNull() throws CrossReferenceApplicationException {
        PatientDB patientToCompare = createPatient("Allal", "Allal", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        PatientDB list1 = createPatient("Silvain", "Allal", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        list1.setDateOfBirth(null);
        CrossReferenceDB CrossReferenceDB = new CrossReferenceDB();

        List<PatientDB> patientsList = new ArrayList<>();
        patientsList.add(list1);
        CrossReferenceDB.setPatients(patientsList);
        assertThrows(CrossReferenceApplicationException.class,
                () -> PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList));
        try {
            PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList);
            Assert.fail();
        } catch (CrossReferenceApplicationException e) {
            Assertions.assertEquals("PatientDB from Database does not have the minimum required fields to be crossreferenced", e.getMessage());
        }
    }

    @Test
    @Description("Testing when second PatientDB is null")
    @Severity(SeverityLevel.CRITICAL)
    @Story("core")
    void crossReferencingSecondPatientNull() throws CrossReferenceApplicationException {
        PatientDB patientToCompare = createPatient("Allal", "Allal", GenderCodeDB.F, LocalDate.of(1990, 6, 19));

        assertThrows(IllegalArgumentException.class,
                () -> PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, null));
    }

    @Test
    @Description("Testing when second PatientDB has empty birth date")
    @Severity(SeverityLevel.CRITICAL)
    @Story("core")
    void crossReferencingFirstPatientNull() throws CrossReferenceApplicationException {

        assertThrows(IllegalArgumentException.class, () -> PatientCrossReferenceAlgorithm.crossReferencing(null
                , null));
        try {
            PatientCrossReferenceAlgorithm.crossReferencing(null, null);
            Assert.fail();
        } catch (IllegalArgumentException e) {
            Assertions.assertEquals("PatientDB toCompare is empty and cannot be processed", e.getMessage());
        }
    }
    @Test
    @Description("Test When second patientD has a too small name")
    @Severity(SeverityLevel.CRITICAL)
    @Story("core")
    void crossReferencingWithTooSmallName(){
        PatientDB patientToCompare = createPatient("Alain", "A", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        PatientDB patient1 = createPatient("Alain", "A", GenderCodeDB.M, LocalDate.of(1990, 6, 19));
        PatientDB patient2 = createPatient("test", "test", GenderCodeDB.M, LocalDate.of(1991, 6, 19));
        CrossReferenceDB crossReferenceDB = new CrossReferenceDB();
        crossReferenceDB.setId(4);
        patient1.setPixReference(crossReferenceDB);
        CrossReferenceDB crossReferenceDB2 = new CrossReferenceDB();
        crossReferenceDB2.setId(2);
        patient2.setPixReference(crossReferenceDB2);
        List<PatientDB> patientsList = new ArrayList<>();
        patientsList.add(patient2);
        patientsList.add(patient1);
        Assertions.assertEquals(2, patientsList.size());
        try {
            int crossReferenceId = PatientCrossReferenceAlgorithm.crossReferencing(patientToCompare, patientsList);
            Assertions.assertEquals(0, crossReferenceId);
        } catch (CrossReferenceApplicationException e) {
            LOGGER.error(e.getMessage());
            Assert.fail();
        }

    }

    private PatientDB createPatient(String firstName, String lastName, GenderCodeDB gender, LocalDate birthDate) {
        PatientDB PatientDB = new PatientDB();
        PatientDB.setLastName(lastName);
        PatientDB.setFirstName(firstName);
        PatientDB.setGenderCode(gender);
        PatientDB.setDateOfBirth(Date.from(birthDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
        return PatientDB;

    }





}