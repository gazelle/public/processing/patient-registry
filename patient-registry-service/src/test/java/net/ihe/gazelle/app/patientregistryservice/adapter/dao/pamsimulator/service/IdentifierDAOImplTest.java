package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.DesignatorTypeDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.HierarchicDesignatorDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientIdentifierDB;
import net.ihe.gazelle.app.patientregistryservice.application.dao.DomainDAO;
import net.ihe.gazelle.app.patientregistryservice.application.dao.IdentifierDAO;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.DomainSearchException;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.PatientIdentifierException;
import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertThrows;

@Feature("PatientIdentifierDAO")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class IdentifierDAOImplTest {

    private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitPatientIdentifierTest";
    private IdentifierDAO identifierDAO;
    private DomainDAO domainDAO;

    private EntityManager entityManager;


    /**
     * database init
     */
    @BeforeAll
    public void initializeDatabase() throws ParseException {
        entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST).createEntityManager();
        domainDAO = new DomainDAOImpl(entityManager);
        identifierDAO = new IdentifierDAOImpl(entityManager, domainDAO);
        entityManager.getTransaction().begin();
    }

    /**
     * Close the connection to the database after all tests are executed.
     */
    @AfterAll
    public void closeDatabase() {
        entityManager.clear();
        entityManager.close();
    }


    @Test
    @Description("Test on constructor to generated an exception no entity")
    void constructorWithNullEntity() {
        assertThrows(IllegalArgumentException.class, () -> new IdentifierDAOImpl(null, domainDAO));
    }

    @Test
    @Description("Test on constructor to generated an exception no DomainDAO")
    void constructorWithNullDomainDAO() {
        assertThrows(IllegalArgumentException.class, () -> new IdentifierDAOImpl(entityManager, null));
    }

    @Test
    @Description("Test on constructor to generated an exception no parameter")
    void constructorWithNullParameter() {
        assertThrows(IllegalArgumentException.class, () -> new IdentifierDAOImpl(null, null));
    }

    @Test
    @Description("Test on Create method with no domain saved")
    void createPatientIdentifierWithoutDomain() throws PatientIdentifierException {
        PatientIdentifierDB patientIdentifierDB = new PatientIdentifierDB();
        patientIdentifierDB.setIdentifier("testIdentifierNoDomain");
        patientIdentifierDB.setIdentifierTypeCode("ISO");
        identifierDAO.createPatientIdentifier(patientIdentifierDB);
        PatientIdentifierDB patientIdentifierDB1 = identifierDAO.retrieve(patientIdentifierDB.getIdentifier(), "ISO");
        Assert.assertEquals("identifier shall be equal", patientIdentifierDB1.getIdentifier(), patientIdentifierDB.getIdentifier());

        identifierDAO.deletePatientIdentifier(patientIdentifierDB1.getId());

        Assert.assertNull( identifierDAO.retrieve(patientIdentifierDB.getIdentifier(), "ISO"));
    }

    @Test
    @Description("Test on Create method with  domain")
    void createPatientIdentifierWithDomain() throws PatientIdentifierException, DomainSearchException {
        domainDAO.createDomain("test", "test");
        PatientIdentifierDB patientIdentifierDB = new PatientIdentifierDB();
        patientIdentifierDB.setIdentifier("testIdentifierDomain");
        patientIdentifierDB.setIdentifierTypeCode("ISO");
        HierarchicDesignatorDB hierarchicDesignatorDB = domainDAO.retrieve("test");
        patientIdentifierDB.setDomain(hierarchicDesignatorDB);

        identifierDAO.createPatientIdentifier(patientIdentifierDB);
        PatientIdentifierDB patientIdentifierDB1 = identifierDAO.retrieve(patientIdentifierDB.getFullPatientId(), "ISO");
        Assert.assertEquals("identifier shall be equal", patientIdentifierDB1.getIdentifier(), patientIdentifierDB.getIdentifier());

        identifierDAO.deletePatientIdentifier("testIdentifierDomain",hierarchicDesignatorDB.getUniversalID());

        Assert.assertNull( identifierDAO.retrieve(patientIdentifierDB.getIdentifier(), "ISO"));
        domainDAO.delete("test");
    }

    @Test
    @Description("Test on Create method generate exception dude to dupplicate")
    void createPatientIdentifierException() throws PatientIdentifierException, DomainSearchException {
        domainDAO.createDomain("test", "test");
        PatientIdentifierDB patientIdentifierDB = new PatientIdentifierDB();
        patientIdentifierDB.setIdentifier("testIdentifier");
        patientIdentifierDB.setIdentifierTypeCode("ISO");
        HierarchicDesignatorDB hierarchicDesignatorDB = domainDAO.retrieve("test");
        patientIdentifierDB.setDomain(hierarchicDesignatorDB);
        hierarchicDesignatorDB.setUsage(DesignatorTypeDB.PATIENT_ID);


        identifierDAO.createPatientIdentifier(patientIdentifierDB);
        PatientIdentifierDB patientIdentifierDB1 = identifierDAO.searchForPatientIdentifier(domainDAO.searchForHierarchicDesignator("test"), "testIdentifier");
        Assert.assertEquals("identifier shall be equal", patientIdentifierDB1.getIdentifier(), patientIdentifierDB.getIdentifier());

        identifierDAO.deletePatientIdentifier(patientIdentifierDB1.getId());
        assertThrows(PatientIdentifierException.class, () -> identifierDAO.searchForPatientIdentifier(domainDAO.searchForHierarchicDesignator("test"), "testIdentifier"));
        Assert.assertNull( identifierDAO.retrieve(patientIdentifierDB.getIdentifier(), "ISO"));
        domainDAO.delete("test");
    }

    @Test
    @Description("Test on Read Method Generate exception")
    void readPatientIdentifierException() throws PatientIdentifierException, DomainSearchException {
        assertThrows(IllegalArgumentException.class, () -> identifierDAO.readPatientIdentifier(null));
        assertThrows(PatientIdentifierException.class,() -> identifierDAO.readPatientIdentifier(1));
        assertThrows(PatientIdentifierException.class,() -> identifierDAO.deletePatientIdentifier(1));
    }

    @Test
    @Description("Test on delete Method generate exception")
    void deletePatientIdentifierException() throws PatientIdentifierException, DomainSearchException {
        assertThrows(PatientIdentifierException.class,() ->  identifierDAO.deletePatientIdentifier("testIdentifierDomain","test"));
    }



}