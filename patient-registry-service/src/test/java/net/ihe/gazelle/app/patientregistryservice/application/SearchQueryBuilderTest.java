package net.ihe.gazelle.app.patientregistryservice.application;

import io.qameta.allure.Feature;
import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.GenderCodeDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search.PatientFieldsMatcherServiceImpl;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search.SearchQueryBuilderImpl;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Feature("SearchQueryBuilder")
class SearchQueryBuilderTest {

    private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitTest";
    private SearchQueryBuilderImpl searchQueryBuilder;
    private EntityManager entityManager;

    @BeforeEach
    public void setUp() {

        entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST).createEntityManager();
        entityManager.getTransaction().begin();
        searchQueryBuilder = new SearchQueryBuilderImpl(entityManager, new PatientFieldsMatcherServiceImpl());
    }


    @Test
    void testBuildQueryWithMultipleQueries() throws SearchException {

        List<SearchParameter> searchParameters = new ArrayList<>();

        searchParameters.add(new SearchParameter("given", "equals", "Michel", "String"));
        searchParameters.add(new SearchParameter("family", "equals", "Dupont", "String"));
        Query query = searchQueryBuilder.build(searchParameters);
        Assertions.assertEquals("SELECT p FROM PatientDB p  WHERE p.lastName = :value2 AND p.firstName = :value1 AND p.genderCode IN ('F', 'M', 'O', 'U')", query.unwrap(org.hibernate.query.Query.class).getQueryString());

    }

    @Test
    void testGetQueryValue() throws SearchException {
        Map<String, Object> values = new HashMap<>();
        searchQueryBuilder.getQueryValue("=", "Michel", "String",values);
        searchQueryBuilder.getQueryValue("=", "0", "Integer",values);
        searchQueryBuilder.getQueryValue("=", "true", "Boolean",values);
        searchQueryBuilder.getQueryValue("=", "female", "GenderCode",values);
        searchQueryBuilder.getQueryValue("=", "male", "GenderCode",values);
        searchQueryBuilder.getQueryValue("=", "other", "GenderCode",values);
        searchQueryBuilder.getQueryValue("=", "undefined", "GenderCode",values);
        Assertions.assertEquals("Michel", values.get("value1"));
        Assertions.assertEquals(0, values.get("value2"));
        Assertions.assertEquals(true, values.get("value3"));
        Assertions.assertEquals(GenderCodeDB.F, values.get("value4"));
        Assertions.assertEquals(GenderCodeDB.M, values.get("value5"));
        Assertions.assertEquals(GenderCodeDB.O, values.get("value6"));
        Assertions.assertEquals(GenderCodeDB.U, values.get("value7"));
    }

    @Test
    void testGetQueryValueWithUnsupportedClassType() {
        try {
            searchQueryBuilder.getQueryValue("=", "Michel", "UnsupportedClassType", new HashMap<>());
            Assertions.fail("Should have thrown a SearchException");
        } catch (SearchException e) {
            Assertions.assertEquals("Class type not supported", e.getMessage());
        }
    }

    @Test
    void testJointure() throws SearchException {
        List<SearchParameter> searchParameters = new ArrayList<>();

        searchParameters.add(new SearchParameter("address-city", "equals", "Paris", "String"));
        searchParameters.add(new SearchParameter("address-country", "equals", "France", "String"));
        searchParameters.add(new SearchParameter("address-postalcode", "equals", "75000", "String"));
        searchParameters.add(new SearchParameter("address-state", "equals", "Ile-de-France", "String"));
        Query query = searchQueryBuilder.build(searchParameters);
        Assertions.assertEquals("SELECT p FROM PatientDB p LEFT JOIN PatientAddressDB  a ON p = a.patient  WHERE a.countryCode = :value2 AND a.state = :value4 AND a.zipCode = :value3 AND a.city = :value1 AND p.genderCode IN ('F', 'M', 'O', 'U')", query.unwrap(org.hibernate.query.Query.class).getQueryString());
    }

    @Test
    void testBuildQueryWithEmptySearchParameters() throws SearchException {
        List<SearchParameter> searchParameters = new ArrayList<>();
        Query query = searchQueryBuilder.build(searchParameters);
        Assertions.assertEquals("SELECT p FROM PatientDB p  WHERE p.genderCode IN ('F', 'M', 'O', 'U')", query.unwrap(org.hibernate.query.Query.class).getQueryString());

    }

    @Test
    void testBuildQueryWithOneQuery() throws SearchException {
        List<SearchParameter> searchParameters = new ArrayList<>();
        searchParameters.add(new SearchParameter("given", "equals", "Michel", "String"));
        Query query = searchQueryBuilder.build(searchParameters);
        Assertions.assertEquals("SELECT p FROM PatientDB p  WHERE p.firstName = :value1 AND p.genderCode IN ('F', 'M', 'O', 'U')", query.unwrap(org.hibernate.query.Query.class).getQueryString());

    }

    @Test
    void testBuildQueryWithOrCondition() throws SearchException {
        List<SearchParameter> searchParameters = new ArrayList<>();
        searchParameters.add(new SearchParameter("family", "equals", "Dupond", "String"));
        searchParameters.add(new SearchParameter("family", "equals", "Dupont", "String"));
        Query query = searchQueryBuilder.build(searchParameters);
        Assertions.assertEquals("SELECT p FROM PatientDB p  WHERE (p.lastName = :value1 OR p.lastName = :value2) AND p.genderCode IN ('F', 'M', 'O', 'U')", query.unwrap(org.hibernate.query.Query.class).getQueryString());

    }
}
