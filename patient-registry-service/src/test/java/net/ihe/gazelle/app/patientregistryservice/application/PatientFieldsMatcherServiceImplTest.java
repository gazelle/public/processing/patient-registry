package net.ihe.gazelle.app.patientregistryservice.application;


import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search.PatientFieldsMatcherServiceImpl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

 class PatientFieldsMatcherServiceImplTest {

    private PatientFieldsMatcherServiceImpl patientFieldsMatcherServiceImpl;

     @BeforeEach
    public void setUp() {
        patientFieldsMatcherServiceImpl = new PatientFieldsMatcherServiceImpl(null);
    }

    @Test
     void testGetMapValue() {
        Assertions.assertEquals("p.uuid", patientFieldsMatcherServiceImpl.getMapValue("_id"));
        Assertions.assertEquals("p.firstName", patientFieldsMatcherServiceImpl.getMapValue("given"));
        Assertions.assertEquals("p.stillActive", patientFieldsMatcherServiceImpl.getMapValue("active"));
        Assertions.assertEquals("p.lastName", patientFieldsMatcherServiceImpl.getMapValue("family"));
        Assertions.assertEquals("i.identifier", patientFieldsMatcherServiceImpl.getMapValue("identifier-value"));
       Assertions.assertEquals("i.domain.universalID", patientFieldsMatcherServiceImpl.getMapValue("identifier-domain-id"));
        Assertions.assertEquals("p.email", patientFieldsMatcherServiceImpl.getMapValue("telecom"));
        Assertions.assertEquals("a.addressLine", patientFieldsMatcherServiceImpl.getMapValue("address"));
        Assertions.assertEquals("a.city", patientFieldsMatcherServiceImpl.getMapValue("address-city"));
        Assertions.assertEquals("a.zipCode", patientFieldsMatcherServiceImpl.getMapValue("address-postalcode"));
        Assertions.assertEquals("a.countryCode", patientFieldsMatcherServiceImpl.getMapValue("address-country"));
        Assertions.assertEquals("a.state", patientFieldsMatcherServiceImpl.getMapValue("address-state"));
        Assertions.assertEquals("p.genderCode", patientFieldsMatcherServiceImpl.getMapValue("gender"));
        Assertions.assertEquals("p.dateOfBirth", patientFieldsMatcherServiceImpl.getMapValue("birthdate"));
        Assertions.assertEquals("p.motherMaidenName", patientFieldsMatcherServiceImpl.getMapValue("mothersMaidenName"));

    }

    @Test
     void testGetMapValueWithNull() {
        Assertions.assertNull(patientFieldsMatcherServiceImpl.getMapValue(null));
    }
}





