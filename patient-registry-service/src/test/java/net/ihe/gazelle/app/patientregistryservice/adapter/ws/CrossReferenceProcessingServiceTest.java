package net.ihe.gazelle.app.patientregistryservice.adapter.ws;

import com.gitb.ps.Void;
import com.gitb.ps.*;
import io.cucumber.java.it.Ma;
import net.ihe.gazelle.app.patientregistryapi.adapter.ws.CrossReferenceProcessingWebServiceConstants;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperObjectToAnyContent;
import net.ihe.gazelle.lib.gitbutils.adapter.MappingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for class {@link CrossReferenceProcessingService}
 */
class CrossReferenceProcessingServiceTest {

    private CrossReferenceProcessingService crossReferenceProcessingService;


    /**
     * Initialize implementations to be tested before each test.
     */
    @BeforeEach
    public void setUp() {
        this.crossReferenceProcessingService =
                new CrossReferenceProcessingService(new CrossReferenceSearchProcessingService(new PatientCrossReferenceSearchMock()));
    }

    /**
     * Test Unsupported Operation getModuleDefinition
     */
    @Test
    void getModuleDefinition() {
        assertThrows(UnsupportedOperationException.class, () -> crossReferenceProcessingService.getModuleDefinition(new Void()));
    }

    /**
     * Test Unsupported Operation beginTransaction
     */
    @Test
    void beginTransaction() {
        assertThrows(UnsupportedOperationException.class, () -> crossReferenceProcessingService.beginTransaction(new BeginTransactionRequest()));
    }

    /**
     * Test Unsupported Operation endTransaction
     */
    @Test
    void endTransaction() {
        assertThrows(UnsupportedOperationException.class, () -> crossReferenceProcessingService.endTransaction(new BasicRequest()));
    }


    /**
     * Test Processing a patient operation, error case using null ProcessRequest
     */
    @Test
    void process_no_ProcessingRequest() {
        assertThrows(IllegalArgumentException.class, () -> crossReferenceProcessingService.process(null));
    }

    /**
     * Test Processing a patient operation, error case using wrong operation in ProcessRequest.
     */
    @Test
    void process_Wrong_Operation() {

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation("Wrong Operation");
        assertThrows(UnsupportedOperationException.class, () -> crossReferenceProcessingService.process(processRequest));
    }

    /**
     * Test Processing a x-Ref Search with mock
     */
    @Test
    void process_Patient_Search() {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("identifierSystem");
        sourceIdentifier.setValue("identifierValue");
        List<String> targetDomains = new ArrayList<>();
        targetDomains.add("test");
        ProcessRequest processRequest = new ProcessRequest();

        processRequest.setOperation(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_SEARCH_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_NAME, sourceIdentifier));
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_TARGET_NAME,targetDomains));
        } catch (MappingException e) {
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        ProcessResponse processResponse = crossReferenceProcessingService.process(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
    }

    /**
     * Test on Constructor for coverage
     */
    @Test
     void create_a_CrossRefProcessingService() {

        CrossReferenceProcessingService crossReferenceProcessingService = new CrossReferenceProcessingService();
        if (crossReferenceProcessingService != null) {
            System.out.println("Test is passed");
        } else {
            fail();
        }

    }
}
