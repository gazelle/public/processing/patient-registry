package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.DesignatorTypeDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.HierarchicDesignatorDB;
import net.ihe.gazelle.app.patientregistryservice.application.dao.DomainDAO;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.DomainSearchException;
import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Persistence;
import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertThrows;

@Feature("DomainDAO")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DomainDAOImplTest {

    private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitDomainTest";
    private DomainDAO domainDAO;

    private EntityManager entityManager;


    /**
     * database init
     */
    @BeforeAll
    public void initializeDatabase() throws ParseException {
        entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST).createEntityManager();
        domainDAO = new DomainDAOImpl(entityManager);
        entityManager.getTransaction().begin();
    }

    /**
     * Close the connection to the database after all tests are executed.
     */
    @AfterAll
    public void closeDatabase() {
        entityManager.clear();
        entityManager.close();
    }

    @Test
    @Description("Test on constructor to generated an exception")
    void constructorWithNullEntity() {
        assertThrows(IllegalArgumentException.class, () -> new DomainDAOImpl(null));
    }

    @Test
    @Description("Create a new domain")
    void createNewDomain() throws DomainSearchException {
        domainDAO.createDomain("domain1", "Domain1Name");

        HierarchicDesignatorDB hierarchicDesignatorDB = domainDAO.retrieve("domain1");

        Assert.assertNotNull("An id shall be generated", hierarchicDesignatorDB.getId());
        Assert.assertEquals("the domainid shall be identical", "domain1", hierarchicDesignatorDB.getUniversalID());

        domainDAO.delete("domain1");
    }

    @Test
    @Description("Test on the function isExist()")
    void isExist() throws DomainSearchException {
        Assert.assertFalse(domainDAO.exist("domainExist"));
        domainDAO.createDomain("domain1", "Domain1Name");
        Assert.assertTrue(domainDAO.exist("domain1"));
        domainDAO.delete("domain1");
    }

    @Test
    @Description("Test on the function delete()")
    void deleteDomain() throws DomainSearchException {
        domainDAO.createDomain("domain1", "Domain1Name");
        Assert.assertTrue(domainDAO.exist("domain1"));
        domainDAO.delete("domain1");
        Assert.assertFalse(domainDAO.exist("domain1"));
    }

    @Test
    @Description("Test on the function retrieve() and return the HierarchicDesignatorDB entity from the DB")
    void retrieveHierarchicDesignatorDB() throws DomainSearchException {
        domainDAO.createDomain("domain1", "Domain1Name");
        Assert.assertTrue(domainDAO.exist("domain1"));
        HierarchicDesignatorDB hierarchicDesignatorDB = domainDAO.retrieve("domain1");
        Assert.assertNotNull("An id shall be generated", hierarchicDesignatorDB.getId());
        Assert.assertEquals("the domainid shall be identical", "domain1", hierarchicDesignatorDB.getUniversalID());
        domainDAO.delete("domain1");
    }


    @Test
    @Description("Test on the function retrieve() and excepted a DomainSearchException with NoResultException ")
    void retrieveHierarchicDesignatorDBExceptionNoResult() {
        DomainSearchException exception = assertThrows(DomainSearchException.class, () -> domainDAO.retrieve("Test1"));
        Assert.assertEquals("The cause shall be", NoResultException.class, exception.getCause().getClass());
        Assert.assertEquals("System does not exist", exception.getMessage());
    }

    @Test
    @Description("Test on the function retrieve() and excepted a DomainSearchException with NonUniqueResultException ")
    void retrieveHierarchicDesignatorDBExceptionNonUniqueResultException() throws DomainSearchException {
        domainDAO.createDomain("Test", "Domain1Name");
        HierarchicDesignatorDB domain = new HierarchicDesignatorDB("Domain1Name", "Test", "ISO", null);
        domain.setUsage(DesignatorTypeDB.PATIENT_ID);
        HierarchicDesignatorDB hierarchicDesignatorDB1 = entityManager.merge(domain);
        DomainSearchException exception = assertThrows(DomainSearchException.class, () -> domainDAO.retrieve("Test"));
        Assert.assertEquals("The cause shall be", NonUniqueResultException.class, exception.getCause().getClass());
        Assert.assertEquals("Duplicated System for value : Test", exception.getMessage());
    }

    @Test
    @Description("Test on the function retrieve() and return the id entity from the DB")
    void retrieveHierarchicDesignatorDBID() throws DomainSearchException {
        domainDAO.createDomain("domain1", "Domain1Name");
        Assert.assertTrue(domainDAO.exist("domain1"));
        Integer hierarchicDesignatorDB = domainDAO.searchForHierarchicDesignator("domain1");
        Assert.assertNotNull("the domainid shall not be null", hierarchicDesignatorDB);
        domainDAO.delete("domain1");
    }

    @Test
    @Description("Try to create retreive without parameter")
    void expectedExceptionThrows() {

        assertThrows(IllegalArgumentException.class, () -> domainDAO.createDomain(null, null));
        assertThrows(IllegalArgumentException.class, () -> domainDAO.retrieve(null));
        assertThrows(IllegalArgumentException.class, () -> domainDAO.delete(null));
        assertThrows(IllegalArgumentException.class, () -> domainDAO.exist(null));

    }


}