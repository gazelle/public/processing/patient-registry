package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.DomainDAOImpl;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.IdentifierDAOImpl;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.PatientCrossReferenceDAOImpl;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search.*;
import net.ihe.gazelle.app.patientregistryservice.application.dao.PatientDAO;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests on PatientDAO implementation
 */
@Feature("PatientDAO Test")
class PatientDaoTest {

    private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitTest";

    private PatientDAO patientDAO;

    private EntityManager entityManager;


    /**
     * Initialize Database to test the DAO implementation
     */
    @BeforeEach
    void initializeDatabase() {
        entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST).createEntityManager();
        entityManager.getTransaction().begin();
        patientDAO = new PatientDAOImpl(entityManager,
                new PatientSearchDAO(entityManager, new PatientSearchCriterionJPAMappingService(entityManager),
                        new PatientSearchResultJPAMappingService())
                , new DomainDAOImpl(entityManager), new IdentifierDAOImpl(entityManager, new DomainDAOImpl(entityManager)),
                new PatientCrossReferenceDAOImpl(entityManager), new SearchQueryBuilderImpl(entityManager, new PatientFieldsMatcherServiceImpl()));
    }

    /**
     * Close Database after tests.
     */
    @AfterEach
    void closeDatabase() {
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    /**
     * test patient feed and retrieve
     */
    @Test
    void retrievePatientTest() throws PatientCreationException, PatientReadException, PatientDeleteException, PatientNotFoundException {
        Patient patient = new Patient();
        String uuid = "myUuid";

        patient.setUuid(uuid);
        patientDAO.createPatient(patient);

        Patient mergedPatient = patientDAO.readPatient(uuid);
        assertEquals(uuid, mergedPatient.getUuid());

        patientDAO.deletePatient(mergedPatient.getUuid());
    }

    /**
     * test delete patient
     */
    @Test
    void deletePatientTest() throws PatientReadException, PatientCreationException, PatientDeleteException, PatientNotFoundException {
        Patient patient = new Patient();
        String uuid = "myUuid";

        patient.setUuid(uuid);
        patientDAO.createPatient(patient);

        patientDAO.deletePatient(patientDAO.readPatient(uuid));

        assertThrows(PatientNotFoundException.class, () -> patientDAO.readPatient(uuid));
    }

    /**
     * test patient feed and isExisting
     */
    @Test
    void existingPatientTest() throws PatientCreationException, PatientReadException, PatientDeleteException, PatientNotFoundException {
        Patient patient = new Patient();
        String uuid = "myUuid";

        patient.setUuid(uuid);
        patientDAO.createPatient(patient);

        assertTrue(patientDAO.isPatientExisting(uuid));

        patientDAO.deletePatient(patientDAO.readPatient(uuid));
    }

    @Test
    @Description("Try use methods without parameter")
    void expectedExceptionThrows() {
        assertThrows(IllegalArgumentException.class, () -> patientDAO.deletePatient((String) null));
        assertThrows(IllegalArgumentException.class, () -> patientDAO.deletePatient((Patient) null));

        assertThrows(IllegalArgumentException.class, () -> patientDAO.deletePatient(new Patient()));
        assertThrows(IllegalArgumentException.class, () -> patientDAO.deletePatient((Patient) null));
        assertThrows(IllegalArgumentException.class, () -> patientDAO.deletePatient(""));
        assertThrows(IllegalArgumentException.class, () -> patientDAO.deletePatient((String) null));
        assertThrows(IllegalArgumentException.class, () -> patientDAO.readPatient((String) null));
        assertThrows(IllegalArgumentException.class, () -> patientDAO.createPatientDB(null));
        assertThrows(IllegalArgumentException.class, () -> patientDAO.createPatient(null));
        assertThrows(IllegalArgumentException.class, () -> patientDAO.updatePatientDB(null));
        assertThrows(IllegalArgumentException.class, () -> patientDAO.isPatientExisting(null));
        assertThrows(IllegalArgumentException.class, () -> patientDAO.readPatientDB(null));
        assertThrows(IllegalArgumentException.class, () -> patientDAO.retrieveMatchingPatients(null));


    }

    @Test
    @Description("Try use methods with empty parameter")
    void expectedExceptionThrowsEmpty() {
        assertThrows(IllegalArgumentException.class, () -> patientDAO.deletePatient(""));
        assertThrows(IllegalArgumentException.class, () -> patientDAO.deletePatient((String) ""));
        assertThrows(IllegalArgumentException.class, () -> patientDAO.readPatient((String) ""));
        assertThrows(IllegalArgumentException.class, () -> patientDAO.createPatientDB(null));
        assertThrows(IllegalArgumentException.class, () -> patientDAO.createPatient(null));
        assertThrows(IllegalArgumentException.class, () -> patientDAO.updatePatientDB(null));
        assertThrows(IllegalArgumentException.class, () -> patientDAO.isPatientExisting(null));
        assertThrows(IllegalArgumentException.class, () -> patientDAO.readPatientDB(null));
        assertThrows(IllegalArgumentException.class, () -> patientDAO.retrieveMatchingPatients(null));
    }

    @Test
    @Description("Testing search methods")
    void searchTest() throws PatientSearchException {
        SearchParameter searchParameter = new SearchParameter("given", "Michel", "equals", "String");

        List<SearchParameter> searchParameters = new ArrayList<>();
        searchParameters.add(searchParameter);
        assertNotNull(patientDAO.searchPatients(searchParameters));
        assertEquals(0, patientDAO.searchPatients(searchParameters).size());

    }


}
