package net.ihe.gazelle.app.patientregistryservice.application.exceptions;

import io.qameta.allure.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

@Feature("Coverage on exception")
@Story("PatientNotFoundException")
class PatientNotFoundExceptionTest {
    /**
     * Dummy Test for coverage
     *
     * @throws PatientNotFoundException testException
     */
    @Test
    @Description(" PatientNotFoundException")
    @Severity(SeverityLevel.MINOR)
    void generateException() {
        assertThrows(PatientNotFoundException.class, () -> {
            throw new PatientNotFoundException("Test");
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientNotFoundException testException
     */
    @Test
    @Description(" PatientNotFoundException")
    @Severity(SeverityLevel.MINOR)
    void generateException1() {
        assertThrows(PatientNotFoundException.class, () -> {
            throw new PatientNotFoundException("Test", new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientNotFoundException testException
     */
    @Test
    @Description(" PatientNotFoundException")
    @Severity(SeverityLevel.MINOR)
    void generateException2() {
        assertThrows(PatientNotFoundException.class, () -> {
            throw new PatientNotFoundException(new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientNotFoundException testException
     */
    @Test
    @Description(" PatientNotFoundException")
    @Severity(SeverityLevel.MINOR)
    void generateException4() {
        assertThrows(PatientNotFoundException.class, () -> {
            throw new PatientNotFoundException("Test", new IllegalArgumentException("test"), false, true);
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientNotFoundException testException
     */
    @Test
    @Description(" PatientNotFoundException")
    @Severity(SeverityLevel.MINOR)
    void generateException3() {
        assertThrows(PatientNotFoundException.class, () -> {
            throw new PatientNotFoundException();
        });
    }
}