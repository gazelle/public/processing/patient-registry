package net.ihe.gazelle.app.patientregistryservice.application;

import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.PatientDAOMock;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Test for Patient Search Application service.
 */
public class PatientSearchApplicationTest {

    PatientSearchApplication patientSearchApplication;
    PatientDAOMock patientDAOMock;

    /**
     * Initialize implementations to be tested before each test.
     */
    @BeforeEach
    public void init() {
        patientDAOMock = new PatientDAOMock();
        patientSearchApplication = new PatientSearchApplication(patientDAOMock);
    }

    /**
     * Test patient Search with valued Criteria
     */
    @Test
    public void searchPatientWithCriteria() throws SearchException {
        List<Patient> patients = patientSearchApplication.search(new SearchCriteria());
        assertEquals(patientDAOMock.getReturnedPatientList(), patients);
    }

    /**
     * Test patient Search with no Criteria
     */
    @Test
    public void searchPatientWithoutCriteria() {
        assertThrows(SearchException.class, () -> patientSearchApplication.search(null));
    }

    /**
     * Test patient Search with a returned PatientReadException
     */
    @Test
    public void searchPatientReadException() {
        patientDAOMock.setSearchResult(PatientDAOMock.SearchResult.PATIENT_READ_EXCEPTION);

        assertThrows(SearchException.class, () -> patientSearchApplication.search(null));
    }


    /**
     * Test patient Search with valued Parameters
     */
    @Test
    public void searchPatientWithParameters() throws SearchException {
        List<Patient> patients = patientSearchApplication.parametrizedSearch(new ArrayList<SearchParameter>());

        assertEquals(patientDAOMock.getReturnedPatientList(), patients);
    }

    /**
     * Test patient Search with no Parameters
     */
    @Test
    public void searchPatientWithoutParameters() {
        assertThrows(SearchException.class, () -> patientSearchApplication.parametrizedSearch(null));
    }
}
