package net.ihe.gazelle.app.patientregistryservice.feature.feed.it;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedException;
import net.ihe.gazelle.app.patientregistryapi.business.*;
import net.ihe.gazelle.app.patientregistryfeedclient.adapter.PatientFeedClient;
import net.ihe.gazelle.app.patientregistrysearchclient.adapter.PatientSearchClient;
import net.ihe.gazelle.app.patientregistryservice.adapter.ws.PatientProcessingService;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.SearchCriterion;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.StringSearchCriterion;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Contains Step Definitions for the execution of Patient Feed IT tests
 */
public class FeedExecutionStepDefinition {

    private PatientFeedClient patientFeedClient;
    private PatientSearchClient patientSearchClient;

    private List<Patient> existingPatients;
    private Patient fedPatient;

    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    private String uuidOfFedPatient;

    /**
     * Sets up in-memory Database and instantiate tested services.
     */
    @Before
    public void setUpFeedClient() throws MalformedURLException {
        patientFeedClient = new PatientFeedClient(new URL("http://localhost:8580/patient-registry/PatientProcessingService/patient-processing" +
                "-service?wsdl"));
        patientSearchClient = new PatientSearchClient(new URL("http://localhost:8580/patient-registry/PatientProcessingService/patient-processing" +
                "-service?wsdl"));

        existingPatients = new ArrayList<>();
    }

    /**
     * Precondition that will store a new {@link Patient} as existing, so it can be used later on in tests.
     *
     * @param patientDataList list of information on the patient to create
     * @throws ParseException if dates cannot be parsed from feature.
     */
    @Given("the following patients exist")
    public void the_following_patients_exist(List<Map<String, String>> patientDataList) throws ParseException {
        for (Map<String, String> patientData : patientDataList) {
            Patient patient = new Patient();
            patient.setUuid(patientData.get("tmpuuid"));
            patient.setGender(GenderCode.valueOf(patientData.get("gender")));
            patient.setActive(Boolean.valueOf(patientData.get("active")));
            patient.setDateOfBirth(dateFormat.parse(patientData.get("dateOfBirth")));
            patient.setDateOfDeath(dateFormat.parse(patientData.get("dateOfDeath")));
            patient.setMultipleBirthOrder(Integer.valueOf(patientData.get("multipleBirthOrder")));

            existingPatients.add(patient);
        }
    }

    /**
     * Precondition that will add identifiers to existing patients.
     *
     * @param patientIdentifierDataList list on information on Identifiers to create
     */
    @Given("patients have the following identifiers")
    public void patients_have_the_following_identifiers(List<Map<String, String>> patientIdentifierDataList) {
        for (Map<String, String> identifierData : patientIdentifierDataList) {

            EntityIdentifier identifier = new EntityIdentifier(identifierData.get("systemIdentifier"),
                    identifierData.get("systemName"), identifierData.get("type"), identifierData.get("value"));

            Patient patient = getExistingPatientWithUUID(identifierData.get("tmpuuid"));
            if (patient == null) {
                throw new IllegalArgumentException(String.format("No patient corresponding to UUID %s !", identifierData.get("tmpuuid")));
            } else {
                patient.addIdentifier(identifier);
            }
        }
    }

    /**
     * Precondition that will add names to existing patients.
     *
     * @param nameDataList list on information on names to create
     */
    @Given("patients have the following names")
    public void patients_have_the_following_names(List<Map<String, String>> nameDataList) {
        for (Map<String, String> nameData : nameDataList) {

            PersonName personName = new PersonName(nameData.get("family"), null, null, null);
            if (nameData.get("given1") != null) {
                personName.addGiven(nameData.get("given1"));
            }
            if (nameData.get("given2") != null) {
                personName.addGiven(nameData.get("given2"));
            }
            if (nameData.get("given3") != null) {
                personName.addGiven(nameData.get("given3"));
            }

            Patient patient = getExistingPatientWithUUID(nameData.get("tmpuuid"));
            if (patient == null) {
                throw new IllegalArgumentException(String.format("No patient corresponding to UUID %s !", nameData.get("tmpuuid")));
            } else {
                patient.addName(personName);
            }
        }
    }

    /**
     * Precondition that will add addresses to existing patients.
     *
     * @param addressDataList list on information on addresses to create
     */
    @Given("patients have the following addresses")
    public void patients_have_the_following_addresses(List<Map<String, String>> addressDataList) {
        for (Map<String, String> addressData : addressDataList) {

            Address address = new Address(addressData.get("city"), addressData.get("countryIso3"), addressData.get("postalCode"), addressData.get(
                    "state")
                    , AddressUse.valueOf(addressData.get("use")));
            if (addressData.get("line1") != null && !addressData.get("line1").isEmpty()) {
                address.addLine(addressData.get("line1"));
            }
            Patient patient = getExistingPatientWithUUID(addressData.get("tmpuuid"));
            if (patient == null) {
                throw new IllegalArgumentException(String.format("No patient corresponding to UUID %s !", addressData.get("tmpuuid")));
            } else {
                patient.addAddress(address);
            }
        }
    }

    /**
     * Precondition that will add contact points to existing patients.
     *
     * @param contactPointDataList list on information on contact points to create
     */
    @Given("patients have the following contact points")
    public void patients_have_the_following_contact_points(List<Map<String, String>> contactPointDataList) {
        for (Map<String, String> contactPointData : contactPointDataList) {

            ContactPoint contactPoint = new ContactPoint(null, ContactPointUse.valueOf(contactPointData.get("use")),
                    contactPointData.get("value"));

            Patient patient = getExistingPatientWithUUID(contactPointData.get("tmpuuid"));
            if (patient == null) {
                throw new IllegalArgumentException(String.format("No patient corresponding to UUID %s !", contactPointData.get("tmpuuid")));
            } else {
                patient.addContactPoint(contactPoint);
            }
        }
    }

    /**
     * Precondition that will add observations to existing patients.
     *
     * @param observationDataList list on information on observations to create
     */
    @Given("patients have the following observations")
    public void patients_have_the_following_observations(List<Map<String, String>> observationDataList) {
        for (Map<String, String> observationData : observationDataList) {

            Observation observation = new Observation(ObservationType.valueOf(observationData.get("type")), observationData.get("value"));

            Patient patient = getExistingPatientWithUUID(observationData.get("tmpuuid"));
            if (patient == null) {
                throw new IllegalArgumentException(String.format("No patient corresponding to UUID %s !", observationData.get("tmpuuid")));
            } else {
                patient.addObservation(observation);
            }
        }
    }

    /**
     * Feed a patient using tested {@link PatientFeedClient} and {@link PatientProcessingService}
     *
     * @param uuid temporary UUID of the patient to feed.
     * @throws PatientFeedException if the feed cannot be performed correctly.
     */
    @When("patient is fed with provisional uuid {string}")
    public void patient_is_fed_with_uuid(String uuid) throws PatientFeedException {
        Patient patient = getExistingPatientWithUUID(uuid);
        if (patient != null) {
            uuidOfFedPatient = patientFeedClient.createPatient(patient);
        } else {
            throw new IllegalArgumentException(String.format("No patient existing with UUID %s !", uuid));
        }
    }

    /**
     * Checks the patient fed to the registry has a new UUID assigned and that it can be retrieve with a {@link PatientSearchClient}
     *
     * @throws SearchException if the patient cannot be retrieved.
     */
    @Then("fed patient has returned new assigned uuid")
    public void fed_patient_has_the_returned_new_assigned_uuid() throws SearchException {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.addSearchCriterion(getSearchCriterionForUUID(uuidOfFedPatient));
        List<Patient> searchedPatients = patientSearchClient.search(searchCriteria);
        assertNotNull(searchedPatients);
        assertEquals(1, searchedPatients.size());
        fedPatient = searchedPatients.get(0);
        assertEquals(uuidOfFedPatient, fedPatient.getUuid());
    }

    /**
     * Compare existing Patient (set from feature data) and Patient retrieved after fed to the registry.
     *
     * @param provisionalUUID provisional UUID of the patient that was fed.
     * @throws SearchException if the patient cannot be retrieved.
     */
    @Then("fed patient is equals to existing patient with provisional uuid {string}")
    public void fed_patient_is_equal_to_existing_patient_with_provisional_uuid(String provisionalUUID) throws SearchException {
        fedPatient.setUuid(provisionalUUID);

        Patient existingPatient = getExistingPatientWithUUID(provisionalUUID);

        assertNotNull(existingPatient, String.format("No existing patient with UUID %s !", provisionalUUID));
        assertEquals(existingPatient, fedPatient);

        fedPatient.setUuid(uuidOfFedPatient);
    }

    /**
     * Create a searchCriterion for UUID. This is used to search for fed patient based on the returned assigned UUID.
     *
     * @param uuid value of the UUID to search.
     * @return {@link SearchCriterion} corresponding to the request.
     */
    private SearchCriterion getSearchCriterionForUUID(String uuid) {
        SearchCriterion searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.UUID);
        searchCriterion.setValue(uuid);
        return searchCriterion;
    }

    /***
     * Get an existing patient by its temporary UUID.
     * @param uuid      UUID of the patient to get.
     * @return the patient with requested UUID
     */
    private Patient getExistingPatientWithUUID(String uuid) {
        if (uuid != null) {
            return existingPatients.stream()
                    .filter(patient -> uuid.equals(patient.getUuid()))
                    .findAny()
                    .orElse(null);
        } else {
            throw new IllegalArgumentException("UUID shall not be null !");
        }
    }
}
