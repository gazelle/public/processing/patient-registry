package net.ihe.gazelle.app.patientregistryservice.adapter.ws;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.ps.ProcessRequest;
import com.gitb.ps.ProcessResponse;
import com.gitb.tr.BAR;
import com.gitb.tr.TestResultType;
import net.ihe.gazelle.app.patientregistryapi.adapter.ws.CrossReferenceProcessingWebServiceConstants;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperObjectToAnyContent;
import net.ihe.gazelle.lib.gitbutils.adapter.MappingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CrossReferenceSearchProcessingServiceTest {


    private CrossReferenceSearchProcessingService crossReferenceSearchProcessingService;
    private PatientCrossReferenceSearchMock patientCrossReferenceSearchMock;

    /**
     * Initialize implementations to be tested before each test.
     */
    @BeforeEach
    public void setUp() {
        this.patientCrossReferenceSearchMock = new PatientCrossReferenceSearchMock();
        this.crossReferenceSearchProcessingService = new CrossReferenceSearchProcessingService(patientCrossReferenceSearchMock);
    }


    /**
     * Test Processing a X-ref search, error case using no input in ProcessRequest
     */
    @Test
    public void process_no_input() {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_SEARCH_OPERATION);
        assertThrows(IllegalArgumentException.class, () -> crossReferenceSearchProcessingService.processPatientCrossReferenceSearch(processRequest));
    }

    /**
     * Test Processing a X-ref search, error case using too much input in ProcessRequest
     */
    @Test
    public void process_with_multiple_inputs() {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_SEARCH_OPERATION);

        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("identifierSystem");
        sourceIdentifier.setValue("identifierValue");
        EntityIdentifier sourceIdentifier2 = new EntityIdentifier();
        sourceIdentifier2.setSystemIdentifier("identifierSystem2");
        sourceIdentifier2.setValue("identifierValue2");

        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_NAME, sourceIdentifier));
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_TARGET_NAME, new ArrayList<String>()));
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_NAME, sourceIdentifier2));
        } catch (MappingException e) {
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }
        assertThrows(IllegalArgumentException.class, () -> crossReferenceSearchProcessingService.processPatientCrossReferenceSearch(processRequest));
    }

    /**
     * Test Processing a X-ref search, error case using a wrong input name in ProcessRequest
     */
    @Test
    public void process_with_wrong_input_name() {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_SEARCH_OPERATION);
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("identifierSystem");
        sourceIdentifier.setValue("identifierValue");

        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent("Bad input name", sourceIdentifier));
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_TARGET_NAME, new ArrayList<String>()));

        } catch (MappingException e) {
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }
        assertThrows(IllegalArgumentException.class, () -> crossReferenceSearchProcessingService.processPatientCrossReferenceSearch(processRequest));
    } /**
     * Test Processing a X-ref search, error case using a wrong input name in ProcessRequest
     */
    @Test
    public void process_with_wrong_input_name2() {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_SEARCH_OPERATION);
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("identifierSystem");
        sourceIdentifier.setValue("identifierValue");

        List<String> strings =new ArrayList<>();
        strings.add("test");
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent("Bad input name", sourceIdentifier));
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_TARGET_NAME, strings));

        } catch (MappingException e) {
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }
        assertThrows(IllegalArgumentException.class, () -> crossReferenceSearchProcessingService.processPatientCrossReferenceSearch(processRequest));
    }

    /**
     * Test Processing a X-ref search, error case using _null_result in ProcessResponse
     */
    @Test
    public void process_with_null_result() {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_SEARCH_OPERATION);
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("nullResult");
        sourceIdentifier.setValue("identifierValue");
        List<String> targetDomains = new ArrayList<>();
        targetDomains.add("test");

        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_NAME, sourceIdentifier));
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_TARGET_NAME, targetDomains));

        } catch (MappingException e) {
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }
        assertThrows(IllegalArgumentException.class, () -> crossReferenceSearchProcessingService.processPatientCrossReferenceSearch(processRequest));
    }

    /**
     * Test Processing a X-ref search, error case using _null_result in ProcessResponse
     */
    @Test
    public void process_with_search_exception() {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_SEARCH_OPERATION);
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("Exception");
        sourceIdentifier.setValue("identifierValue");
        List<String> targetDomains = new ArrayList<>();
        targetDomains.add("test");
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_NAME, sourceIdentifier));
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_TARGET_NAME, targetDomains));
        } catch (MappingException e) {
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }
        ProcessResponse processResponse = crossReferenceSearchProcessingService.processPatientCrossReferenceSearch(processRequest);

        assertNotNull(processResponse, "ProcessResponse shall not be null !");
        assertNotNull(processResponse.getReport(), "ProcessResponse.report shall not be null !");
        assertEquals(TestResultType.FAILURE, processResponse.getReport().getResult(), "Result of the processing shall be success !");
        assertNotNull(processResponse.getReport().getReports(), "Report shall contain error");
        assertNotNull(processResponse.getReport().getReports().getInfoOrWarningOrError(), "Report shall contain error");
        assertEquals(1, processResponse.getReport().getReports().getInfoOrWarningOrError().size(), "Report shall contain a single error");
        assertNotNull(processResponse.getReport().getReports().getInfoOrWarningOrError().get(0), "Report shall contain error");
        assertNotNull(((BAR) processResponse.getReport().getReports().getInfoOrWarningOrError().get(0).getValue()).getDescription(),
                "Report shall contain error with valued description !");
    }


    /**
     * Test Processing a X-ref search, nominal case.
     */
    @Test
    public void process() {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_SEARCH_OPERATION);
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("identifierSystem");
        sourceIdentifier.setValue("identifierValue");
        List<String> targetDomains = new ArrayList<>();
        targetDomains.add("test");
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_NAME, sourceIdentifier));
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_TARGET_NAME,targetDomains ));
        } catch (MappingException e) {
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        ProcessResponse processResponse = crossReferenceSearchProcessingService.processPatientCrossReferenceSearch(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
        assertEquals(1, processResponse.getOutput().size(), "Response shall contain a unique output which is the value of the" +
                " UUID assigned to the fed patient.");
        assertEquals("object", processResponse.getOutput().get(0).getType(), "Returned UUID shall be stored in a string GITB AnyContent");
        assertEquals(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_OUTPUT_NAME, processResponse.getOutput().get(0).getName(),
                "Name of returned GITB AnyContent shall be 'XRefPatient'");
        assertEquals(ValueEmbeddingEnumeration.BASE_64, processResponse.getOutput().get(0).getEmbeddingMethod(), "Embedding method of returned " +
                "GITB AnyContent shall be 'STRING'");
        assertNotNull(processResponse.getReport(), "ProcessResponse.report shall not be null !");
        assertEquals(TestResultType.SUCCESS, processResponse.getReport().getResult(), "Result of the processing shall be success !");
    }


}