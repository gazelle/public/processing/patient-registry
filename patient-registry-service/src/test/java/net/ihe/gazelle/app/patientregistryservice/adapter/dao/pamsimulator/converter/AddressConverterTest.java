package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.converter;

import net.ihe.gazelle.app.patientregistryapi.business.Address;
import net.ihe.gazelle.app.patientregistryapi.business.AddressUse;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.AddressTypeDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientAddressDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test for the converter of Address objects from business to DB model and back.
 */
class AddressConverterTest {

    /**
     * Map a business Address to DB model
     */
    @Test
    public void mapAddressPatientTest() {
        Address address = new Address();
        PatientDB patient = new PatientDB();

        PatientAddressDB patAddress = AddressConverter.toAddressDB(address,patient);

        assertEquals(patient, patAddress.getPatient());
    }

    /**
     * Map the PostalCode of an Address to DB model
     */
    @Test
    public void mapAddressPostalCodeTest() {
        Address address = new Address();
        String postalCode = "PostalCode";
        address.setPostalCode(postalCode);

        PatientAddressDB patAddress = AddressConverter.toAddressDB(address,null);

        assertEquals(postalCode, patAddress.getZipCode());
    }

    /**
     * Map the City of an Address to DB model
     */
    @Test
    public void mapAddressCityTest() {
        Address address = new Address();
        String city = "City";
        address.setCity(city);

        PatientAddressDB patAddress = AddressConverter.toAddressDB(address,null);

        assertEquals(city, patAddress.getCity());
    }

    /**
     * Map the Country of an Address to DB model
     */
    @Test
    public void mapAddressCountryTest() {
        Address address = new Address();
        String country = "Country";
        address.setCountryIso3(country);

        PatientAddressDB patAddress = AddressConverter.toAddressDB(address,null);

        assertEquals(country, patAddress.getCountryCode());
    }

    /**
     * Map the State of an Address to DB model
     */
    @Test
    public void mapAddressStateTest() {
        Address address = new Address();
        String state = "State";
        address.setState(state);

        PatientAddressDB patAddress = AddressConverter.toAddressDB(address,null);

        assertEquals(state, patAddress.getState());
    }

    /**
     * Map the single line of an Address to DB model
     */
    @Test
    public void mapAddressOneLineTest() {
        Address address = new Address();
        String line = "line";
        address.addLine(line);

        PatientAddressDB patAddress = AddressConverter.toAddressDB(address,null);

        assertEquals(line, patAddress.getAddressLine());
    }

    /**
     * Map two lines of an Address to DB model
     */
    @Test
    public void mapAddressTwoLinesTest() {
        Address address = new Address();
        String line1 = "line1";
        address.addLine(line1);
        String line2 = "line2";
        address.addLine(line2);

        PatientAddressDB patAddress = AddressConverter.toAddressDB(address,null);

        assertEquals(line1+line2, patAddress.getAddressLine());
    }

    /**
     * Map the AddressUse of a Main Address to DB model
     */
    @Test
    public void mapAddressUseMainAddressTest() {
        Address address = new Address();
        address.setUse(AddressUse.PRIMARY_HOME);

        PatientAddressDB patAddress = AddressConverter.toAddressDB(address,null);

        assertTrue(patAddress.isMainAddress());
        assertEquals(AddressTypeDB.HOME, patAddress.getAddressType());
    }

    /**
     * Map the AddressUse of a BirthPlace Address to DB model
     */
    @Test
    public void mapAddressUseBirthPlaceTest() {
        Address address = new Address();
        address.setUse(AddressUse.BIRTH_PLACE);

        PatientAddressDB patAddress = AddressConverter.toAddressDB(address,null);

        assertFalse(patAddress.isMainAddress());
        assertEquals(AddressTypeDB.BIRTH, patAddress.getAddressType());
    }

    /**
     * Map the AddressUse of a Bad Address to DB model
     */
    @Test
    public void mapAddressUseBadTest() {
        Address address = new Address();
        address.setUse(AddressUse.BAD);

        PatientAddressDB patAddress = AddressConverter.toAddressDB(address, null);

        assertFalse(patAddress.isMainAddress());
        assertEquals(AddressTypeDB.BAD_ADDRESS, patAddress.getAddressType());
    }

    /**
     * Map the AddressUse of an Office Address to DB model
     */
    @Test
    public void mapAddressUseOfficeTest() {
        Address address = new Address();
        address.setUse(AddressUse.WORK);

        PatientAddressDB patAddress = AddressConverter.toAddressDB(address, null);

        assertFalse(patAddress.isMainAddress());
        assertEquals(AddressTypeDB.OFFICE, patAddress.getAddressType());
    }

    /**
     * Map the AddressUse of a Legal Address to DB model
     */
    @Test
    public void mapAddressUseLegalTest() {
        Address address = new Address();
        address.setUse(AddressUse.LEGAL);

        PatientAddressDB patAddress = AddressConverter.toAddressDB(address, null);

        assertFalse(patAddress.isMainAddress());
        assertEquals(AddressTypeDB.LEGAL, patAddress.getAddressType());
    }

    /**
     * Map the AddressUse of a Mailing Address to DB model
     */
    @Test
    public void mapAddressUseMailingTest() {
        Address address = new Address();
        address.setUse(AddressUse.MAILING);

        PatientAddressDB patAddress = AddressConverter.toAddressDB(address, null);

        assertFalse(patAddress.isMainAddress());
        assertEquals(AddressTypeDB.MAILING, patAddress.getAddressType());
    }

    /**
     * Map the AddressUse of a BirthResidence Address to DB model
     */
    @Test
    public void mapAddressUseBirthResidenceTest() {
        Address address = new Address();
        address.setUse(AddressUse.BIRTH_RESIDENCE);

        PatientAddressDB patAddress = AddressConverter.toAddressDB(address, null);

        assertFalse(patAddress.isMainAddress());
        assertEquals(AddressTypeDB.BIRTH_RESIDENCE, patAddress.getAddressType());
    }

    /**
     * Map the AddressUse of a Country of Origin Address to DB model
     */
    @Test
    public void mapAddressUseCountryOfOriginTest() {
        Address address = new Address();
        address.setUse(AddressUse.COUNTRY_OF_ORIGIN);

        PatientAddressDB patAddress = AddressConverter.toAddressDB(address, null);

        assertFalse(patAddress.isMainAddress());
        assertEquals(AddressTypeDB.ORIGIN, patAddress.getAddressType());
    }

    /**
     * Map Home AddressDB Type to Address Use
     */
    @Test
    public void mapHomeAddressDBToAddress() {
        PatientAddressDB addressDB = new PatientAddressDB();
        addressDB.setAddressType(AddressTypeDB.HOME);

        Address address = AddressConverter.toAddress(addressDB);

        assertEquals(AddressUse.HOME, address.getUse());
    }

    /**
     * Map Bad AddressDB Type to Address Use
     */
    @Test
    public void mapBadAddressDBToAddress() {
        PatientAddressDB addressDB = new PatientAddressDB();
        addressDB.setAddressType(AddressTypeDB.BAD_ADDRESS);

        Address address = AddressConverter.toAddress(addressDB);

        assertEquals(AddressUse.BAD, address.getUse());
    }

    /**
     * Map Office AddressDB Type to Address Use
     */
    @Test
    public void mapOfficeAddressDBToAddress() {
        PatientAddressDB addressDB = new PatientAddressDB();
        addressDB.setAddressType(AddressTypeDB.OFFICE);

        Address address = AddressConverter.toAddress(addressDB);

        assertEquals(AddressUse.WORK, address.getUse());
    }

    /**
     * Map Legal AddressDB Type to Address Use
     */
    @Test
    public void mapLegalAddressDBToAddress() {
        PatientAddressDB addressDB = new PatientAddressDB();
        addressDB.setAddressType(AddressTypeDB.LEGAL);

        Address address = AddressConverter.toAddress(addressDB);

        assertEquals(AddressUse.LEGAL, address.getUse());
    }

    /**
     * Map Mailing AddressDB Type to Address Use
     */
    @Test
    public void mapMailingAddressDBToAddress() {
        PatientAddressDB addressDB = new PatientAddressDB();
        addressDB.setAddressType(AddressTypeDB.MAILING);

        Address address = AddressConverter.toAddress(addressDB);

        assertEquals(AddressUse.MAILING, address.getUse());
    }

    /**
     * Map Birth AddressDB Type to Address Use
     */
    @Test
    public void mapBirthAddressDBToAddress() {
        PatientAddressDB addressDB = new PatientAddressDB();
        addressDB.setAddressType(AddressTypeDB.BIRTH);

        Address address = AddressConverter.toAddress(addressDB);

        assertEquals(AddressUse.BIRTH_PLACE, address.getUse());
    }

    /**
     * Map BirthResidence AddressDB Type to Address Use
     */
    @Test
    public void mapBirthResidenceAddressDBToAddress() {
        PatientAddressDB addressDB = new PatientAddressDB();
        addressDB.setAddressType(AddressTypeDB.BIRTH_RESIDENCE);

        Address address = AddressConverter.toAddress(addressDB);

        assertEquals(AddressUse.BIRTH_RESIDENCE, address.getUse());
    }

    /**
     * Map Origin AddressDB Type to Address Use
     */
    @Test
    public void mapOriginAddressDBToAddress() {
        PatientAddressDB addressDB = new PatientAddressDB();
        addressDB.setAddressType(AddressTypeDB.ORIGIN);

        Address address = AddressConverter.toAddress(addressDB);

        assertEquals(AddressUse.COUNTRY_OF_ORIGIN, address.getUse());
    }
}