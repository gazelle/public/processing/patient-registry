package net.ihe.gazelle.app.patientregistryservice.application.exceptions;

import io.qameta.allure.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;


@Feature("Coverage on exception")
@Story("CreationCrossReferenceException")
class CreationCrossReferenceExceptionTest {
    /**
     * Dummy Test for coverage
     *
     * @throws CreationCrossReferenceException testException
     */
    @Test
    @Description(" CreationCrossReferenceException")
    @Severity(SeverityLevel.MINOR)
    void generateException() {
        assertThrows(CreationCrossReferenceException.class, () -> {
            throw new CreationCrossReferenceException("Test");
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws CreationCrossReferenceException testException
     */
    @Test
    @Description(" CreationCrossReferenceException")
    @Severity(SeverityLevel.MINOR)
    void generateException1() {
        assertThrows(CreationCrossReferenceException.class, () -> {
            throw new CreationCrossReferenceException("Test", new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws CreationCrossReferenceException testException
     */
    @Test
    @Description(" CreationCrossReferenceException")
    @Severity(SeverityLevel.MINOR)
    void generateException2() {
        assertThrows(CreationCrossReferenceException.class, () -> {
            throw new CreationCrossReferenceException(new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws CreationCrossReferenceException testException
     */
    @Test
    @Description(" CreationCrossReferenceException")
    @Severity(SeverityLevel.MINOR)
    void generateException4() {
        assertThrows(CreationCrossReferenceException.class, () -> {
            throw new CreationCrossReferenceException("Test", new IllegalArgumentException("test"), false, true);
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws CreationCrossReferenceException testException
     */
    @Test
    @Description(" CreationCrossReferenceException")
    @Severity(SeverityLevel.MINOR)
    void generateException3() {
        assertThrows(CreationCrossReferenceException.class, () -> {
            throw new CreationCrossReferenceException();
        });
    }

}