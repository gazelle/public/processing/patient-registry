package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.converter;

import net.ihe.gazelle.app.patientregistryapi.business.PersonName;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test Conversion on Names from business to Database model or the opposite
 */
class NameConverterTest {

    /**
     * test family name mapping
     */
    @Test
    public void mapNameFamilyNameTest() {
        PersonName name = new PersonName();
        String family = "family";
        name.setFamily(family);
        PatientDB patient = new PatientDB();

        NameConverter.mapNameToPatientDB(name,patient);

        assertEquals(family, patient.getLastName(), "family name should be mapped to last name");
    }

    /**
     * test given names mapping
     */
    @Test
    public void mapNameGivenNameTest() {
        PersonName name = new PersonName();
        String given1 = "given1";
        name.addGiven(given1);
        String given2 = "given2";
        name.addGiven(given2);
        String given3 = "given3";
        name.addGiven(given3);
        PatientDB patient = new PatientDB();

        NameConverter.mapNameToPatientDB(name,patient);

        assertEquals(given1, patient.getFirstName(), "first given name should be mapped to first name");
        assertEquals(given2, patient.getSecondName(), "second given name should be mapped to second name");
        assertEquals(given3, patient.getThirdName(), "third given name should be mapped to third name");
    }
}