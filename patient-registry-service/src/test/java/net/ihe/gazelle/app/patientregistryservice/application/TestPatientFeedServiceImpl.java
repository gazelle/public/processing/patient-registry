package net.ihe.gazelle.app.patientregistryservice.application;

import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedException;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedService;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;

/**
 * Test implementation of Patient Feed service.
 */

public class TestPatientFeedServiceImpl implements PatientFeedService {

    public static final String UUID = "test012-uuid";
    public static final Patient PATIENT_TEST = new Patient();
    private FeedResult feedResult;

    /**
     * Public constructor, setting the default response to a correct UUID.
     */
    public TestPatientFeedServiceImpl() {
        this.feedResult = FeedResult.OK;
    }


    /**
     * {@inheritDoc}
     * <p>
     * OK : will return a valid UUID.
     * KO : will throw a {@link PatientFeedException}
     * NULL : will return null.
     */
    @Override
    public String createPatient(Patient patient) throws PatientFeedException {

        switch (feedResult) {
            case OK:
                return UUID;
            case KO:
                throw new PatientFeedException("The Patient is not created");
            case NULL:
                return null;

        }
        return null;
    }

    /**
     *
     * @param patient Patient to update to the service
     * @return
     * @throws PatientFeedException
     */

    @Override
    public Patient updatePatient(Patient patient, EntityIdentifier identifier) throws PatientFeedException {

        switch (feedResult){
            case OK:
                return PATIENT_TEST;
            case KO :
                throw new PatientFeedException("The Patient is not updated");
            case NULL:
                return null;
        }
        return null;
    }

    @Override
    public String mergePatient(String uuidOfOriginalPatient, String uuidOfDuplicatedPatient) throws PatientFeedException {

        switch (feedResult){
            case OK:
                return UUID;
            case KO :
                throw new PatientFeedException("The Patient is not merged");
            case NULL:
                return null;
        }
        return null;
    }

    @Override
    public boolean deletePatient(String uuid) throws PatientFeedException {
        switch (uuid){
            case "123":
                return true;
            case "456" :
                throw new PatientFeedException("The Patient is not deleted");
            case "789" :
                return false;
            default :
                throw new PatientFeedException();
        }

    }

    @Override
    public EntityIdentifier deletePatient(EntityIdentifier identifier) throws PatientFeedException {
        return null;
    }

    /**
     * Setter for the feedResult property.
     *
     * @param feedResult value of the feedResult to set
     */
    public void setFeedResult(FeedResult feedResult) {
        this.feedResult = feedResult;
    }

    /**
     * Enumeration of all response types this test implementation can return.
     */
    public enum FeedResult {
        OK,
        KO,
        NULL
    }

}
