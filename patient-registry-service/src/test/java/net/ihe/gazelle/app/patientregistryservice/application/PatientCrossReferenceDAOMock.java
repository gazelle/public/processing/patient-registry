package net.ihe.gazelle.app.patientregistryservice.application;

import net.ihe.gazelle.app.patientregistryapi.application.SearchCrossReferenceException;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.HierarchicDesignatorDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientIdentifierDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.references.CrossReferenceDB;
import net.ihe.gazelle.app.patientregistryservice.application.dao.PatientCrossReferenceDAO;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.CreationCrossReferenceException;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.DeletionCrossReferenceException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PatientCrossReferenceDAOMock implements PatientCrossReferenceDAO {

    @Override
    public PatientDB searchForPatientDBWithSourceIdentifier(String systemID, String patientIDInSystem) throws SearchCrossReferenceException {
        switch (patientIDInSystem) {
            case "UnexpectedException":
                throw new IllegalArgumentException("Exception");
            case "SystemNotFound":
                throw new SearchCrossReferenceException("Error in the sourceIdentifier : System does not exit");
            case "PatientIdentifiernotfound":
                throw new SearchCrossReferenceException("Error in the sourceIdentifier : it does not match any identity");
            case "PatientNotFound":
                throw new SearchCrossReferenceException("Error in the sourceIdentifier : it does not match any Patient");
            case "NoXCrossrefFound":
                return createPatientDB1();
            case "1CrossFound":
                return createPatientDB2();
            case "3CrossFound":
                return createPatientDB3();
            case "searchTargetDomainFound":
                return createPatientDB4();
            case "NoCrossRef":
                PatientDB patientDB = new PatientDB();
                patientDB.setUuid("NoCrossRef");
                patientDB.setFirstName("testFirstName");
                patientDB.setLastName("testLastName");
                ;
                patientDB.setId(1);
                return patientDB;
            case "No uuid":
                PatientDB patientDB5 = new PatientDB();
                patientDB5.setFirstName("testFirstName");
                patientDB5.setLastName("testLastName");
                patientDB5.setId(1);
                return patientDB5;

        }


        return null;
    }

    @Override
    public CrossReferenceDB searchForPatientAliasesWithPatientDB(Integer patientDBPixReferenceId) throws SearchCrossReferenceException {
        switch (patientDBPixReferenceId) {
            case 1:
                throw new SearchCrossReferenceException("Search error : No unique X-ref for given Patient");
            case 2:
                return crossReferenceDBS(createPatientDB2(), "");
            case 3:
                return multipleCrossReferenceDBS(createPatientDB3());
            case 4:
                return multipleCrossReferenceDBS(createPatientDB4());
        }
        return null;
    }


    @Override
    public CrossReferenceDB createCrossReference(CrossReferenceDB crossReferenceDB) throws CreationCrossReferenceException {
        return null;
    }

    @Override
    public void deleteCrossReference(Integer crossReferenceId) throws DeletionCrossReferenceException {

    }


    private CrossReferenceDB multipleCrossReferenceDBS(PatientDB patient) {
        CrossReferenceDB crossReferenceDBS = new CrossReferenceDB();

        List<PatientDB> patientDBS = new ArrayList<>();
        patientDBS.addAll(crossReferenceDBS(patient, "1").getPatients());
        patientDBS.addAll(crossReferenceDBS(patient, "2").getPatients());
        patientDBS.addAll(crossReferenceDBS(patient, "3").getPatients());
        crossReferenceDBS.setPatients(patientDBS);
        return crossReferenceDBS;
    }

    private CrossReferenceDB crossReferenceDBS(PatientDB patient, String numb) {
        String lastModifier = "test";
        Date lastModifierDateTime = new Date();
        CrossReferenceDB patientAliases = new CrossReferenceDB();
        patientAliases.setLastChanged(lastModifierDateTime);
        patientAliases.setLastModifier(lastModifier);


        HierarchicDesignatorDB hierarchicDesignatorDB = new HierarchicDesignatorDB();
        hierarchicDesignatorDB.setUniversalID("targetDomain");
        PatientIdentifierDB patientIdentifierDB = new PatientIdentifierDB();
        patientIdentifierDB.setIdentifier("patientIdentifierDBX1");
        patientIdentifierDB.setDomain(hierarchicDesignatorDB);
        PatientIdentifierDB patientIdentifierDB2 = new PatientIdentifierDB();
        patientIdentifierDB2.setIdentifier("patientIdentifierDBX2");
        List<PatientIdentifierDB> patientIdentifierDBS = new ArrayList<>();
        patientIdentifierDBS.add(patientIdentifierDB);
        patientIdentifierDBS.add(patientIdentifierDB2);


        PatientDB patientDB = new PatientDB();
        patientDB.setUuid("Retrieved1" + numb);
        patientDB.setFirstName("testFirstName");
        patientDB.setLastName("testLastName");
        patientDB.setPatientIdentifiers(patientIdentifierDBS);

        PatientDB patientDB2 = new PatientDB();
        patientDB2.setUuid("Retrieved2" + numb);
        patientDB2.setFirstName("testFirstName");
        patientDB2.setLastName("testLastName");

        PatientDB patientDB3 = new PatientDB();
        patientDB3.setUuid("Retrieved3" + numb);
        patientDB3.setFirstName("testFirstName");
        patientDB3.setLastName("testLastName");

        List<PatientDB> patientDBS = new ArrayList<>();
        patientDBS.add(patient);
        patientDBS.add(patientDB);
        patientDBS.add(patientDB2);
        patientDBS.add(patientDB3);
        patientAliases.setPatients(patientDBS);
        return patientAliases;
    }

    private PatientDB createPatientDB1() {
        PatientDB patientDB = new PatientDB();
        patientDB.setUuid("NoCrossRef");
        patientDB.setFirstName("testFirstName");
        patientDB.setLastName("testLastName");
        CrossReferenceDB crossReferenceDB = new CrossReferenceDB();
        crossReferenceDB.setId(1);
        patientDB.setPixReference(crossReferenceDB);
        patientDB.setId(1);
        return patientDB;
    }

    private PatientDB createPatientDB2() {
        PatientDB patientDB = new PatientDB();
        patientDB.setUuid("original");
        patientDB.setFirstName("testFirstName");
        patientDB.setLastName("testLastName");
        CrossReferenceDB crossReferenceDB = new CrossReferenceDB();
        crossReferenceDB.setId(2);
        patientDB.setPixReference(crossReferenceDB);
        patientDB.setId(2);
        return patientDB;
    }

    private PatientDB createPatientDB3() {
        PatientDB patientDB3 = new PatientDB();
        patientDB3.setUuid("searchTargetDomainFound");
        patientDB3.setFirstName("testFirstName");
        patientDB3.setLastName("testLastName");
        patientDB3.setId(3);
        CrossReferenceDB crossReferenceDB = new CrossReferenceDB();
        crossReferenceDB.setId(3);
        patientDB3.setPixReference(crossReferenceDB);
        PatientIdentifierDB patientIdentifierDB = new PatientIdentifierDB();
        patientIdentifierDB.setIdentifier("patientIdentifierDB1");
        PatientIdentifierDB patientIdentifierDB2 = new PatientIdentifierDB();
        patientIdentifierDB2.setIdentifier("patientIdentifierDB2");
        List<PatientIdentifierDB> patientIdentifierDBS = new ArrayList<>();
        patientIdentifierDBS.add(patientIdentifierDB);
        patientIdentifierDBS.add(patientIdentifierDB2);
        patientDB3.setPatientIdentifiers(patientIdentifierDBS);
        return patientDB3;
    }

    private PatientDB createPatientDB4() {
        PatientDB patientDB4 = new PatientDB();
        patientDB4.setUuid("targetDomain");
        CrossReferenceDB crossReferenceDB = new CrossReferenceDB();
        crossReferenceDB.setId(4);
        patientDB4.setPixReference(crossReferenceDB);
        return patientDB4;
    }

}
