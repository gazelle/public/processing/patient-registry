package net.ihe.gazelle.app.patientregistryservice.adapter.ws;

import net.ihe.gazelle.app.patientregistryapi.application.PatientCrossReferenceSearch;
import net.ihe.gazelle.app.patientregistryapi.application.SearchCrossReferenceException;
import net.ihe.gazelle.app.patientregistryapi.application.UnrecognizedDomainException;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PatientAliases;
import net.ihe.gazelle.app.patientregistryapi.business.PersonName;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;

import java.util.List;

public class PatientCrossReferenceSearchMock implements PatientCrossReferenceSearch {
    @Override
    public PatientAliases search(EntityIdentifier sourceIdentifier,List<String>targetDomains) throws SearchCrossReferenceException {
        if (sourceIdentifier.getSystemIdentifier().equals("identifierSystem")) {
            PatientAliases patientAliases = new PatientAliases();
            patientAliases.setUuid("123456789");
            Patient patient = new Patient();
            PersonName personName = new PersonName();
            personName.setFamily("TestName");
            patient.addName(personName);
            patientAliases.addMember(patient);
            return patientAliases;
        } else if (sourceIdentifier.getSystemIdentifier().equals("nullResult")) {
            return null;
        } else {
            throw new SearchCrossReferenceException("Test exception");
        }

    }
}
