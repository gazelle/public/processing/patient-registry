package net.ihe.gazelle.app.patientregistryservice.application;

import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.HierarchicDesignatorDB;
import net.ihe.gazelle.app.patientregistryservice.application.dao.DomainDAO;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.DomainSearchException;

public class DomainServiceMock implements DomainDAO {
    @Override
    public HierarchicDesignatorDB retrieve(String domainIdentifier) {
        return null;
    }

    @Override
    public void delete(String domainIdentifier) {

    }

    @Override
    public boolean exist(String domainIdentifier) {
        if (domainIdentifier == null) {
            return false;
        }
        switch (domainIdentifier) {
            case "valid":
            case "targetDomain":
            case "Retrieved11":
            case "original3":
                return true;
        }
        return false;
    }

    @Override
    public void createDomain(String domainIdentifier, String domainName) {

    }

    @Override
    public Integer searchForHierarchicDesignator(String systemIdentifier) throws DomainSearchException {
        return null;
    }

}
