package net.ihe.gazelle.app.patientregistryservice.feature.feed.ut;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/**
 * Launcher for .features found in resources/feature/feed
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:feature/feed", glue = {"net.ihe.gazelle.app.patientregistryservice.feature.feed.ut"})
public class PatientFeedTest {
}
