package net.ihe.gazelle.app.patientregistryservice.adapter.ws;

import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedService;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFieldsMatcherService;
import net.ihe.gazelle.app.patientregistryapi.application.PatientRetrieveService;
import net.ihe.gazelle.app.patientregistryapi.application.PatientSearchService;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.TestEntityManagerProducer;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.DomainDAOImpl;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.IdentifierDAOImpl;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.PatientCrossReferenceDAOImpl;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.PatientDAOImpl;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search.*;
import net.ihe.gazelle.app.patientregistryservice.application.PatientCrossReferenceAlgorithm;
import net.ihe.gazelle.app.patientregistryservice.application.PatientFeedApplication;
import net.ihe.gazelle.app.patientregistryservice.application.PatientRetrieveApplication;
import net.ihe.gazelle.app.patientregistryservice.application.PatientSearchApplication;
import net.ihe.gazelle.app.patientregistryservice.application.dao.DomainDAO;
import net.ihe.gazelle.app.patientregistryservice.application.dao.IdentifierDAO;
import net.ihe.gazelle.app.patientregistryservice.application.dao.PatientCrossReferenceDAO;
import net.ihe.gazelle.app.patientregistryservice.application.dao.PatientDAO;
import org.junit.jupiter.api.Test;

import javax.enterprise.inject.Instance;
import javax.enterprise.inject.se.SeContainer;
import javax.enterprise.inject.se.SeContainerInitializer;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Tests that a container can instantiate beans correctly
 */
class ContainerInjectionTest {

    /**
     * Initialize {@link PatientProcessingService}
     */
    @Test
    void initializePatientProcessingService() {
        SeContainer container = initializeContainer();

        Instance<PatientProcessingService> lazyPSI = container.select(PatientProcessingService.class);
        PatientProcessingService patientProcessingService = lazyPSI.get();

        assertNotNull(patientProcessingService);
    }

    /**
     * Initialize {@link PatientFeedApplication}
     */
    @Test
    void initializePatientProcessingApplication() {
        SeContainer container = initializeContainer();

        Instance<PatientFeedApplication> lazyPSI = container.select(PatientFeedApplication.class);
        PatientFeedApplication patientProcessingService = lazyPSI.get();

        assertNotNull(patientProcessingService);
    }

    /**
     * Initialize {@link PatientSearchApplication}
     */
    @Test
    void initializePatientSearchApplication() {
        SeContainer container = initializeContainer();

        Instance<PatientSearchApplication> lazyPSI = container.select(PatientSearchApplication.class);
        PatientSearchApplication patientProcessingService = lazyPSI.get();

        assertNotNull(patientProcessingService);
    }

    /**
     * Initialize a {@link SeContainer} with all classes needed for the application to run with injection.
     *
     * @return the initialized container.
     */
    private SeContainer initializeContainer() {
        SeContainerInitializer initializer = SeContainerInitializer.newInstance()
                .disableDiscovery().addBeanClasses(PatientProcessingService.class,
                        PatientFeedService.class, PatientFeedApplication.class, PatientFeedProcessingService.class,
                        PatientSearchService.class, PatientSearchApplication.class, PatientSearchProcessingService.class,
                        PatientSearchDAO.class, PatientDAO.class, PatientDAOImpl.class,
                        PatientRetrieveService.class, PatientRetrieveApplication.class, PatientCrossReferenceAlgorithm.class,
                        PatientCrossReferenceDAO.class, PatientCrossReferenceDAOImpl.class,
                        DomainDAO.class, DomainDAOImpl.class,
                        IdentifierDAO.class, IdentifierDAOImpl.class,
                        TestEntityManagerProducer.class,
                        PatientSearchCriterionJPAMappingService.class,
                        PatientSearchResultJPAMappingService.class, PatientFieldsMatcherService.class, PatientFieldsMatcherServiceImpl.class, SearchQueryBuilderImpl.class);

        return initializer.initialize();
    }
}
