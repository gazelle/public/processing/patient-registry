package net.ihe.gazelle.app.patientregistryservice.feature.search.ut;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/**
 * Launcher for .features found in resources/feature/feed
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:feature/search", glue = {"net.ihe.gazelle.app.patientregistryservice.feature.search.ut"})
public class PatientSearchTest {
}
