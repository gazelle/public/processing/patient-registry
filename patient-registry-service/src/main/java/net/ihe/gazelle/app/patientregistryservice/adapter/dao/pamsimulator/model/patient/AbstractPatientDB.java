package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>AbstractPatientDB class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */

@MappedSuperclass
public abstract class AbstractPatientDB {

    @Id
    @GeneratedValue(generator = "pat_patient_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "alternate_first_name")
    private String alternateFirstName;

    @Column(name = "alternate_last_name")
    private String alternateLastName;

    @Column(name = "mother_maiden_name")
    private String motherMaidenName;

    @Column(name = "gender_code")
    @Enumerated(EnumType.STRING)
    private GenderCodeDB genderCode;

    @Column(name = "religion_code")
    private String religionCode;

    @Column(name = "race_code")
    private String raceCode;

    @Column(name = "date_of_birth")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfBirth;

    @Column(name = "country_code")
    private String countryCode;

    @Column(name = "character_set")
    private String characterSet;

    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @Column(name = "alternate_mothers_maiden_name")
    private String alternateMothersMaidenName;

    @Column(name = "second_name")
    private String secondName;

    @Column(name = "third_name")
    private String thirdName;

    @Column(name = "alternate_second_name")
    private String alternateSecondName;

    @Column(name = "alternate_third_name")
    private String alternateThirdName;

    @Column(name = "weight")
    private Integer weight;

    @Column(name="size")
    private Integer size;

    @OneToMany(mappedBy = "patient", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<PatientAddressDB> addressList;

    /**
     * Constructor
     */
    public AbstractPatientDB() {
        this.creationDate = new Date();
    }

    /**
     * <p>Getter for the field <code>firstName</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * <p>Setter for the field <code>firstName</code>.</p>
     *
     * @param firstName a {@link String} object.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * <p>Getter for the field <code>lastName</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * <p>Setter for the field <code>lastName</code>.</p>
     *
     * @param lastName a {@link String} object.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * <p>Getter for the field <code>motherMaidenName</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getMotherMaidenName() {
        return motherMaidenName;
    }

    /**
     * <p>Setter for the field <code>motherMaidenName</code>.</p>
     *
     * @param motherMaidenName a {@link String} object.
     */
    public void setMotherMaidenName(String motherMaidenName) {
        this.motherMaidenName = motherMaidenName;
    }

    /**
     * <p>Getter for the field <code>genderCode</code>.</p>
     *
     * @return a {@link String} object.
     */
    public GenderCodeDB getGenderCode() {
        return genderCode;
    }

    /**
     * <p>Setter for the field <code>genderCode</code>.</p>
     *
     * @param genderCode a {@link String} object.
     */
    public void setGenderCode(GenderCodeDB genderCode) {
        this.genderCode = genderCode;
    }

    /**
     * <p>Getter for the field <code>religionCode</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getReligionCode() {
        return religionCode;
    }

    /**
     * <p>Setter for the field <code>religionCode</code>.</p>
     *
     * @param religionCode a {@link String} object.
     */
    public void setReligionCode(String religionCode) {
        this.religionCode = religionCode;
    }

    /**
     * <p>Getter for the field <code>raceCode</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getRaceCode() {
        return raceCode;
    }

    /**
     * <p>Setter for the field <code>raceCode</code>.</p>
     *
     * @param raceCode a {@link String} object.
     */
    public void setRaceCode(String raceCode) {
        this.raceCode = raceCode;
    }

    /**
     * <p>Getter for the field <code>dateOfBirth</code>.</p>
     *
     * @return a {@link Date} object.
     */
    public Date getDateOfBirth() {
        if (dateOfBirth != null) {
            return (Date) this.dateOfBirth.clone();
        }
        return null;
    }

    /**
     * <p>Setter for the field <code>dateOfBirth</code>.</p>
     *
     * @param dateOfBirth a {@link Date} object.
     */
    public void setDateOfBirth(Date dateOfBirth) {
        if (dateOfBirth != null) {
            this.dateOfBirth = (Date) dateOfBirth.clone();
        } else {
            this.dateOfBirth = null;
        }
    }

    /**
     * <p>Getter for the field <code>countryCode</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * <p>Setter for the field <code>countryCode</code>.</p>
     *
     * @param countryCode a {@link String} object.
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }


    /**
     * <p>Getter for the field <code>characterSet</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getCharacterSet() {
        return characterSet;
    }

    /**
     * <p>Setter for the field <code>characterSet</code>.</p>
     *
     * @param characterSet a {@link String} object.
     */
    public void setCharacterSet(String characterSet) {
        this.characterSet = characterSet;
    }

    /**
     * <p>Setter for the field <code>creationDate</code>.</p>
     *
     * @param creationDate a {@link Date} object.
     */
    public void setCreationDate(Date creationDate) {
        if (creationDate != null) {
            this.creationDate = (Date) creationDate.clone();
        } else {
            this.creationDate = null;
        }
    }

    /**
     * <p>Getter for the field <code>creationDate</code>.</p>
     *
     * @return a {@link Date} object.
     */
    public Date getCreationDate() {
        if (creationDate != null) {
            return (Date) creationDate.clone();
        } else {
            return null;
        }
    }

    /**
     * <p>Getter for the field <code>alternateFirstName</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getAlternateFirstName() {
        return alternateFirstName;
    }

    /**
     * <p>Setter for the field <code>alternateFirstName</code>.</p>
     *
     * @param alternateFirstName a {@link String} object.
     */
    public void setAlternateFirstName(String alternateFirstName) {
        this.alternateFirstName = alternateFirstName;
    }

    /**
     * <p>Getter for the field <code>alternateLastName</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getAlternateLastName() {
        return alternateLastName;
    }

    /**
     * <p>Setter for the field <code>alternateLastName</code>.</p>
     *
     * @param alternateLastName a {@link String} object.
     */
    public void setAlternateLastName(String alternateLastName) {
        this.alternateLastName = alternateLastName;
    }

    /**
     * <p>Getter for the field <code>alternateMothersMaidenName</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getAlternateMothersMaidenName() {
        return alternateMothersMaidenName;
    }

    /**
     * <p>Setter for the field <code>alternateMothersMaidenName</code>.</p>
     *
     * @param alternateMothersMaidenName a {@link String} object.
     */
    public void setAlternateMothersMaidenName(String alternateMothersMaidenName) {
        this.alternateMothersMaidenName = alternateMothersMaidenName;
    }

    /**
     * <p>Getter for the field <code>secondName</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getSecondName() {
        return secondName;
    }

    /**
     * <p>Setter for the field <code>secondName</code>.</p>
     *
     * @param secondName a {@link String} object.
     */
    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    /**
     * <p>Getter for the field <code>thirdName</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getThirdName() {
        return thirdName;
    }

    /**
     * <p>Setter for the field <code>thirdName</code>.</p>
     *
     * @param thirdName a {@link String} object.
     */
    public void setThirdName(String thirdName) {
        this.thirdName = thirdName;
    }

    /**
     * <p>Getter for the field <code>alternateSecondName</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getAlternateSecondName() {
        return alternateSecondName;
    }

    /**
     * <p>Setter for the field <code>alternateSecondName</code>.</p>
     *
     * @param alternateSecondName a {@link String} object.
     */
    public void setAlternateSecondName(String alternateSecondName) {
        this.alternateSecondName = alternateSecondName;
    }

    /**
     * <p>Getter for the field <code>alternateThirdName</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getAlternateThirdName() {
        return alternateThirdName;
    }

    /**
     * <p>Setter for the field <code>alternateThirdName</code>.</p>
     *
     * @param alternateThirdName a {@link String} object.
     */
    public void setAlternateThirdName(String alternateThirdName) {
        this.alternateThirdName = alternateThirdName;
    }

    /**
     * <p>Getter for the field <code>addressList</code>.</p>
     *
     * @return a {@link List} object.
     * @since 2.2.0
     */
    public List<PatientAddressDB> getAddressList() {
        if (addressList == null) {
            addressList = new ArrayList<>();
        }
        return addressList;
    }

    /**
     * <p>Setter for the field <code>addressList</code>.</p>
     *
     * @param addressList a {@link List} object.
     * @since 2.2.0
     */
    public void setAddressList(List<PatientAddressDB> addressList) {
        this.addressList = addressList;
    }

    /**
     * <p>addPatientAddress.</p>
     *
     * @param address a {@link PatientAddressDB} object.
     * @since 2.2.0
     */
    public void addPatientAddress(PatientAddressDB address) {
        if (address != null) {
            getAddressList().add(address);
        }
    }

    /**
     * <p>removePatientAddress.</p>
     *
     * @param address a {@link PatientAddressDB} object.
     * @since 2.2.0
     */
    public void removePatientAddress(PatientAddressDB address) {
        if (address != null) {
            getAddressList().remove(address);
        }
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link Integer} object.
     * @since 2.2.0
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link Integer} object.
     * @since 2.2.0
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>weight</code>.</p>
     *
     * @return a {@link Integer} object.
     */
    public Integer getWeight() {
        return weight;
    }

    /**
     * <p>Setter for the field <code>weight</code>.</p>
     *
     * @param weight a {@link Integer} object.
     */
    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    /**
     * <p>Getter for the field <code>size</code>.</p>
     *
     * @return a {@link Integer} object.
     */
    public Integer getSize() {
        return size;
    }

    /**
     * <p>Setter for the field <code>size</code>.</p>
     *
     * @param size a {@link Integer} object.
     */
    public void setSize(Integer size) {
        this.size = size;
    }
}
