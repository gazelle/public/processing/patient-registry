package net.ihe.gazelle.app.patientregistryservice.adapter.ws;

import com.gitb.core.AnyContent;
import com.gitb.ps.ProcessRequest;
import com.gitb.ps.ProcessResponse;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import net.ihe.gazelle.app.patientregistryapi.adapter.ws.PatientProcessingWebserviceConstants;
import net.ihe.gazelle.app.patientregistryapi.application.PatientSearchService;
import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.lib.gitbutils.adapter.GITBAnyContentType;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperAnyContentToObject;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperObjectToAnyContent;
import net.ihe.gazelle.lib.gitbutils.adapter.MappingException;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Processing service for Patient Search requests.
 */
public class PatientSearchProcessingService {


    private MapperAnyContentToObject mapper;
    private PatientSearchService patientSearchService;

    /**
     * Constructor for the class with all needed parameters
     *
     * @param patientSearchService : Application service to feed a patient
     */
    @Inject
    public PatientSearchProcessingService(PatientSearchService patientSearchService) {
        this.mapper = new MapperAnyContentToObject();
        this.patientSearchService = patientSearchService;
    }

    /**
     * Process the Patient Search request.
     *
     * @param processRequest : GITB request received.
     * @return ProcessResponse corresponding to the received request
     */
    public ProcessResponse processPatientSearch(ProcessRequest processRequest) {
        List<Patient> patients;
        List<AnyContent> searchCriteria = processRequest.getInput();
        if (searchCriteria.size() != 1) {
            throw new IllegalArgumentException("ProcessRequest.input shall contain one and only one patient !");
        } else if (!PatientProcessingWebserviceConstants.PATIENT_SEARCH_CRITERIA_INPUT_NAME.equals(searchCriteria.get(0).getName())) {
            throw new IllegalArgumentException(String.format("ProcessRequest.input shall have the name '%s' for a %s operation !",
                    PatientProcessingWebserviceConstants.PATIENT_SEARCH_CRITERIA_INPUT_NAME, PatientProcessingWebserviceConstants.PATIENT_SEARCH_OPERATION));
        }
        try {
            patients = patientSearchService.search(mapper.getObject(searchCriteria.get(0), SearchCriteria.class));
            return createPatientSearchProcessResponse(patients);
        } catch (MappingException e) {
            throw new IllegalArgumentException("Cannot decode Request inputs as SearchCriteria !", e);
        } catch (SearchException e) {
            return ProcessResponseWithReportCreator.createFailedProcessResponse(e, searchCriteria.get(0));
        }
    }


    public ProcessResponse processPatientParametersSearch(ProcessRequest processRequest) {
        List<Patient> patients;
        List<SearchParameter> parameters = new ArrayList<>();
        if (processRequest.getInput().isEmpty()) {
            throw new IllegalArgumentException("ProcessRequest.input shall contain one and only one parameters list !");
        } else if (!PatientProcessingWebserviceConstants.PATIENT_PARAMETERS_SEARCH_INPUT_NAME.equals(processRequest.getInput().get(0).getName())) {
            throw new IllegalArgumentException(String.format("ProcessRequest.input shall have the name '%s' for a %s operation !",
                    PatientProcessingWebserviceConstants.PATIENT_PARAMETERS_SEARCH_INPUT_NAME, PatientProcessingWebserviceConstants.PATIENT_SEARCH_OPERATION));
        }
        try {
            for (AnyContent anyContent : processRequest.getInput()) {
                parameters.add(mapper.getObject(anyContent, SearchParameter.class));
            }
            patients = patientSearchService.parametrizedSearch(parameters);
            return createPatientSearchProcessResponse(patients);
        } catch (MappingException e) {
            throw new IllegalArgumentException("Cannot decode Request inputs as SearchParameters !", e);
        } catch (SearchException e) {
            return ProcessResponseWithReportCreator.createFailedProcessResponse(e, processRequest.getInput().get(0));
        }
    }

    /**
     * Create a {@link ProcessResponse} to a {@link ProcessRequest} sent to this service after a successful processing.
     * The response will contain a {@link TAR} report containing the result Status for the processing operation,
     * as well as a single Output containing the List of Patients returned.
     *
     * @param patients : List of {@link Patient} returned by the search service.
     * @return the process response to return to the WebService
     */
    private ProcessResponse createPatientSearchProcessResponse(List<Patient> patients) throws SearchException {
        AnyContent patientListAnyContent;
        ProcessResponse processResponse = new ProcessResponse();
        if (patients == null) {
            throw new IllegalArgumentException("Successful operation shall return a not null List of Patients as an output !");
        }
        try {
            patientListAnyContent = new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_LIST_OUTPUT_NAME, patients);
            if (patientListAnyContent == null) {
                patientListAnyContent = new AnyContent();
                patientListAnyContent.setName(PatientProcessingWebserviceConstants.PATIENT_LIST_OUTPUT_NAME);
                patientListAnyContent.setType(GITBAnyContentType.LIST.getGitbType());
            }
        } catch (MappingException e) {
            throw new SearchException("Search request response cannot be mapped to GITB AnyContent !", e);
        }
        processResponse.getOutput().add(patientListAnyContent);
        processResponse.setReport(ProcessResponseWithReportCreator.createReport(TestResultType.SUCCESS));
        return processResponse;
    }
}
