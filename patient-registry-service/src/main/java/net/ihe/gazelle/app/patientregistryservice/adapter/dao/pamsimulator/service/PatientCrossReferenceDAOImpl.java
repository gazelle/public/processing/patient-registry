package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service;

import net.ihe.gazelle.app.patientregistryapi.application.SearchCrossReferenceException;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.EntityManagerProducer;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.DesignatorTypeDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.HierarchicDesignatorDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientIdentifierDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.references.CrossReferenceDB;
import net.ihe.gazelle.app.patientregistryservice.application.dao.PatientCrossReferenceDAO;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.CreationCrossReferenceException;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.DeletionCrossReferenceException;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import java.util.List;

@Named("PatientCrossReferenceDAO")
public class PatientCrossReferenceDAOImpl implements PatientCrossReferenceDAO {

    private EntityManager entityManager;
    private static final GazelleLogger log = GazelleLoggerFactory.getInstance().getLogger(PatientCrossReferenceDAOImpl.class);

    //private Constructor
    private PatientCrossReferenceDAOImpl() {
    }

    /**
     * Constructor for the class. Takes an EntityManager as argument. It will be used to perform actions on Database.
     *
     * @param entityManager {@link EntityManager} to perform actions on Database.
     */
    @Inject
    public PatientCrossReferenceDAOImpl(@EntityManagerProducer.InjectEntityManager EntityManager entityManager) {
        if (entityManager == null) {
            throw new IllegalArgumentException("EntityManager cannot be null");
        }
        this.entityManager = entityManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PatientDB searchForPatientDBWithSourceIdentifier(String systemID, String patientIDInSystem) throws SearchCrossReferenceException {
        if (systemID == null || systemID.isBlank()) {
            throw new SearchCrossReferenceException("The system identifier from entityIdentifier cannot be null or empty");
        }
        if (patientIDInSystem == null || patientIDInSystem.isBlank()) {
            throw new SearchCrossReferenceException("The value from entityIdentifier cannot be null or empty");
        }

        List<HierarchicDesignatorDB> hierarchicDesignatorDBList = searchForHierarchicDesignator(systemID);
        String patientIdentifier = searchForPatientIdentifier(hierarchicDesignatorDBList, patientIDInSystem);
        return searchForPatient(patientIdentifier);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CrossReferenceDB searchForPatientAliasesWithPatientDB(Integer patientDBPixReferenceId) throws SearchCrossReferenceException {
        if (patientDBPixReferenceId == null) {
            throw new IllegalArgumentException("Null patientDBPixReferenceId parameter for search");
        }
        try {
            TypedQuery<CrossReferenceDB> queryForPatientAliases = entityManager.createNamedQuery("CrossReferenceDB.findByCrossRefId",
                    CrossReferenceDB.class);
            queryForPatientAliases.setParameter("id", patientDBPixReferenceId);
            return queryForPatientAliases.getSingleResult();
        } catch (NoResultException exception) {

            throw new SearchCrossReferenceException("Search error : No unique X-ref for given Patient", exception);
        }

    }


    @Override
    public CrossReferenceDB createCrossReference(CrossReferenceDB crossReferenceDB) throws CreationCrossReferenceException {
        if (crossReferenceDB == null) {
            throw new IllegalArgumentException("Null crossReference parameter for creation");
        }
        try {
            entityManager.merge(crossReferenceDB);
            entityManager.flush();
            log.info(String.format("XReference : %s  created", crossReferenceDB));
            return crossReferenceDB;
        } catch (PersistenceException exception) {
            throw new CreationCrossReferenceException("Persistence Error", exception);
        }
    }

    @Override
    public void deleteCrossReference(Integer crossReferenceId) throws DeletionCrossReferenceException {
        try {
            CrossReferenceDB crossReferenceDB = searchForPatientAliasesWithPatientDB(crossReferenceId);
            entityManager.remove(crossReferenceDB);
            log.info(String.format("crossReferenceDB : %s  deleted", crossReferenceDB));
            entityManager.flush();
        } catch (SearchCrossReferenceException exception) {
            log.warn(String.format("crossReference : %s not found or already deleted", crossReferenceId), exception);
        }
    }

    /**
     * Search for a HierarchicDesignatorDB given a system identifier
     *
     * @param systemIdentifier to be check
     * @return Id of the matching HierarchicDesignatorDB to the systemIdentifier
     * @throws SearchCrossReferenceException if an error occurred during the search
     */
    private List<HierarchicDesignatorDB> searchForHierarchicDesignator(String systemIdentifier) throws SearchCrossReferenceException {

        TypedQuery<HierarchicDesignatorDB> queryForAPatient = entityManager.createNamedQuery("HierarchicDesignatorDB.findByUniversalID",
                HierarchicDesignatorDB.class);
        queryForAPatient.setParameter("universalID", systemIdentifier);
        queryForAPatient.setParameter("givenUsage", DesignatorTypeDB.PATIENT_ID);
        List<HierarchicDesignatorDB> hierarchicDesignatorDBS = queryForAPatient.getResultList();
        if (hierarchicDesignatorDBS.isEmpty()) {
            throw new SearchCrossReferenceException("Error in the sourceIdentifier : System does not exist");
        }
        return queryForAPatient.getResultList();

    }

    /**
     * search for a PatientIdentifierDB given its HierarchicDesignatorDB  and its id_number
     *
     * @param hierarchicDesignatorDBList list of the system domain
     * @param idNumber                 of the patient in the system
     * @return PatientIdentifierDB
     * @throws SearchCrossReferenceException if an error occurred during the search
     */
    private String searchForPatientIdentifier(List<HierarchicDesignatorDB> hierarchicDesignatorDBList, String idNumber) throws SearchCrossReferenceException {
        for (HierarchicDesignatorDB hierarchicDesignatorDB : hierarchicDesignatorDBList) {
            try {
                TypedQuery<PatientIdentifierDB> queryForAPatient = entityManager.createNamedQuery("PatientIdentifierDB.findByHierarchicDomainAndId",
                        PatientIdentifierDB.class);
                queryForAPatient.setParameter("domain_id", hierarchicDesignatorDB.getId());
                queryForAPatient.setParameter("id_number", idNumber);
                return queryForAPatient.getSingleResult().getIdentifier();
            } catch (NoResultException exception) {
                log.warn("No Patient Identifier found for the given hierarchicDesignator and PatientID");
            }
        }
        throw new SearchCrossReferenceException("Error in the sourceIdentifier : it does not match any identity");
    }

    /**
     * Search for a PatientDB given its PatientIdentifierDB
     *
     * @param patientIdentifier of the target patient
     * @return PatientDB matching the patient identifier
     * @throws SearchCrossReferenceException if an error occurred during the search
     */
    private PatientDB searchForPatient(String patientIdentifier) throws SearchCrossReferenceException {
        try {
            TypedQuery<PatientDB> queryForAPatient = entityManager.createNamedQuery("PatientDB.findByPatientIdentifier", PatientDB.class);
            queryForAPatient.setParameter("patient_identifier", patientIdentifier);
            return queryForAPatient.getSingleResult();
        } catch (NoResultException exception) {
            throw new SearchCrossReferenceException("Error in the sourceIdentifier : it does not match any Patient");
        }
    }

}
