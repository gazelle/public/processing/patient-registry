package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient;

import javax.persistence.*;

/**
 * Created by aberge on 03/07/17.
 */

@Entity
@Table(schema = "public", name = "pat_patient_address", uniqueConstraints = @UniqueConstraint(columnNames = {"id"}))
@SequenceGenerator(name = "pat_patient_address_sequence", sequenceName = "pat_patient_address_id_seq", allocationSize = 1)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class RawAddressDB {

    @Id
    @GeneratedValue(generator = "pat_patient_address_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "address_type")
    private AddressTypeDB addressType;


    @Column(name = "country_code")
    private String countryCode;

    @Column(name = "street_number")
    private String streetNumber;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "city")
    private String city;

    @Column(name = "address_line")
    private String addressLine;

    @Column(name = "state")
    private String state;

    /**
     * <p>Constructor for PatientAddressDB.</p>
     */
    public RawAddressDB() {
        this.addressType = AddressTypeDB.HOME;
    }

    public RawAddressDB(RawAddressDB inPatientAddress){
        this.countryCode = inPatientAddress.getCountryCode();
        this.zipCode = inPatientAddress.getZipCode();
        this.state = inPatientAddress.getState();
        this.addressLine = inPatientAddress.getAddressLine();
        this.streetNumber = inPatientAddress.getStreetNumber();
        this.addressType = inPatientAddress.getAddressType();
        this.city = inPatientAddress.getCity();
    }

    public RawAddressDB(AddressTypeDB inAddressType){
        this.addressType = inAddressType;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>addressType</code>.</p>
     *
     * @return a {@link AddressTypeDB} object.
     */
    public AddressTypeDB getAddressType() {
        return addressType;
    }

    /**
     * <p>Setter for the field <code>addressType</code>.</p>
     *
     * @param addressType a {@link AddressTypeDB} object.
     */
    public void setAddressType(AddressTypeDB addressType) {
        this.addressType = addressType;
    }

    /**
     * <p>Getter for the field <code>countryCode</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * <p>Setter for the field <code>countryCode</code>.</p>
     *
     * @param countryCode a {@link String} object.
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * <p>Getter for the field <code>streetNumber</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getStreetNumber() {
        return streetNumber;
    }

    /**
     * <p>Setter for the field <code>streetNumber</code>.</p>
     *
     * @param streetNumber a {@link String} object.
     */
    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    /**
     * <p>Getter for the field <code>zipCode</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     * <p>Setter for the field <code>zipCode</code>.</p>
     *
     * @param zipCode a {@link String} object.
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     * <p>Getter for the field <code>city</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getCity() {
        return city;
    }

    /**
     * <p>Setter for the field <code>city</code>.</p>
     *
     * @param city a {@link String} object.
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * <p>Getter for the field <code>addressLine</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getAddressLine() {
        return addressLine;
    }

    /**
     * <p>Setter for the field <code>addressLine</code>.</p>
     *
     * @param addressLine a {@link String} object.
     */
    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    /**
     * <p>Getter for the field <code>state</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getState() {
        return state;
    }

    /**
     * <p>Setter for the field <code>state</code>.</p>
     *
     * @param state a {@link String} object.
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * <p>isBirthPlace.</p>
     *
     * @return a boolean.
     */
    public boolean isBirthPlace() {
        return AddressTypeDB.BIRTH.equals(this.addressType);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RawAddressDB)) {
            return false;
        }

        RawAddressDB that = (RawAddressDB) o;

        if (addressType != that.addressType) {
            return false;
        }
        if (countryCode != null ? !countryCode.equals(that.countryCode) : that.countryCode != null) {
            return false;
        }
        if (streetNumber != null ? !streetNumber.equals(that.streetNumber) : that.streetNumber != null) {
            return false;
        }
        if (zipCode != null ? !zipCode.equals(that.zipCode) : that.zipCode != null) {
            return false;
        }
        if (city != null ? !city.equals(that.city) : that.city != null) {
            return false;
        }
        if (addressLine != null ? !addressLine.equals(that.addressLine) : that.addressLine != null) {
            return false;
        }
        return state != null ? state.equals(that.state) : that.state == null;
    }

    @Override
    public int hashCode() {
        int result = addressType != null ? addressType.hashCode() : 0;
        result = 31 * result + (countryCode != null ? countryCode.hashCode() : 0);
        result = 31 * result + (streetNumber != null ? streetNumber.hashCode() : 0);
        result = 31 * result + (zipCode != null ? zipCode.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (addressLine != null ? addressLine.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        return result;
    }
}
