package net.ihe.gazelle.app.patientregistryservice.adapter.ws;

import com.gitb.core.AnyContent;
import com.gitb.ps.ProcessRequest;
import com.gitb.ps.ProcessResponse;
import com.gitb.tr.*;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.lib.annotations.Package;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.math.BigInteger;
import java.util.Date;

/**
 * Utility class that allow different processing services to generate the {@link ProcessResponse} to return.
 */
@Package
class ProcessResponseWithReportCreator {

    private static final GazelleLogger LOGGER = GazelleLoggerFactory.getInstance().getLogger(ProcessResponseWithReportCreator.class);

    /**
     * Private constructor to hide the implicit one.
     */
    private ProcessResponseWithReportCreator(){
    }

    /**
     * Create a {@link ProcessResponse} to a {@link ProcessRequest} sent to this service that throws an error.
     * The response will contain a {@link TAR} report containing the result Status for the processing operation as failure,
     * as well as the exception message.
     * @param exception : exception thrown during processing whose message will appear in report.
     * @param fedAnyContent : Any Content that was fed to the service
     * @return Response created from failed response
     */
    @Package
    static ProcessResponse createFailedProcessResponse(Exception exception, AnyContent fedAnyContent){
        ProcessResponse processResponse = new ProcessResponse();
        if (exception == null){
            throw new IllegalArgumentException("Failed operation shall return an error message in the report !");
        }
        processResponse.setReport(ProcessResponseWithReportCreator.createReport(exception, fedAnyContent));
        return processResponse;
    }

    /**
     * Create {@link TAR} report for the processing operation with the date and a status ("success" or "failure").
     * @param testResultType : result of the processing operation
     * @return a {@link TAR} report for the processing operation
     */
    @Package
    static TAR createReport(TestResultType testResultType){
        TAR testAssertionReport = new TAR();
        testAssertionReport.setResult(testResultType);
        try {
            testAssertionReport.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(new Date().toInstant().toString()));
        } catch (DatatypeConfigurationException e){
            LOGGER.warn("Error converting date to XMLGregorianCalendar !", e);
        }
        return testAssertionReport;
    }

    /**
     * Create {@link TAR} report for the processing operation with the date and a status ("success" or "failure"), as well
     * as the error message from the exception raised during the processing.
     * @param exception : the exception thrown during processing
     * @param fedAnyContent : any content sent to the processing, used for report context
     * @return a {@link TAR} report for the processing operation
     */
    @Package
    static TAR createReport(Exception exception, AnyContent fedAnyContent) {
        ObjectFactory objectFactory = new ObjectFactory();
        TAR report = createReport(TestResultType.FAILURE);

        report.setReports(new TestAssertionGroupReportsType());

        BAR itemContent = new BAR();
        itemContent.setDescription(exception.getMessage());
        report.getReports().getInfoOrWarningOrError().add(objectFactory.createTestAssertionGroupReportsTypeError(itemContent));

        report.setCounters(new ValidationCounters());
        report.getCounters().setNrOfAssertions(BigInteger.valueOf(0));
        report.getCounters().setNrOfWarnings(BigInteger.valueOf(0));
        report.getCounters().setNrOfErrors(BigInteger.valueOf(1));

        report.setContext(new AnyContent());
        report.getContext().getItem().add(fedAnyContent);
        return report;
    }
}
