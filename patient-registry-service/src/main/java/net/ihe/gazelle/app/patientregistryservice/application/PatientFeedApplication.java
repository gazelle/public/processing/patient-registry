package net.ihe.gazelle.app.patientregistryservice.application;

import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedException;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedService;
import net.ihe.gazelle.app.patientregistryapi.application.SearchCrossReferenceException;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.converter.PatientConverter;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.references.CrossReferenceDB;
import net.ihe.gazelle.app.patientregistryservice.application.dao.DomainDAO;
import net.ihe.gazelle.app.patientregistryservice.application.dao.PatientCrossReferenceDAO;
import net.ihe.gazelle.app.patientregistryservice.application.dao.PatientDAO;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.*;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Named("feedApplicationService")
public class PatientFeedApplication implements PatientFeedService {
    private static final GazelleLogger log = GazelleLoggerFactory.getInstance().getLogger(PatientFeedApplication.class);
    PatientDAO patientDAO;
    DomainDAO domainDao;

    PatientCrossReferenceDAO patientCrossReferenceDAO;


    /**
     * Default injection constructor for the class.
     *
     * @param patientDAO Data Access Object for {@link net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB}
     * @param domainDao  Data Access Object for
     *                   {@link net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.HierarchicDesignatorDB}
     */
    @Inject
    public PatientFeedApplication(@Named("PatientDAO") PatientDAO patientDAO,
                                  @Named("domainDAOService") DomainDAO domainDao,
                                  @Named("PatientCrossReferenceDAO") PatientCrossReferenceDAO patientCrossReferenceDAO) {
        this.patientDAO = patientDAO;
        this.domainDao = domainDao;
        this.patientCrossReferenceDAO = patientCrossReferenceDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String createPatient(Patient patient) throws PatientFeedException {
        log.info("Create Patient");
        try {
        PatientDB patientDB = PatientConverter.toPatientDB(patient);
        createIdentifiersDomainIfMissing(patient.getIdentifiers());
        if ((patient.getUuid() != null) && patient.getUuid().equals("PatientPIXmFeed")) {
            patientDB.setSimulatedActor("PAT_IDENTITY_X_REF_MGR");
            patientDB.setActor_id(7); // TODO : Remove when the the Patient Manager Model will be refactored and separated from Actor !!!
        }
        String uuid = generateUuid();
        patientDB.setUuid(uuid);

        patientDAO.createPatientDB(patientDB);

        if (PatientCrossReferenceAlgorithm.isPatientInvalidForXReferenceAlgorithm(patientDB)) {
            log.info(String.format("Patient is saved with uuid and not used for X-referencing  : %s", uuid));
            return uuid;
        }

        createOrUpdateCrossReferenceForPatient(patientDB);
        return uuid;
        } catch (PatientCreationException e) {
            throw new PatientFeedException("Cannot persist patient in DB", e);
        }

    }

    @Override
    //fixme: This implementation only update the first patient with the given identifier, THERE IS A HIGH RISK OF RANDOM BEHAVIOR
    public Patient updatePatient(Patient patient, EntityIdentifier identifier) throws  PatientFeedException {
        log.info("Update Patient");

        try {

            PatientDB found = patientDAO.readFirstPatientByIdentifier(identifier);

            if (found == null) {
                log.warn("Patient {}|{} not found, creating a new patient", identifier.getSystemIdentifier(), identifier.getValue());
                String uuid = createPatient(patient);
                patient.setUuid(uuid);
                return patient;
            }
            if(Boolean.FALSE.equals(found.getStillActive())){
                throw new PatientFeedException("Cannot update patient with identifier : " + identifier + " because it is inactive");
            }
            PatientDB patientDB = PatientConverter.toPatientDB(patient);
            createIdentifiersDomainIfMissing(patient.getIdentifiers());

            patientDB.setId(found.getId());
            patientDB.setActor_id(found.getActor_id());
            patientDB.setSimulatedActor(found.getSimulatedActor());


            patientDAO.updatePatientDB(patientDB);


            return patient;
        } catch (PatientUpdateException | PatientNotFoundException e) {
            throw new PatientFeedException(e);
        }

    }


    /** merge is directly done as "deactivate" from the PIXm point of view
     */
    @Override
    public String mergePatient(String uuidOfOriginalPatient, String uuidOfDuplicatedPatient) throws PatientFeedException {
        throw new PatientFeedException("Not implemented");
    }

    /**
     * Check if the given uuid has a valid format. Then search this uuid in DB.
     * Get all its crossReference if any, and delete them if they contain only this patient
     *
     * @param uuid of the patient
     * @return true if the deletion has been done. False if the uuid does not exist (Will be used in a future version where a history will be used)
     * @throws PatientFeedException When an  error occurred during deletion
     */
    @Override
    public boolean deletePatient(String uuid) throws PatientFeedException {
        if (StringUtils.isEmpty(uuid)) {
            throw new PatientFeedException("The uuid cannot be null or empty");
        }
        try {
            log.info("Start deletion process");
            patientDAO.deletePatient(uuid);
        } catch (PatientDeleteException exception) {
            if (exception.getCause().getClass().equals(PatientNotFoundException.class)) {
                log.warn(String.format("Patient with uuid : %s has already been deleted", uuid));
                return false;
            }
            throw new PatientFeedException("Cannot proceed to delete",exception);
        }
        log.info(String.format("Patient with uuid : %s has been deleted", uuid));
        return true;
    }

    /**
     * {@inheritDoc}
     */
    //Fixme: this implementation delete all patients with the given identifier, is it the correct behavior ?
    @Override
    public EntityIdentifier deletePatient(EntityIdentifier identifier) throws PatientFeedException {
        try {
            List<PatientDB> patientsToDelete = patientDAO.readAllPatientsByIdentifier(identifier);
            if(patientsToDelete != null && !patientsToDelete.isEmpty()){
                for(PatientDB patientDB : patientsToDelete){
                    patientDAO.deletePatientDB(patientDB);
                }
            }
            else {
                EntityIdentifier emptyIdentifier = new EntityIdentifier();
                emptyIdentifier.setSystemIdentifier("");
                emptyIdentifier.setValue("");
                return emptyIdentifier;
            }
        } catch (PatientNotFoundException | PatientDeleteException e) {
            throw new PatientFeedException(e);
        }
        return identifier;
    }

    /**
     * Create {@link net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.HierarchicDesignatorDB} if they do not exist.
     *
     * @param identifiers list of {@link EntityIdentifier} containing Domains to create if not existing.
     */
    private void createIdentifiersDomainIfMissing(List<EntityIdentifier> identifiers) {
        identifiers.forEach(identifier -> {
            if (identifier.getSystemIdentifier() != null && !isDomainOfIdentifierExisting(identifier)) {
                this.domainDao.createDomain(identifier.getSystemIdentifier(), identifier.getSystemName());
            }
        });
    }

    /**
     * Checks if a Domain exists.
     *
     * @param identifier {@link EntityIdentifier} defining the Domain System Identifier
     * @return true if the Domain exists, false otherwise.
     */
    private boolean isDomainOfIdentifierExisting(EntityIdentifier identifier) {
        return this.domainDao.exist(identifier.getSystemIdentifier());
    }

    /**
     * Generate a new UUID
     *
     * @return the literal value of the new UUID
     * @throws PatientFeedException if the UUID cannot be checked to be unique.
     */
    private String generateUuid() throws PatientFeedException {
        String uuid = UUID.randomUUID().toString();
        try {
            if (patientDAO.isPatientExisting(uuid)) {
                uuid = generateUuid();
            }
        } catch (PatientReadException e) {
            throw new PatientFeedException("Error checking generated UUID is free !", e);
        }

        return uuid;
    }


    /**
     * @param patientDB to proceed with in XReferencing
     * @throws PatientFeedException if an error occurred during XReferencing
     */
    private void createOrUpdateCrossReferenceForPatient(PatientDB patientDB) throws PatientFeedException {
        try {
            List<PatientDB> matchingPatients = patientDAO.retrieveMatchingPatients(patientDB);
            int crossReferenceDBID = PatientCrossReferenceAlgorithm.crossReferencing(patientDB, matchingPatients);
            CrossReferenceDB crossReferenceDB;
            if (crossReferenceDBID == 0) {
                crossReferenceDB = new CrossReferenceDB();
            } else {
                crossReferenceDB = patientCrossReferenceDAO.searchForPatientAliasesWithPatientDB(crossReferenceDBID);
            }
            crossReferenceDB.setLastModifier("PatientRegistry");
            crossReferenceDB.setLastChanged(new Date());
            patientDB.setPixReference(crossReferenceDB);
            patientDAO.updatePatientDB(patientDB);
        } catch (PatientNotFoundException | PatientUpdateException | CrossReferenceApplicationException | SearchCrossReferenceException e) {
            throw new PatientFeedException("Impossible to cross reference the patient (not saved)",e);
        }
    }
}
