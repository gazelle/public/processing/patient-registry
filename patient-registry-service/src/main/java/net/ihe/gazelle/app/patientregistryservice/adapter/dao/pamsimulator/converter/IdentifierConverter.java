package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.converter;

import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.DesignatorTypeDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.HierarchicDesignatorDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientIdentifierDB;

/**
 * Converter for Identifiers from business model to Database model and back.
 */
public class IdentifierConverter {

    /**
     * Private Constructor to hide the implicit one
     */
    private IdentifierConverter(){
    }

    /**
     * Map a business {@link EntityIdentifier} object to a {@link PatientIdentifierDB} entity to use during Database access, and assign it to
     * the corresponding Patient.
     * @param identifier : business object to map
     * @param patient : Patient entity containing the identifier to map.
     * @return the mapped {@link PatientIdentifierDB}
     */
    public static PatientIdentifierDB toPatientIdentifierDB(EntityIdentifier identifier, PatientDB patient) {
        PatientIdentifierDB patientIdentifierDB = toPatientIdentifierDB(identifier);

        if (patient != null){
            patientIdentifierDB.getPatients().add(patient);
            patient.getPatientIdentifiers().add(patientIdentifierDB);
        }

        return patientIdentifierDB;

    }

    /**
     * Map a business {@link EntityIdentifier} object to a {@link PatientIdentifierDB} entity to use during Database access.
     * @param identifier : business object to map
     * @return the mapped {@link PatientIdentifierDB}
     */
    public static PatientIdentifierDB toPatientIdentifierDB(EntityIdentifier identifier) {
        PatientIdentifierDB patientIdentifierDB = new PatientIdentifierDB();
        if (identifier != null) {
        patientIdentifierDB.setIdentifier(identifier.getValue());
        patientIdentifierDB.setIdentifierTypeCode(identifier.getType());

        if(identifier.getSystemIdentifier() != null) {
            HierarchicDesignatorDB designator = new HierarchicDesignatorDB();
            designator.setUniversalID(identifier.getSystemIdentifier());
            designator.setNamespaceID(identifier.getSystemName());
            designator.setUsage(DesignatorTypeDB.PATIENT_ID);
            patientIdentifierDB.setDomain(designator);
                patientIdentifierDB.setFullPatientIdentifierIfEmpty();
            }
        }
        return patientIdentifierDB;
    }

    /**
     * Map a {@link PatientIdentifierDB} entity to a business {@link EntityIdentifier} object.
     * @param patientIdentifierDB : entity to map
     * @return mapped business object.
     */
    public static EntityIdentifier toPatientIdentifier(PatientIdentifierDB patientIdentifierDB) {
            EntityIdentifier returnedPatientIdentifier = new EntityIdentifier();
            returnedPatientIdentifier.setValue(patientIdentifierDB.getIdentifier());
            returnedPatientIdentifier.setType(patientIdentifierDB.getIdentifierTypeCode());
            if(patientIdentifierDB.getDomain() != null) {
                returnedPatientIdentifier.setSystemIdentifier(patientIdentifierDB.getDomain().getUniversalID());
                returnedPatientIdentifier.setSystemName(patientIdentifierDB.getDomain().getNamespaceID());
            }
            return returnedPatientIdentifier;
    }
}
