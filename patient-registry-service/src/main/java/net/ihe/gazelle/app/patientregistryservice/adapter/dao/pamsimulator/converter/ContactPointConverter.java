package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.converter;

import net.ihe.gazelle.app.patientregistryapi.business.ContactPoint;
import net.ihe.gazelle.app.patientregistryapi.business.ContactPointUse;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientPhoneNumberDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PhoneNumberTypeDB;

/**
 * Converter for ContactPoints from business model to Database model and back.
 */
public class ContactPointConverter {

    /**
     * Private constructor to hide implicit one.
     */
    private ContactPointConverter(){
    }

    /**
     * Map a business {@link ContactPoint} object to a {@link PatientPhoneNumberDB} entity to use during Database access, and assign it to
     * the corresponding  in both direction.
     * @param contactPoint : business object to map
     * @param patient : Patient entity containing the Contact Point to map.
     * @return the mapped {@link PatientPhoneNumberDB}
     */
    public static PatientPhoneNumberDB toContactPointDB(ContactPoint contactPoint, PatientDB patient) {
        PatientPhoneNumberDB contact = toContactPointDB(contactPoint);
        if (patient != null){
            contact.setPatient(patient);
            patient.addPhoneNumber(contact);
        }
        return contact;
    }

    /**
     * Map a business {@link ContactPoint} object to a {@link PatientPhoneNumberDB} entity to use during Database access
     * @param contactPoint : business object to map
     * @return the mapped {@link PatientPhoneNumberDB}
     */
    public static PatientPhoneNumberDB toContactPointDB(ContactPoint contactPoint) {
        PatientPhoneNumberDB contact = new PatientPhoneNumberDB();
        contact.setType(convertType(contactPoint.getUse()));
        contact.setValue(contactPoint.getValue());
        return contact;
    }

    /**
     * Map ContactPoint Type from the business to the Database Model.
     * @param contactPointUse : business value of the Contact Point Use.
     * @return Database value for the ContactPoint Type.
     */
    private static PhoneNumberTypeDB convertType(ContactPointUse contactPointUse) {
        if (contactPointUse != null) {
            switch (contactPointUse) {
                case BEEPER:
                    return PhoneNumberTypeDB.BEEPER;
                case MOBILE:
                    return PhoneNumberTypeDB.MOBILE;
                case HOME:
                    return PhoneNumberTypeDB.HOME;
                case OTHER:
                    return PhoneNumberTypeDB.OTHER;
                case EMERGENCY:
                    return PhoneNumberTypeDB.EMERGENCY;
                case TEMPORARY:
                    return PhoneNumberTypeDB.VACATION;
                case WORK:
                    return PhoneNumberTypeDB.WORK;
                default:
                    return PhoneNumberTypeDB.OTHER;
            }
        } else {
            return null;
        }
    }

    /**
     * Map a {@link PatientPhoneNumberDB} entity to a business {@link ContactPoint} object.
     * @param phoneNumber : entity to map
     * @return mapped business object.
     */
    public static ContactPoint toContactPoint(PatientPhoneNumberDB phoneNumber) {
        return new ContactPoint(null,
                toContactPointUse(phoneNumber.getType()),phoneNumber.getValue());
    }

    /**
     * Map ContactPoint Use from the Database Model to the business.
     * @param type : database value of the ContactPoint Use.
     * @return business value for the ContactPoint Use.
     */
    private static ContactPointUse toContactPointUse(PhoneNumberTypeDB type) {
        if (type != null) {
            switch (type) {
                case MOBILE:
                    return ContactPointUse.MOBILE;
                case HOME:
                    return ContactPointUse.HOME;
                case BEEPER:
                    return ContactPointUse.BEEPER;
                case OTHER:
                    return ContactPointUse.OTHER;
                case EMERGENCY:
                    return ContactPointUse.EMERGENCY;
                case VACATION:
                    return ContactPointUse.TEMPORARY;
                case WORK:
                    return ContactPointUse.WORK;
                default:
                    return ContactPointUse.HOME;
            }
        } else {
            return null;
        }
    }
}
