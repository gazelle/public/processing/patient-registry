package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search;

import net.ihe.gazelle.app.patientregistryapi.application.ModifierProvider;

import java.util.HashMap;
import java.util.Map;

public class ModifierProviderImpl implements ModifierProvider {


    private static final Map<String, String> MODIFIER_TO_OPERATOR;

    static {
        MODIFIER_TO_OPERATOR = new HashMap<>();
        MODIFIER_TO_OPERATOR.put(CONTAINS, "LIKE");
        MODIFIER_TO_OPERATOR.put(NOT_EQUALS, "!=");
        MODIFIER_TO_OPERATOR.put(LT, "<");
        MODIFIER_TO_OPERATOR.put(EB, "<");
        MODIFIER_TO_OPERATOR.put(LE, "<=");
        MODIFIER_TO_OPERATOR.put(GT, ">");
        MODIFIER_TO_OPERATOR.put(SA, ">");
        MODIFIER_TO_OPERATOR.put(GE, ">=");
        MODIFIER_TO_OPERATOR.put(AP, "BETWEEN");
    }

    public String getQueryOperator(String modifier) {

        return MODIFIER_TO_OPERATOR.getOrDefault(modifier, DEFAULT_VALUE);

    }

}
