package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.converter;

import net.ihe.gazelle.app.patientregistryapi.business.PersonName;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;

/**
 * Converter for Names from business model to Database model and back.
 */
public class NameConverter {

    /**
     * Private constructor to hide implicit one.
     */
    private NameConverter(){
    }

    /**
     * Map a business {@link PersonName} object to a corresponding to {@link PatientDB} entity field to use during Database access.
     * @param name : business object to map
     * @param patient : Patient entity containing the Name information to map.
     */
    public static void mapNameToPatientDB(PersonName name, PatientDB patient) {
        patient.setLastName(name.getFamily());
        if(!name.getGivens().isEmpty()) {
            patient.setFirstName(name.getGivens().get(0));
            if(name.getGivens().size() > 1) {
                patient.setSecondName(name.getGivens().get(1));
                if(name.getGivens().size() > 2) {
                    patient.setThirdName(name.getGivens().get(2));
                }
            }
        }
    }

    /**
     * Map corresponding fields in {@link PatientDB} entity to a business {@link PersonName} object.
     * @param patient : database object to map
     * @return mapped {@link PersonName}
     */
    public static PersonName mapPatientDbToName(PatientDB patient) {
        PersonName name = new PersonName();
        name.setFamily(patient.getLastName());
        if(patient.getFirstName() != null) {
            name.addGiven(patient.getFirstName());
        }
        if(patient.getSecondName() != null) {
            name.addGiven(patient.getSecondName());
        }
        if(patient.getThirdName() != null) {
            name.addGiven(patient.getThirdName());
        }
        return name;
    }
}
