package net.ihe.gazelle.app.patientregistryservice.application.dao;

import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientIdentifierDB;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.PatientIdentifierException;

/**
 * API for the Identifier DAO
 */
public interface IdentifierDAO {

    /**
     * Retrieve a {@link net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientIdentifierDB} from database.
     *
     * @param fullPatientIdentifier Full Patient identifier
     * @param identifierTypeCode    Type Code for the identifier
     * @return the domain from database that corresponds to the requested identifier
     */
    PatientIdentifierDB retrieve(String fullPatientIdentifier, String identifierTypeCode) throws PatientIdentifierException;

    /**
     * search for a PatientIdentifierDB ID given its HierarchicDesignatorDB  and its id_number
     *
     * @param hierarchicDesignatorDBID id of the system domain
     * @param idNumber                 of the patient in the system
     * @return PatientIdentifierDB ID
     * @throws PatientIdentifierException if an error occurred during the search
     */
    PatientIdentifierDB searchForPatientIdentifier(Integer hierarchicDesignatorDBID, String idNumber) throws PatientIdentifierException;

    /**
     * Creates the PatientIdentifierDB in the Patient Registry so it can be used on new Patient.
     *
     * @param patientIdentifierDB Identifier  to create
     * @return savedPatientIdentifier
     * @throws PatientIdentifierException if a PatientIdentifier already exist
     */
    PatientIdentifierDB createPatientIdentifier(PatientIdentifierDB patientIdentifierDB) throws PatientIdentifierException;

    /**
     * Read a Patient identifier using its patientIdentifier ID
     *
     * @param patientIdentifierId to retrieve PatientIdentifier
     * @return matching PatientIdentifierDB
     * @throws PatientIdentifierException if an error occurred during the read
     */
    PatientIdentifierDB readPatientIdentifier(Integer patientIdentifierId) throws PatientIdentifierException;


    /**
     * Delete a Patient Identifier in DB
     *
     * @param identifierValue Identifier of the patient
     * @param systemIdentifier System of the patient identifier if exist
     * @throws PatientIdentifierException if an error occurred during the deletion
     */
    void deletePatientIdentifier(String identifierValue, String systemIdentifier) throws PatientIdentifierException;

    /**
     * Delete a Patient Identifier in DB using the patientIdentifier ID
     * @param identifierID the patientIdentifier ID
     * @throws PatientIdentifierException if an error occurred during the deletion
     */
    void deletePatientIdentifier(Integer identifierID) throws PatientIdentifierException;
}
