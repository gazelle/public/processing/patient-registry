package net.ihe.gazelle.app.patientregistryservice.adapter.ws;

import com.gitb.core.AnyContent;
import com.gitb.ps.ProcessRequest;
import com.gitb.ps.ProcessResponse;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import net.ihe.gazelle.app.patientregistryapi.adapter.ws.PatientProcessingWebserviceConstants;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedException;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedService;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperAnyContentToObject;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperObjectToAnyContent;
import net.ihe.gazelle.lib.gitbutils.adapter.MappingException;

import javax.inject.Inject;
import java.util.List;

/**
 * Processing service for Patient Feed requests.
 */
public class PatientFeedProcessingService {

    public static final String PROCESS_REQUEST_INPUT_SHALL_CONTAIN_ONE_AND_ONLY_ONE_PATIENT = "ProcessRequest.input shall contain one and only one " +
            "patient !";
    public static final String PROCESS_REQUEST_INPUT_SHALL_HAVE_THE_NAME_S_FOR_A_S_OPERATION = "ProcessRequest.input shall have the name '%s' for a" +
            " %s operation !";
    public static final String PROCESS_REQUEST_INPUT_SHALL_ONLY_CONTAIN_A_PATIENT_AND_AN_IDENTIFIER = "ProcessRequest.input shall only contain a patient and an identifier!";
    private MapperAnyContentToObject mapper;
    private PatientFeedService patientFeedService;

    /**
     * Constructor for the class with all needed parameters
     *
     * @param patientFeedService : Application service to feed a patient
     */
    @Inject
    public PatientFeedProcessingService(PatientFeedService patientFeedService) {
        this.mapper = new MapperAnyContentToObject();
        this.patientFeedService = patientFeedService;
    }

    /**
     * Process the Patient Feed request.
     *
     * @param processRequest : GITB request received.
     * @return ProcessResponse corresponding to the received request
     */
    public ProcessResponse processPatientCreate(ProcessRequest processRequest) {
        String uuidOfCreatedPatient;
        List<AnyContent> patientToFeed = processRequest.getInput();
        if (patientToFeed.size() != 1) {
            throw new IllegalArgumentException(PROCESS_REQUEST_INPUT_SHALL_CONTAIN_ONE_AND_ONLY_ONE_PATIENT);
        } else if (!PatientProcessingWebserviceConstants.PATIENT_INPUT_NAME.equals(patientToFeed.get(0).getName())) {
            throw new IllegalArgumentException(String.format(PROCESS_REQUEST_INPUT_SHALL_HAVE_THE_NAME_S_FOR_A_S_OPERATION,
                    PatientProcessingWebserviceConstants.PATIENT_INPUT_NAME, PatientProcessingWebserviceConstants.PATIENT_CREATE_OPERATION));
        }

        try {
            uuidOfCreatedPatient = patientFeedService.createPatient(mapper.getObject(patientToFeed.get(0), Patient.class));
            return createPatientCreatedOrMergeProcessResponse(uuidOfCreatedPatient);
        } catch (MappingException e) {
            throw new IllegalArgumentException("Cannot decode Request inputs as Patients !");
        } catch (PatientFeedException e) {
            return ProcessResponseWithReportCreator.createFailedProcessResponse(e, patientToFeed.get(0));
        }
    }

    /**
     * Process the Patient Update request.
     *
     * @param processRequest : GITB request received.
     * @return ProcessResponse corresponding to the received request
     */
    public ProcessResponse processPatientUpdate(ProcessRequest processRequest) {
        Patient updatedPatient;
        List<AnyContent> updateInput = processRequest.getInput();
        if (updateInput.size() != 2) {
            throw new IllegalArgumentException(PROCESS_REQUEST_INPUT_SHALL_ONLY_CONTAIN_A_PATIENT_AND_AN_IDENTIFIER);
        } else if (!PatientProcessingWebserviceConstants.PATIENT_INPUT_NAME.equals(updateInput.get(0).getName())) {
            throw new IllegalArgumentException(String.format(PROCESS_REQUEST_INPUT_SHALL_HAVE_THE_NAME_S_FOR_A_S_OPERATION,
                    PatientProcessingWebserviceConstants.PATIENT_INPUT_NAME, PatientProcessingWebserviceConstants.PATIENT_UPDATE_OPERATION));
        }
        try {
            updatedPatient = patientFeedService.updatePatient(
                    mapper.getObject(updateInput.get(0), Patient.class),
                    mapper.getObject(updateInput.get(1), EntityIdentifier.class)
            );
            return createProcessResponseWithUpdatedPatient(updatedPatient);
        } catch (MappingException e) {
            throw new IllegalArgumentException("Cannot decode Request inputs as Patients !");
        } catch (PatientFeedException e) {
            return ProcessResponseWithReportCreator.createFailedProcessResponse(e, updateInput.get(0));
        }
    }

    /**
     * Process the Patient Merge Request
     *
     * @param processRequest : GITB request received
     * @return ProcessResponse corresponding to the received request
     */
    public ProcessResponse processPatientMerge(ProcessRequest processRequest) {
        String originalPatientUUID;
        List<AnyContent> patientToFeed = processRequest.getInput();
        if (patientToFeed.size() != 2) {
            throw new IllegalArgumentException("ProcessRequest.input shall contain two and only two patient uuids !");
        } else if (!PatientProcessingWebserviceConstants.UUID_OF_ORIGINAL_PATIENT_INPUT_NAME.equals(patientToFeed.get(0).getName()) || !PatientProcessingWebserviceConstants.UUID_OF_DUPLICATED_PATIENT_INPUT_NAME.equals(patientToFeed.get(1).getName())) {
            throw new IllegalArgumentException(String.format("ProcessRequest.input shall have the name '%s' and '%s' for a %s operation !",
                    PatientProcessingWebserviceConstants.UUID_OF_ORIGINAL_PATIENT_INPUT_NAME, PatientProcessingWebserviceConstants.UUID_OF_DUPLICATED_PATIENT_INPUT_NAME, PatientProcessingWebserviceConstants.PATIENT_MERGE_OPERATION));
        }
        try {
            originalPatientUUID = patientFeedService.mergePatient(mapper.getObject(patientToFeed.get(0), String.class), mapper.getObject(patientToFeed.get(1), String.class));
            return createPatientCreatedOrMergeProcessResponse(originalPatientUUID);
        } catch (MappingException e) {
            throw new IllegalArgumentException("Cannot decode Request inputs as UUIDs !");
        } catch (PatientFeedException e) {
            return ProcessResponseWithReportCreator.createFailedProcessResponse(e, patientToFeed.get(0));
        }
    }

    /**
     * Process the Patient Delete Request by uuid
     *
     * @param processRequest : GITB request received
     * @return ProcessResponse corresponding to the received request
     */
    public ProcessResponse processPatientDelete(ProcessRequest processRequest) {
        boolean isDeleted;
        List<AnyContent> patientToFeed = processRequest.getInput();
        if (patientToFeed.size() != 1) {
            throw new IllegalArgumentException(PROCESS_REQUEST_INPUT_SHALL_CONTAIN_ONE_AND_ONLY_ONE_PATIENT);
        } else if (!PatientProcessingWebserviceConstants.UUID_PATIENT_TO_DELETE_INPUT_NAME.equals(patientToFeed.get(0).getName())) {
            throw new IllegalArgumentException(String.format(PROCESS_REQUEST_INPUT_SHALL_HAVE_THE_NAME_S_FOR_A_S_OPERATION,
                    PatientProcessingWebserviceConstants.UUID_PATIENT_TO_DELETE_INPUT_NAME, PatientProcessingWebserviceConstants.PATIENT_DELETE_OPERATION));
        }
        try {
            isDeleted = patientFeedService.deletePatient(mapper.getObject(patientToFeed.get(0), String.class));
            return createPatientDeleteProcessResponse(isDeleted);
        } catch (MappingException e) {
            throw new IllegalArgumentException("Cannot decode Request inputs as UUID !");
        } catch (PatientFeedException e) {
            return ProcessResponseWithReportCreator.createFailedProcessResponse(e, patientToFeed.get(0));
        }
    }

    /**
     * Process the Patient Delete Request by Identifier
     *
     * @param processRequest : GITB request received
     * @return ProcessResponse corresponding to the received request
     */
    public ProcessResponse processPatientDeleteByIdentifier(ProcessRequest processRequest) {
        EntityIdentifier deleted;
        List<AnyContent> patientToFeed = processRequest.getInput();
        if (patientToFeed.size() != 1) {
            throw new IllegalArgumentException(PROCESS_REQUEST_INPUT_SHALL_CONTAIN_ONE_AND_ONLY_ONE_PATIENT);
        } else if (!PatientProcessingWebserviceConstants.IDENTIFIER_PATIENT_TO_DELETE_INPUT_NAME.equals(patientToFeed.get(0).getName())) {
            throw new IllegalArgumentException(String.format(PROCESS_REQUEST_INPUT_SHALL_HAVE_THE_NAME_S_FOR_A_S_OPERATION,
                    PatientProcessingWebserviceConstants.IDENTIFIER_PATIENT_TO_DELETE_INPUT_NAME, PatientProcessingWebserviceConstants.PATIENT_DELETE_OPERATION));
        }
        try {
            deleted = patientFeedService.deletePatient(mapper.getObject(patientToFeed.get(0), EntityIdentifier.class));
            return createPatientDeleteProcessResponse(deleted);
        } catch (MappingException e) {
            throw new IllegalArgumentException("Cannot decode Request inputs as UUID !");
        } catch (PatientFeedException e) {
            return ProcessResponseWithReportCreator.createFailedProcessResponse(e, patientToFeed.get(0));
        }
    }

    /**
     * Create a {@link ProcessResponse} to a {@link ProcessRequest} sent to this service after a successful processing.
     * The response will contain a {@link TAR} report containing the result Status for the processing operation,
     * as well as a single Output containing the UUID assigned to the created OR merged patient.
     *
     * @param uuid : UUID assigned to the created OR merged Patient, null can be used if the operation fails. No output will appear in the response.
     * @return the process response to return to the WebService
     * @throws PatientFeedException when response output cannot be mapped to AnyContent
     */
    private ProcessResponse createPatientCreatedOrMergeProcessResponse(String uuid) throws PatientFeedException {
        ProcessResponse processResponse = new ProcessResponse();
        if (uuid == null) {
            throw new IllegalArgumentException("Successful operation shall return a valued UUID as an output !");
        }
        AnyContent uuidAnyContent;
        try {
            uuidAnyContent = new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.UUID_OUTPUT_NAME, uuid);
        } catch (MappingException e) {
            throw new PatientFeedException("Cannot map assigned UUID to AnyContent !");
        }
        processResponse.getOutput().add(uuidAnyContent);
        processResponse.setReport(ProcessResponseWithReportCreator.createReport(TestResultType.SUCCESS));
        return processResponse;
    }

    /**
     * Create a {@link ProcessResponse} to a {@link ProcessRequest} sent to this service after a successful processing.
     * The response will contain a {@link TAR} report containing the result Status for the processing operation,
     * as well as a single Output containing the UUID assigned to the updated patient.
     *
     * @param patientToReturn : Patient assigned to the original Patient, null can be used if the operation fails. No output will appear in the response.
     * @return the process response to return to the WebService
     * @throws PatientFeedException when response output cannot be mapped to AnyContent
     */
    private ProcessResponse createProcessResponseWithUpdatedPatient(Patient patientToReturn) throws PatientFeedException {

        ProcessResponse processResponse = new ProcessResponse();
        if (patientToReturn == null) {
            throw new IllegalArgumentException("Successful operation shall return an existing Patient as an output !");
        }
        AnyContent patientAnyContent;
        try {
            patientAnyContent = new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_OUTPUT_NAME, patientToReturn);
        } catch (MappingException e) {
            throw new PatientFeedException("Cannot map Patient to AnyContent !");
        }
        processResponse.getOutput().add(patientAnyContent);
        processResponse.setReport(ProcessResponseWithReportCreator.createReport(TestResultType.SUCCESS));
        return processResponse;
    }

    /**
     * Create a {@link ProcessResponse} to a {@link ProcessRequest} sent to this service after a successful processing.
     * The response will contain a {@link TAR} report containing the result Status for the processing operation,
     * as well as a single Output containing the UUID assigned to the deleted patient.
     *
     * @param isDelete : boolean value assigned to the deleted Patient. No output will appear in the response.
     * @return the process response to return to the WebService
     * @throws PatientFeedException when response output cannot be mapped to AnyContent
     */
    private ProcessResponse createPatientDeleteProcessResponse(boolean isDelete) throws PatientFeedException {
        ProcessResponse processResponse = new ProcessResponse();
        AnyContent deletionStatusAnyContent;
        try {
            if (isDelete) {
                deletionStatusAnyContent = new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.DELETE_STATUS_OUTPUT_NAME, PatientProcessingWebserviceConstants.DELETE_STATUS_DONE);
            } else {
                deletionStatusAnyContent = new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.DELETE_STATUS_OUTPUT_NAME, PatientProcessingWebserviceConstants.DELETE_STATUS_GONE);
            }

        } catch (MappingException e) {
            throw new PatientFeedException("Cannot map to AnyContent !");
        }
        processResponse.getOutput().add(deletionStatusAnyContent);
        processResponse.setReport(ProcessResponseWithReportCreator.createReport(TestResultType.SUCCESS));
        return processResponse;
    }

    /**
     * @see PatientFeedProcessingService#createPatientDeleteProcessResponse(boolean isDelete)
     * @param identifier : delete identifier
     * @return the process response to return to the WebService
     * @throws PatientFeedException when response output cannot be mapped to AnyContent
     */
    private ProcessResponse createPatientDeleteProcessResponse(EntityIdentifier identifier) throws PatientFeedException {
        ProcessResponse processResponse = new ProcessResponse();
        AnyContent deletionStatusAnyContent;
        try {
            deletionStatusAnyContent = new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.DELETED_IDENTIFIER_OUTPUT_NAME, identifier);
        } catch (MappingException e) {
            throw new PatientFeedException("Cannot map to AnyContent !");
        }
        processResponse.getOutput().add(deletionStatusAnyContent);
        processResponse.setReport(ProcessResponseWithReportCreator.createReport(TestResultType.SUCCESS));
        return processResponse;
    }
}
