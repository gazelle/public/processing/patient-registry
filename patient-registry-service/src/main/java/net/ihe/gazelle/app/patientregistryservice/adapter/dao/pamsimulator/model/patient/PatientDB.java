package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient;

import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.references.CrossReferenceDB;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>Patient class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Entity
@NamedQuery(name = "PatientDB.findByUuid", query = "SELECT p FROM PatientDB p WHERE p.uuid = :uuid")
@NamedQuery(name = "PatientDB.findForSimulatedActorMatchingPatient", query = "SELECT p FROM PatientDB p WHERE p.simulatedActor = :actorKeyword and " +
        "p.dateOfBirth =:patientBirth and p.genderCode =: patientGenderCode")
@NamedQuery(name = "PatientDB.findUUID", query = "SELECT p.uuid FROM PatientDB p WHERE p.uuid = :uuid")
@NamedQuery(name = "PatientDB.findByPatientIdentifier", query = "SELECT pdb FROM PatientDB pdb, IN (pdb.patientIdentifiers) pid WHERE pid" +
        ".identifier = :patient_identifier")
@NamedQuery(name ="PatientDB.findByPatientIdentifierAndSystem", query = "SELECT pdb FROM PatientDB pdb, IN (pdb.patientIdentifiers) pid WHERE pid" +
        ".identifier = :patient_identifier and pid.domain.universalID = :system_identifier")
@Table(name = "pat_patient", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "pat_patient_sequence", sequenceName = "pat_patient_id_seq", allocationSize = 1)
public class PatientDB extends AbstractPatientDB {

    private static int generalFakeId = 0;

    @Transient
    private int fakeId;

    @Column(name = "dtype")
    private String dtype = "pam_patient";

    @Column(name = "uuid")
    private String uuid;

    @Column(name = "email")
    private String email;

    @OneToMany(mappedBy = "patient", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<PatientPhoneNumberDB> phoneNumbers;

    @Column(name = "creator")
    private String creator;

    @Column(name = "actor_id")
    private int actor_id;


    @Column(name = "simulated_actor_keyword")
    private String simulatedActor;

    @Column(name = "still_active")
    private Boolean stillActive;

    @Column(name = "account_number")
    private String accountNumber;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "pam_patient_patient_identifier", joinColumns = @JoinColumn(name = "patient_id"), inverseJoinColumns = @JoinColumn(name =
            "patient_identifier_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "patient_id", "patient_identifier_id"}))
    private List<PatientIdentifierDB> patientIdentifiers = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "cross_reference_id")
    private CrossReferenceDB pixReference;

    @Column(name = "blood_group")
    private String bloodGroup;

    @Column(name = "vip_indicator")
    private String vipIndicator;

    /**
     * those extra columns are used to store the double metaphone (needed for automatically cross-referencing patients)
     */
    @Column(name = "dmetaphone_first_name")
    private String dmetaphoneFirstName;

    @Column(name = "dmetaphone_last_name")
    private String dmetaphoneLastName;

    @Column(name = "dmetaphone_mother_maiden_name")
    private String dmetaphoneMotherMaidenName;

    @Column(name = "multiple_birth_indicator")
    private Boolean multipleBirthIndicator;

    @Transient
    private String multipleBirthIndicatorAsString;

    @Transient
    private String birthOrderAsString;

    @Column(name = "birth_order")
    private Integer birthOrder;

    @Column(name = "last_update_facility")
    private String lastUpdateFacility;

    @Column(name = "marital_status")
    private String maritalStatus;

    @Column(name = "identity_reliability_code")
    private String identityReliabilityCode;

    @Column(name = "is_patient_dead")
    private boolean patientDead;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "patient_death_time")
    private Date patientDeathTime;

    @Column(name = "is_test_data")
    private boolean testData;

    @OneToMany(mappedBy = "patient", targetEntity = PersonDB.class)
    private List<PersonDB> personalRelationships;

    @Column(name = "birth_place_name")
    private String birthPlaceName;

    /**
     * Constructors
     */
    public PatientDB() {
        super();
        this.pixReference = null;
        setFakeId();
        patientDead = false;
        patientDeathTime = null;
        this.testData = false;
        this.stillActive = true;
    }

    private void setFakeId() {
        synchronized (PatientDB.class) {
            fakeId = generalFakeId++;
        }
    }

    /**
     * <p>isPatientDead.</p>
     *
     * @return a boolean.
     */
    public boolean isPatientDead() {
        return patientDead;
    }

    /**
     * <p>Setter for the field <code>patientDead</code>.</p>
     *
     * @param patientDead a boolean.
     */
    public void setPatientDead(boolean patientDead) {
        this.patientDead = patientDead;
    }

    /**
     * <p>Getter for the field <code>patientDeathTime</code>.</p>
     *
     * @return a {@link Date} object.
     */
    public Date getPatientDeathTime() {
        if (patientDeathTime != null) {
            return (Date) patientDeathTime.clone();
        }
        return null;
    }

    /**
     * <p>Setter for the field <code>patientDeathTime</code>.</p>
     *
     * @param patientDeathTime a {@link Date} object.
     */
    public void setPatientDeathTime(Date patientDeathTime) {
        if (patientDeathTime != null) {
            this.patientDeathTime = (Date) patientDeathTime.clone();
        } else {
            this.patientDeathTime = null;
        }
    }

    /**
     * <p>Getter for the field <code>maritalStatus</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * <p>Setter for the field <code>maritalStatus</code>.</p>
     *
     * @param maritalStatus a {@link String} object.
     */
    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    /**
     * <p>Getter for the field <code>identityReliabilityCode</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getIdentityReliabilityCode() {
        return identityReliabilityCode;
    }

    /**
     * <p>Setter for the field <code>identityReliabilityCode</code>.</p>
     *
     * @param identityReliabilityCode a {@link String} object.
     */
    public void setIdentityReliabilityCode(String identityReliabilityCode) {
        this.identityReliabilityCode = identityReliabilityCode;
    }

    /**
     * <p>Getter for the field <code>creator</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getCreator() {
        return creator;
    }

    /**
     * <p>Setter for the field <code>creator</code>.</p>
     *
     * @param creator a {@link String} object.
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * <p>Setter for the field <code>simulatedActor</code>.</p>
     *
     * @param simulatedActor .
     */
    public void setSimulatedActor(String simulatedActor) {
        this.simulatedActor = simulatedActor;
    }

    /**
     * <p>Getter for the field <code>simulatedActor</code>.</p>
     *
     * @return the simulatedActor literal value
     */
    public String getSimulatedActor() {
        return simulatedActor;
    }

    /**
     * <p>Getter for the field <code>email</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getEmail() {
        return email;
    }

    /**
     * <p>Getter for the field <code>vipIndicator</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getVipIndicator() {
        return vipIndicator;
    }

    /**
     * <p>Setter for the field <code>vipIndicator</code>.</p>
     *
     * @param vipIndicator a {@link String} object.
     */
    public void setVipIndicator(String vipIndicator) {
        this.vipIndicator = vipIndicator;
    }

    /**
     * <p>Setter for the field <code>email</code>.</p>
     *
     * @param email a {@link String} object.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * <p>Setter for the field <code>stillActive</code>.</p>
     *
     * @param stillActive a {@link Boolean} object.
     */
    public void setStillActive(Boolean stillActive) {
        this.stillActive = stillActive;
    }

    /**
     * <p>Getter for the field <code>stillActive</code>.</p>
     *
     * @return a {@link Boolean} object.
     */
    public Boolean getStillActive() {
        return stillActive;
    }

    /**
     * <p>Setter for the field <code>patientIdentifiers</code>.</p>
     *
     * @param patientIdentifiers a {@link List} object.
     */
    public void setPatientIdentifiers(List<PatientIdentifierDB> patientIdentifiers) {
        this.patientIdentifiers = patientIdentifiers;
    }

    /**
     * <p>Getter for the field <code>patientIdentifiers</code>.</p>
     *
     * @return a {@link List} object.
     */
    public List<PatientIdentifierDB> getPatientIdentifiers() {
        return this.patientIdentifiers;
    }

    /**
     * <p>remove element from the field <code>patientIdentifiers</code>.</p>
     *
     * @param patientIdentifierDB a {@link PatientIdentifierDB} object.
     */
    public void removePatientIdentifiers(PatientIdentifierDB patientIdentifierDB) {
        patientIdentifierDB.removePatient(this);
        this.patientIdentifiers.remove(patientIdentifierDB);

    }

    /**
     * <p>Add element for the field <code>patientIdentifiers</code>.</p>
     *
     * @param patientIdentifierDB a {@link PatientIdentifierDB} object.
     */
    public void addPatientIdentifiers(PatientIdentifierDB patientIdentifierDB) {
        patientIdentifierDB.addPatient(this);
        this.patientIdentifiers.add(patientIdentifierDB);
    }

    /**
     * <p>Getter for the field <code>phoneNumbers</code>.</p>
     *
     * @return a {@link List} object.
     */
    public List<PatientPhoneNumberDB> getPhoneNumbers() {
        if (phoneNumbers == null) {
            phoneNumbers = new ArrayList<>();
        }
        return phoneNumbers;
    }

    /**
     * <p>Setter for the field <code>phoneNumbers</code>.</p>
     *
     * @param phoneNumbers a {@link List} object.
     */
    public void setPhoneNumbers(List<PatientPhoneNumberDB> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    /**
     * <p>Setter for the field <code>accountNumber</code>.</p>
     *
     * @param accountNumber a {@link String} object.
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * <p>Getter for the field <code>accountNumber</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * <p>Getter for the field <code>pixReference</code>.</p>
     *
     * @return the pixReference value
     */
    public CrossReferenceDB getPixReference() {
        return pixReference;
    }

    /**
     * <p>Setter for the field <code>pixReference</code>.</p>
     *
     * @param pixReference value to set to the property
     */
    public void setPixReference(CrossReferenceDB pixReference) {
        this.pixReference = pixReference;
        pixReference.getPatients().add(this);
    }

    public void removePixReference(){
        pixReference.getPatients().remove(this);
        this.pixReference=null;
    }

    /**
     * <p>Getter for the field <code>dmetaphoneFirstName</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getDmetaphoneFirstName() {
        return dmetaphoneFirstName;
    }

    /**
     * <p>Getter for the field <code>dmetaphoneLastName</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getDmetaphoneLastName() {
        return dmetaphoneLastName;
    }

    /**
     * <p>Getter for the field <code>dmetaphoneMotherMaidenName</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getDmetaphoneMotherMaidenName() {
        return dmetaphoneMotherMaidenName;
    }

    /**
     * <p>Getter for the field <code>bloodGroup</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getBloodGroup() {
        return bloodGroup;
    }

    /**
     * <p>Setter for the field <code>bloodGroup</code>.</p>
     *
     * @param bloodGroup a {@link String} object.
     */
    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    /**
     * <p>Getter for the field <code>birthOrder</code>.</p>
     *
     * @return a {@link Integer} object.
     */
    public Integer getBirthOrder() {
        return birthOrder;
    }

    /**
     * <p>Setter for the field <code>birthOrder</code>.</p>
     *
     * @param birthOrder a {@link Integer} object.
     */
    public void setBirthOrder(Integer birthOrder) {
        this.birthOrder = birthOrder;
    }

    /**
     * <p>Getter for the field <code>lastUpdateFacility</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getLastUpdateFacility() {
        return lastUpdateFacility;
    }

    /**
     * <p>Setter for the field <code>lastUpdateFacility</code>.</p>
     *
     * @param lastUpdateFacility a {@link String} object.
     */
    public void setLastUpdateFacility(String lastUpdateFacility) {
        this.lastUpdateFacility = lastUpdateFacility;
    }

    /**
     * <p>Getter for the field <code>multipleBirthIndicator</code>.</p>
     *
     * @return a {@link Boolean} object.
     */
    public Boolean getMultipleBirthIndicator() {
        return multipleBirthIndicator;
    }

    /**
     * <p>Setter for the field <code>multipleBirthIndicator</code>.</p>
     *
     * @param multipleBirthIndicator a {@link Boolean} object.
     */
    public void setMultipleBirthIndicator(Boolean multipleBirthIndicator) {
        this.multipleBirthIndicator = multipleBirthIndicator;
    }

    /**
     * <p>Getter for the field <code>birthOrderAsString</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getBirthOrderAsString() {
        return birthOrderAsString;
    }

    /**
     * <p>Setter for the field <code>birthOrderAsString</code>.</p>
     *
     * @param birthOrderAsString a {@link String} object.
     */
    public void setBirthOrderAsString(String birthOrderAsString) {
        this.birthOrderAsString = birthOrderAsString;
    }

    /**
     * <p>Getter for the field <code>multipleBirthIndicatorAsString</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getMultipleBirthIndicatorAsString() {
        return multipleBirthIndicatorAsString;
    }

    /**
     * <p>Setter for the field <code>multipleBirthIndicatorAsString</code>.</p>
     *
     * @param multipleBirthIndicatorAsString a {@link String} object.
     */
    public void setMultipleBirthIndicatorAsString(String multipleBirthIndicatorAsString) {
        this.multipleBirthIndicatorAsString = multipleBirthIndicatorAsString;
    }

    /**
     * <p>addPhoneNumber.</p>
     *
     * @param phoneNumber a {@link PatientPhoneNumberDB} object.
     */
    public void addPhoneNumber(PatientPhoneNumberDB phoneNumber) {
        if (this.phoneNumbers == null) {
            this.phoneNumbers = new ArrayList<>();
        }
        this.phoneNumbers.add(phoneNumber);
    }

    /**
     * <p>Getter for the field <code>personalRelationships</code>.</p>
     *
     * @return a {@link List} object.
     */
    public List<PersonDB> getPersonalRelationships() {
        if (this.personalRelationships == null) {
            this.personalRelationships = new ArrayList<>();
        }
        return personalRelationships;
    }

    /**
     * <p>Setter for the field <code>personalRelationships</code>.</p>
     *
     * @param personalRelationships a {@link List} object.
     */
    public void setPersonalRelationships(List<PersonDB> personalRelationships) {
        this.personalRelationships = personalRelationships;
    }

    /**
     * <p>addPersonalRelationship.</p>
     *
     * @param inPerson a {@link PersonDB} object.
     */
    public void addPersonalRelationship(PersonDB inPerson) {
        this.getPersonalRelationships().add(inPerson);
    }

    /**
     * <p>removePersonalRelationship.</p>
     *
     * @param inPerson a {@link PersonDB} object.
     */
    public void removePersonalRelationship(PersonDB inPerson) {
        this.getPersonalRelationships().remove(inPerson);
    }

    /**
     * Getter for the birthPlaceName property
     *
     * @return the value of the property
     */
    public String getBirthPlaceName() {
        return birthPlaceName;
    }

    /**
     * Setter for the birthPlaneName property
     *
     * @param birthPlaceName value to set to the property.
     */
    public void setBirthPlaceName(String birthPlaceName) {
        this.birthPlaceName = birthPlaceName;
    }

    /**
     * Getter for the uuid property
     *
     * @return the value of the uuid property.
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Setter for the uuid property.
     *
     * @param uuid value to set to the uuid property.
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Getter for the actor_id property
     *
     * @return the value of the actor_id property.
     */
    public int getActor_id() {
        return actor_id;
    }

    /**
     * Setter for the actor_id property.
     *
     * @param actor_id value to set to the actor_id property.
     */
    public void setActor_id(int actor_id) {
        this.actor_id = actor_id;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object p) {
        if (p == null) {
            return false;
        }
        if (!(p instanceof PatientDB)) {
            return false;
        }
        PatientDB patient = (PatientDB) p;
        if (getId() == null || patient.getId() == null) {
            return fakeId == patient.fakeId;
        } else {
            return super.equals(patient);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        if (getId() == null) {
            final int prime = 31;
            int result = 1;
            result = (prime * result) + fakeId;
            return result;
        } else {
            return super.hashCode();
        }
    }

}
