package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient;


import net.ihe.gazelle.app.patientregistryapi.business.GenderCode;

public enum GenderCodeDB {

   M(GenderCode.MALE),
   F(GenderCode.FEMALE),
   U(GenderCode.UNDEFINED),
   O(GenderCode.OTHER);

   private final GenderCode genderCode;

   GenderCodeDB(GenderCode genderCode) {
      this.genderCode = genderCode;
   }

   public static GenderCodeDB fromGenderCode(GenderCode genderCode) {
      if (genderCode != null) {
         switch (genderCode) {
            case MALE:
               return GenderCodeDB.M;
            case FEMALE:
               return GenderCodeDB.F;
            case UNDEFINED:
               return GenderCodeDB.U;
            case OTHER:
               return GenderCodeDB.O;
            default:
               throw new IllegalArgumentException("Unknown GenderCode");
         }
      }
      return null;
   }

   public GenderCode toGenderCode() {
      return genderCode;
   }

}
