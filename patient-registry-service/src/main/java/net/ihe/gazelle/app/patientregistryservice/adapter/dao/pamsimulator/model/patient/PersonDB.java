package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aberge on 03/07/17.
 *
 * @author aberge
 * @version $Id: $Id
 */

@Entity
@Table(name = "pam_person", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "pam_person_sequence", sequenceName = "pam_person_id_seq", allocationSize = 1)
public class PersonDB {

    @Id
    @GeneratedValue(generator = "pam_person_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "second_name")
    private String secondName;

    @Column(name = "last_name")
    private String lastName;

    /**
     * populated using SVS Simulator (OID: 2.16.840.1.113883.12.63, keyword: RELATIONSHIP)
     */
    @Column(name = "relationship_code")
    private String relationshipCode;

    /**
     * populate using SVS Simulator (OID :2.16.840.1.113883.12.131, keyword: CONTACT_ROLE)
     */
    @Column(name = "contact_role_code")
    private String contactRoleCode;

    @Column(name = "identifier")
    private String identifier;


    @ManyToOne(targetEntity = PatientDB.class)
    @JoinColumn(name = "patient_id")
    private PatientDB patient;

    @OneToMany(mappedBy = "person", targetEntity = PersonPhoneNumberDB.class)
    private List<PersonPhoneNumberDB> phoneNumberList;

    @OneToMany(mappedBy = "person", targetEntity = PersonAddressDB.class)
    private List<PersonAddressDB> addressList;


    /**
     * <p>Constructor for PersonDB.</p>
     */
    public PersonDB() {

    }

    /**
     * <p>Constructor for PersonDB.</p>
     *
     * @param inPatient a {@link PatientDB} object.
     */
    public PersonDB(PatientDB inPatient) {
        this.patient = inPatient;
    }

    /**
     * <p>Constructor for PersonDB.</p>
     *
     * @param relationshipRole Relationship Role for the new PersonDB.
     */
    public PersonDB(PersonalRelationshipRoleTypeDB relationshipRole) {
        this.relationshipCode = relationshipRole.name();
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>firstName</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * <p>Setter for the field <code>firstName</code>.</p>
     *
     * @param firstName a {@link String} object.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * <p>Getter for the field <code>secondName</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getSecondName() {
        return secondName;
    }

    /**
     * <p>Setter for the field <code>secondName</code>.</p>
     *
     * @param secondName a {@link String} object.
     */
    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    /**
     * <p>Getter for the field <code>lastName</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * <p>Setter for the field <code>lastName</code>.</p>
     *
     * @param lastName a {@link String} object.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * <p>Getter for the field <code>relationshipCode</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getRelationshipCode() {
        return relationshipCode;
    }

    /**
     * <p>Setter for the field <code>relationshipCode</code>.</p>
     *
     * @param relationshipCode a {@link String} object.
     */
    public void setRelationshipCode(String relationshipCode) {
        this.relationshipCode = relationshipCode;
    }

    /**
     * <p>Getter for the field <code>contactRoleCode</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getContactRoleCode() {
        return contactRoleCode;
    }

    /**
     * <p>Setter for the field <code>contactRoleCode</code>.</p>
     *
     * @param contactRoleCode a {@link String} object.
     */
    public void setContactRoleCode(String contactRoleCode) {
        this.contactRoleCode = contactRoleCode;
    }

    /**
     * <p>Getter for the field <code>identifier</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * <p>Setter for the field <code>identifier</code>.</p>
     *
     * @param identifier a {@link String} object.
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * <p>Getter for the field <code>patient</code>.</p>
     *
     * @return a {@link PatientDB} object.
     */
    public PatientDB getPatient() {
        return patient;
    }

    /**
     * <p>Setter for the field <code>patient</code>.</p>
     *
     * @param patient a {@link PatientDB} object.
     */
    public void setPatient(PatientDB patient) {
        this.patient = patient;
    }

    /**
     * <p>Getter for the field <code>phoneNumberList</code>.</p>
     *
     * @return a {@link List} object.
     */
    public List<PersonPhoneNumberDB> getPhoneNumberList() {
        if (this.phoneNumberList == null) {
            this.phoneNumberList = new ArrayList<>();
        }
        return phoneNumberList;
    }

    /**
     * <p>Setter for the field <code>phoneNumberList</code>.</p>
     *
     * @param phoneNumberList a {@link List} object.
     */
    public void setPhoneNumberList(List<PersonPhoneNumberDB> phoneNumberList) {
        this.phoneNumberList = phoneNumberList;
    }

    /**
     * <p>Getter for the field <code>addressList</code>.</p>
     *
     * @return a {@link List} object.
     */
    public List<PersonAddressDB> getAddressList() {
        if (this.addressList == null) {
            this.addressList = new ArrayList<>();
        }
        return addressList;
    }

    /**
     * <p>Setter for the field <code>addressList</code>.</p>
     *
     * @param addressList a {@link List} object.
     */
    public void setAddressList(List<PersonAddressDB> addressList) {
        this.addressList = addressList;
    }

    /**
     * <p>addPhoneNumber.</p>
     *
     * @param inNumber a {@link PersonPhoneNumberDB} object.
     */
    public void addPhoneNumber(PersonPhoneNumberDB inNumber) {
        this.getPhoneNumberList().add(inNumber);
    }

    /**
     * <p>removePhoneNumber.</p>
     *
     * @param inNumber a {@link PersonPhoneNumberDB} object.
     */
    public void removePhoneNumber(PersonPhoneNumberDB inNumber) {
        this.getPhoneNumberList().remove(inNumber);
    }

    /**
     * <p>addPersonAddress.</p>
     *
     * @param inAddress a {@link PersonAddressDB} object.
     */
    public void addPersonAddress(PersonAddressDB inAddress) {
        this.getAddressList().add(inAddress);
    }

    /**
     * <p>removePersonAddress.</p>
     *
     * @param inAddress a {@link PersonAddressDB} object.
     */
    public void removePersonAddress(PersonAddressDB inAddress) {
        this.getAddressList().remove(inAddress);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PersonDB)) {
            return false;
        }

        PersonDB person = (PersonDB) o;

        if (firstName != null ? !firstName.equals(person.firstName) : person.firstName != null) {
            return false;
        }
        if (secondName != null ? !secondName.equals(person.secondName) : person.secondName != null) {
            return false;
        }
        if (lastName != null ? !lastName.equals(person.lastName) : person.lastName != null) {
            return false;
        }
        if (relationshipCode != null ? !relationshipCode.equals(person.relationshipCode) : person.relationshipCode != null) {
            return false;
        }
        if (contactRoleCode != null ? !contactRoleCode.equals(person.contactRoleCode) : person.contactRoleCode != null) {
            return false;
        }
        return identifier != null ? identifier.equals(person.identifier) : person.identifier == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (secondName != null ? secondName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (relationshipCode != null ? relationshipCode.hashCode() : 0);
        result = 31 * result + (contactRoleCode != null ? contactRoleCode.hashCode() : 0);
        result = 31 * result + (identifier != null ? identifier.hashCode() : 0);
        return result;
    }

    /**
     * <p>isDefined.</p>
     *
     * @return a boolean.
     */
    public boolean isDefined() {
        return (this.relationshipCode != null && this.hashCode() != this.relationshipCode.hashCode()) || this.hashCode() != 0;
    }

}
