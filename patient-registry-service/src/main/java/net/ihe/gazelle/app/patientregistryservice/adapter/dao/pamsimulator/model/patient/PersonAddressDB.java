package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by aberge on 03/07/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
@Entity
@DiscriminatorValue("person_address")
public class PersonAddressDB extends RawAddressDB {

    @ManyToOne(targetEntity = PersonDB.class)
    @JoinColumn(name = "person_id")
    private PersonDB person;

    /**
     * <p>Constructor for PersonAddressDB.</p>
     */
    public PersonAddressDB(){
        super();
    }

    /**
     * <p>Constructor for PersonAddressDB.</p>
     *
     * @param inPerson a {@link PersonDB} object.
     */
    public PersonAddressDB(PersonDB inPerson){
        super();
        this.person = inPerson;
    }

    /**
     * <p>Constructor for PersonAddressDB.</p>
     *
     * @param inPersonAddress a {@link PersonAddressDB} object.
     */
    public PersonAddressDB(PersonAddressDB inPersonAddress){
        super(inPersonAddress);
    }

    /**
     * <p>Getter for the field <code>person</code>.</p>
     *
     * @return a {@link PersonDB} object.
     */
    public PersonDB getPerson() {
        return person;
    }

    /**
     * <p>Setter for the field <code>person</code>.</p>
     *
     * @param person a {@link PersonDB} object.
     */
    public void setPerson(PersonDB person) {
        this.person = person;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        PersonAddressDB that = (PersonAddressDB) o;

        return person != null ? person.equals(that.person) : that.person == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (person != null ? person.hashCode() : 0);
        return result;
    }
}
