package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search;

import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.converter.PatientConverter;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.lib.searchjpadao.adapter.SearchResultJPAMappingService;

import java.util.ArrayList;
import java.util.List;

/**
 * This service allows the Patient Search DAO to convert the list of Database results to a list of business objects.
 */
public class PatientSearchResultJPAMappingService implements SearchResultJPAMappingService<Patient, PatientDB> {

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Patient> mapToBusiness(List<PatientDB> patientDBS) {
        List<Patient> patients = new ArrayList<>();
        patientDBS.forEach(patientDB -> patients.add(PatientConverter.toPatient(patientDB)));
        return patients;
    }
}
