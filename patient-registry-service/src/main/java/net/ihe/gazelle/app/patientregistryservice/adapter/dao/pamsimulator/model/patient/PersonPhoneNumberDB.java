package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient;

import javax.persistence.*;

/**
 * Created by aberge on 09/03/17.
 */
@Entity
@Table(name = "pam_phone_number", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {"id"}))
@SequenceGenerator(name = "pam_phone_number_sequence", sequenceName = "pam_phone_number_id_seq", allocationSize = 1)
public class PersonPhoneNumberDB extends AbstractPhoneNumberDB {


    @Column(name = "dtype")
    private String dtype = "person_phone_number";

    @ManyToOne(targetEntity = PersonDB.class)
    @JoinColumn(name = "person_id")
    private PersonDB person;

    public PersonPhoneNumberDB(){
        super();
    }

    public PersonPhoneNumberDB(PersonDB inPerson) {
        super();
        this.person = inPerson;
    }

    public PersonDB getPerson() {
        return person;
    }

    public void setPerson(PersonDB person) {
        this.person = person;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        PersonPhoneNumberDB that = (PersonPhoneNumberDB) o;

        return person != null ? person.equals(that.person) : that.person == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (person != null ? person.hashCode() : 0);
        return result;
    }
}
