package net.ihe.gazelle.app.patientregistryservice.adapter.dao;

import javax.enterprise.inject.Produces;
import javax.inject.Qualifier;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Producer for EntityManager used for CDI injection.
 */
public class EntityManagerProducer {

    @Qualifier
    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.FIELD, ElementType.METHOD,
            ElementType.TYPE, ElementType.PARAMETER})
    public @interface InjectEntityManager {}

    @Produces
    @PersistenceContext(unitName = "patientRegistryPU")
    @InjectEntityManager
    private EntityManager entityManager;
}
