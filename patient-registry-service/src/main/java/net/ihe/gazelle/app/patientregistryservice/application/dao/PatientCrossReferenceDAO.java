package net.ihe.gazelle.app.patientregistryservice.application.dao;

import net.ihe.gazelle.app.patientregistryapi.application.SearchCrossReferenceException;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.references.CrossReferenceDB;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.CreationCrossReferenceException;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.DeletionCrossReferenceException;


public interface PatientCrossReferenceDAO {

    /**
     * Method to retrieve matching patient to given systemId and patientId in the system
     *
     * @param systemID          of hte system
     * @param patientIDInSystem ID of the patient in the system.
     * @return The matching patientDB
     * @throws SearchCrossReferenceException if an error occurred during the search
     */
    PatientDB searchForPatientDBWithSourceIdentifier(String systemID, String patientIDInSystem) throws SearchCrossReferenceException;

    /**
     * Search for a list of patientAliases by its patient cross-reference id
     *
     * @param patientDBPixReferenceId retrieve a list of XRef matching with a patient
     * @return a list of cross-reference
     * @throws SearchCrossReferenceException if an error occurred during the search
     */
    CrossReferenceDB searchForPatientAliasesWithPatientDB(Integer patientDBPixReferenceId) throws SearchCrossReferenceException;

    /**
     * Create a CrossReference Entity
     *
     * @param crossReferenceDB crossReference to saved in DB
     * @return ID of the saved CrossReference
     * @throws CreationCrossReferenceException if an error occurred during the creation
     */
    CrossReferenceDB createCrossReference(CrossReferenceDB crossReferenceDB) throws CreationCrossReferenceException;


    void deleteCrossReference(Integer crossReferenceId) throws DeletionCrossReferenceException;

}
