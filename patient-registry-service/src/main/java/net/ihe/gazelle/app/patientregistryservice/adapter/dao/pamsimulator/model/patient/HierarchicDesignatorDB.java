package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient;

import javax.persistence.*;

@Entity
@Table(name = "pam_hierarchic_designator", schema = "public")
@SequenceGenerator(name = "pam_hierarchic_designator_sequence", sequenceName = "pam_hierarchic_designator_id_seq", allocationSize = 1)
@NamedQuery(name = "HierarchicDesignatorDB.findByUniversalID", query = "SELECT hd FROM HierarchicDesignatorDB hd WHERE hd.universalID = :universalID AND hd.usage =: givenUsage")
public class HierarchicDesignatorDB {

    private static final String DEFAULT_UNIVERSAL_ID_TYPE = "ISO";
    private static final String URN_PREFIX = "urn:oid:";

    @Id
    @GeneratedValue(generator = "pam_hierarchic_designator_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /**
     * HD-1
     */
    @Column(name = "namespace_id")
    private String namespaceID;

    /**
     * HD-2
     */
    @Column(name = "universal_id")
    private String universalID;

    /**
     * HD-3
     */
    @Column(name = "universal_id_type")
    private String universalIDType;

    /**
     * populates CX-5
     */
    @Column(name = "type")
    private String type;

    /**
     * if true, can be used by the user to generate identifiers
     */
    @Column(name = "is_tool_assigning_authority")
    private boolean toolAssigningAuthority;

    /**
     * used to build the identifiers in this domain
     */
    @Column(name = "index")
    private Integer index;

    /**
     * default assigning authorities will be preselected in the GUI for identifier creation
     */
    @Column(name = "is_default")
    private boolean defaultAssigningAuthority;

    @Column(name = "cat_usage")
    private boolean catUsage;

    /**
     * prefix used for generating Identifiers
     */
    @Column(name = "prefix")
    private String prefix;

    @Column(name = "usage")
    private DesignatorTypeDB usage;

    public HierarchicDesignatorDB(DesignatorTypeDB inUsage){
        this.defaultAssigningAuthority = false;
        this.toolAssigningAuthority = false;
        this.usage = inUsage;
        this.catUsage = false;
        this.setUniversalIDType("ISO");
    }

    public HierarchicDesignatorDB() {
        this.defaultAssigningAuthority = false;
        this.toolAssigningAuthority = false;
        this.catUsage = false;
        this.setUniversalIDType("ISO");
    }

    public HierarchicDesignatorDB(String namespaceID, String universalID, String universalIDType, DesignatorTypeDB hdType) {
        this.namespaceID = namespaceID;
        this.universalID = universalID;
        this.universalIDType = universalIDType;
        if (hdType != null) {
            this.type = hdType.getIdentifierTypeCode();
        }
        this.toolAssigningAuthority = false;
        this.index = 0;
        this.defaultAssigningAuthority = false;
        this.usage = hdType;
        if (universalIDType == null || universalIDType.isEmpty()){
            this.universalIDType = DEFAULT_UNIVERSAL_ID_TYPE;
        }
        this.catUsage = false;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Integer getId() {
        return id;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public boolean isDefaultAssigningAuthority() {
        return defaultAssigningAuthority;
    }

    public void setDefaultAssigningAuthority(boolean defaultAssigningAuthority) {
        this.defaultAssigningAuthority = defaultAssigningAuthority;
    }


    public String getNamespaceID() {
        return namespaceID;
    }

    public void setNamespaceID(String namespaceID) {
        this.namespaceID = namespaceID;
    }

    public String getUniversalID() {
        return universalID;
    }

    public String getUniversalIDAsUrn(){
        if (universalID == null){
            return null;
        } else if (universalID.startsWith(URN_PREFIX)){
            return universalID;
        } else {
            return URN_PREFIX + universalID;
        }
    }

    public void setUniversalID(String universalID) {
        this.universalID = universalID;
    }

    public String getUniversalIDType() {
        return universalIDType;
    }

    public void setUniversalIDType(String universalIDType) {
        this.universalIDType = universalIDType;
    }

    /**
     * returns the current object as an encoding HD datatype (uses ^ as components separator)
     */
    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        if (this.namespaceID != null) {
            string.append(this.namespaceID);
        }
        string.append("^");
        if (this.universalID != null) {
            string.append(universalID);
        }
        string.append("^");
        if (this.universalIDType != null) {
            string.append(universalIDType);
        }
        return string.toString();
    }



    public String displayWithAmp() {
        String string = toString();
        string = string.replace('^', '&');
        return string.concat(" (" + type + ')');
    }

    public boolean isToolAssigningAuthority() {
        return toolAssigningAuthority;
    }

    public void setToolAssigningAuthority(boolean toolAssigningAuthority) {
        this.toolAssigningAuthority = toolAssigningAuthority;
        if (!this.toolAssigningAuthority){
            setCatUsage(false);
        }
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public DesignatorTypeDB getUsage() {
        return usage;
    }

    public void setUsage(DesignatorTypeDB usage) {
        this.usage = usage;
    }

    public boolean isEmpty() {
        return hashCode() == 0;
    }

    public boolean isCatUsage() {
        return catUsage;
    }

    public void setCatUsage(boolean catUsage) {
        this.catUsage = catUsage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HierarchicDesignatorDB)) {
            return false;
        }

        HierarchicDesignatorDB that = (HierarchicDesignatorDB) o;

        if (namespaceID != null ? !namespaceID.equals(that.namespaceID) : that.namespaceID != null) {
            return false;
        }
        if (universalID != null ? !universalID.equals(that.universalID) : that.universalID != null) {
            return false;
        }
        if (universalIDType != null ? !universalIDType.equals(that.universalIDType) : that.universalIDType != null) {
            return false;
        }
        if (type != null ? !type.equals(that.type) : that.type != null) {
            return false;
        }
        return prefix != null ? prefix.equals(that.prefix) : that.prefix == null;

    }

    @Override
    public int hashCode() {
        int result = namespaceID != null ? namespaceID.hashCode() : 0;
        result = 31 * result + (universalID != null ? universalID.hashCode() : 0);
        result = 31 * result + (universalIDType != null ? universalIDType.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (prefix != null ? prefix.hashCode() : 0);
        return result;
    }
}
