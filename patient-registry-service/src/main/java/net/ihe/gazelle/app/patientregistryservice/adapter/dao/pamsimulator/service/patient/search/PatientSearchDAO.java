package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search;

import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.EntityManagerProducer;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.lib.searchjpadao.adapter.SearchCriterionJPAMappingService;
import net.ihe.gazelle.lib.searchjpadao.adapter.SearchJPADAO;
import net.ihe.gazelle.lib.searchjpadao.adapter.SearchResultJPAMappingService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

/**
 * Concrete JPA DAO for a patient Search.
 */
@Named("searchDAOService")
public final class PatientSearchDAO extends SearchJPADAO<Patient, PatientDB> {

    /**
     * Default constructor for the class.
     * @param entityManager                 {@link EntityManager} to use to perform actions on Database
     * @param criterionJPAMappingService    {@link SearchCriterionJPAMappingService} mapping Patient Search Criteria to JPA path and properties
     * @param resultJPAMappingService       {@link SearchResultJPAMappingService} mapping JPA results to business objects
     */
    @Inject
    public PatientSearchDAO(@EntityManagerProducer.InjectEntityManager EntityManager entityManager,
                            SearchCriterionJPAMappingService criterionJPAMappingService,
                            SearchResultJPAMappingService<Patient,PatientDB> resultJPAMappingService) {
        super(entityManager, criterionJPAMappingService, resultJPAMappingService);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<PatientDB> getJPAEntityClass(){
        return PatientDB.class;
    }
}
