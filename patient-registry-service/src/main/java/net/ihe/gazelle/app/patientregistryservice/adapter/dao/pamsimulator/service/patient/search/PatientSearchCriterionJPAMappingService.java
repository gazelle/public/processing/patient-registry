package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search;

import net.ihe.gazelle.app.patientregistryapi.business.PatientSearchCriterionKey;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.EntityManagerProducer;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.HierarchicDesignatorDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientIdentifierDB;
import net.ihe.gazelle.lib.searchjpadao.adapter.SearchCriterionJPAMappingService;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.key.SearchCriterionKey;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;

/**
 * Concrete {@link SearchCriterionJPAMappingService} for Patients.
 */
public class PatientSearchCriterionJPAMappingService implements SearchCriterionJPAMappingService {


    private static final String PATIENT_IDENTIFIER_ATTRIBUTE = "patientIdentifiers";
    private static final String DOMAIN_ATTRIBUTE = "domain";
    private EntityManager entityManager;

    /**
     * Constructor for injection
     * @param entityManager     {@link EntityManager} to use to build JPA criteria.
     */
    @Inject
    public PatientSearchCriterionJPAMappingService(@EntityManagerProducer.InjectEntityManager EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public From from(SearchCriterionKey searchCriterionKey, From from) {
        PatientSearchCriterionKey patientSearchCriterionKey = castToPatientKey(searchCriterionKey);
        Metamodel metamodel = entityManager.getMetamodel();
        EntityType<PatientDB> entityType = metamodel.entity(PatientDB.class);
        EntityType<PatientIdentifierDB> entityTypeIdentifier = metamodel.entity(PatientIdentifierDB.class);
        if (PatientSearchCriterionKey.UUID.equals(patientSearchCriterionKey)) {
            return from;
        } else if (PatientSearchCriterionKey.IDENTIFIER.equals(patientSearchCriterionKey)) {
            From from2 = (From) from.getJoins().stream()
                    .filter(p ->
                            ((Join) p).getAttribute().getName().equals(PATIENT_IDENTIFIER_ATTRIBUTE) &&
                                    JoinType.LEFT.equals(((Join) p).getJoinType()))
                    .findAny().orElse(null);
            return from2 != null ? from2 : from.join(entityType.getList(PATIENT_IDENTIFIER_ATTRIBUTE, PatientIdentifierDB.class), JoinType.LEFT);
        } else if (PatientSearchCriterionKey.DOMAIN.equals(patientSearchCriterionKey)) {
            From from2 = (From) from.getJoins().stream()
                    .filter(p ->
                            ((Join) p).getAttribute().getName().equals(PATIENT_IDENTIFIER_ATTRIBUTE) &&
                                    JoinType.LEFT.equals(((Join) p).getJoinType()))
                    .findAny().orElse(null);
            from2 = from2 != null ? from2 : from.join(entityType.getList(PATIENT_IDENTIFIER_ATTRIBUTE, PatientIdentifierDB.class), JoinType.LEFT);
            From from3 = (From) from2.getJoins().stream()
                    .filter(p ->
                            ((Join) p).getAttribute().getName().equals(DOMAIN_ATTRIBUTE) &&
                                    JoinType.LEFT.equals(((Join) p).getJoinType()))
                    .findAny().orElse(null);
            return from3 != null ? from3 : from2.join(entityTypeIdentifier.getSingularAttribute(DOMAIN_ATTRIBUTE, HierarchicDesignatorDB.class),
                    JoinType.LEFT);
        } else {
            throw new IllegalArgumentException("Unsupported test key : " + patientSearchCriterionKey);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEntityProperty(SearchCriterionKey searchCriterionKey) {
        PatientSearchCriterionKey patientSearchCriterionKey = castToPatientKey(searchCriterionKey);
        if (PatientSearchCriterionKey.UUID.equals(patientSearchCriterionKey)) {
            return "uuid";
        } else if (PatientSearchCriterionKey.IDENTIFIER.equals(patientSearchCriterionKey)) {
            return "identifier";
        } else if (PatientSearchCriterionKey.DOMAIN.equals(patientSearchCriterionKey)) {
            return "universalID";
        } else {
            throw new IllegalArgumentException("Unsupported test key : " + patientSearchCriterionKey);
        }
    }

    /**
     * Cast a generic {@link SearchCriterionKey} to a specific {@link PatientSearchCriterionKey}
     * @param key   value of the key to cast.
     * @return the casted {@link PatientSearchCriterionKey}
     */
    private PatientSearchCriterionKey castToPatientKey(SearchCriterionKey key){
        try {
            return (PatientSearchCriterionKey) key;
        } catch (ClassCastException e){
            throw new IllegalArgumentException("Criterion Key cannot be casted to Patient Criterion key !");
        }
    }
}
