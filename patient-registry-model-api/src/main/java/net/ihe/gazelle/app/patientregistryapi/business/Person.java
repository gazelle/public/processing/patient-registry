/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistryapi.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>Person class.</p>
 * This class represent a person, it might be a patient but can be a relative or a practitioner
 *
 * @author abe
 * @version 1.0: 11/10/2019
 */

public class Person {

    private List<PersonName> names = new ArrayList<>();

    private GenderCode gender;

    private Date dateOfBirth;

    private List<Address> addresses = new ArrayList<>();

    private List<ContactPoint> contactPoints = new ArrayList<>();

    private List<Observation> observations = new ArrayList<>();

    /**
     * Gets the value of the name property.
     *
     * <p>
     * This accessor method returns a copy of the live list.
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PersonName }
     *
     * @return the list of names of the person
     */
    public List<PersonName> getNames() {
        return new ArrayList<>(this.names);
    }

    /**
     * Add a person name to a patient
     *
     * @param name the name to add
     */
    public void addName(PersonName name) {
        this.names.add(name);
    }

    /**
     * Add an personName to a patient
     *
     * @param name  the personName to add
     * @param index the index to insert the personName
     */
    public void addName(PersonName name, int index) {
        this.names.add(index, name);
    }

    /**
     * Remove an personName to a patient
     *
     * @param name the personName to remove
     */
    public void removeName(PersonName name) {
        this.names.remove(name);
    }

    /**
     * Getter for the gender property.
     *
     * @return possible object is
     * {@link GenderCode }
     */
    public GenderCode getGender() {
        return gender;
    }

    /**
     * Setter for the gender property
     *
     * @param value allowed object is
     *              {@link GenderCode }
     */
    public void setGender(GenderCode value) {
        this.gender = value;
    }

    /**
     * Getter for the dateOfBirth property.
     *
     * @return possible object is
     * {@link Date }
     */
    public Date getDateOfBirth() {
        if (dateOfBirth != null) {
            return (Date) dateOfBirth.clone();
        }
        return null;
    }

    /**
     * Setter for the dateOfBirth property.
     *
     * @param value allowed object is
     *              {@link Date }
     */
    public void setDateOfBirth(Date value) {
        if (value != null) {
            this.dateOfBirth = (Date) value.clone();
        } else {
            dateOfBirth = null;
        }
    }

    /**
     * Gets the value of the address property.
     *
     * <p>
     * This accessor method returns a copy of the live list.
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Address }
     *
     * @return the list of Addresses of the person
     */
    public List<Address> getAddresses() {
        return new ArrayList<>(this.addresses);
    }


    /**
     * Add an address to a patient
     *
     * @param address the address to add
     */
    public void addAddress(Address address) {
        this.addresses.add(address);
    }

    /**
     * Add an address to a patient
     *
     * @param address the address to add
     * @param index   the index to insert the address
     */
    public void addAddress(Address address, int index) {
        this.addresses.add(index, address);
    }

    /**
     * Remove an address to a patient
     *
     * @param address the address to remove
     */
    public void removeAddress(Address address) {
        this.addresses.remove(address);
    }


    /**
     * Gets the value of the contactPoint property.
     *
     * <p>
     * This accessor method returns a copy of the live list.
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactPoint }
     *
     * @return the list of Contact Points of the person
     */
    public List<ContactPoint> getContactPoints() {
        return new ArrayList<>(this.contactPoints);
    }

    /**
     * Add a contactPoint to a patient
     *
     * @param contactPoint the contactPoint to add
     */
    public void addContactPoint(ContactPoint contactPoint) {
        this.contactPoints.add(contactPoint);
    }

    /**
     * Add an contactPoint to a patient
     *
     * @param contactPoint the contactPoint to add
     * @param index        the index to insert the contactPoint
     */
    public void addContactPoint(ContactPoint contactPoint, int index) {
        this.contactPoints.add(index, contactPoint);
    }

    /**
     * Remove an contactPoint to a patient
     *
     * @param contactPoint the contactPoint to remove
     */
    public void removeContactPoint(ContactPoint contactPoint) {
        this.contactPoints.remove(contactPoint);
    }

    /**
     * Gets the value of the observation property.
     *
     * <p>
     * This accessor method returns a copy of the live list.
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Observation }
     *
     * @return the list of Observations of the Person
     */
    public List<Observation> getObservations() {
        return new ArrayList<>(this.observations);
    }

    /**
     * Add an observation to a patient
     *
     * @param observation the observation to add
     */
    public void addObservation(Observation observation) {
        this.observations.add(observation);
    }

    /**
     * Add an observation to a patient
     *
     * @param observation the observation to add
     * @param index       the index to insert the observation
     */
    public void addObservation(Observation observation, int index) {
        this.observations.add(index, observation);
    }

    /**
     * Remove an observation to a patient
     *
     * @param observation the observation to remove
     */
    public void removeObservation(Observation observation) {
        this.observations.remove(observation);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Person person = (Person) o;

        if (!names.equals(person.names)) {
            return false;
        }
        if (gender != person.gender) {
            return false;
        }
        if (dateOfBirth != null ? !dateOfBirth.equals(person.dateOfBirth) : person.dateOfBirth != null) {
            return false;
        }
        if (!addresses.equals(person.addresses)) {
            return false;
        }
        if (!contactPoints.equals(person.contactPoints)) {
            return false;
        }
        return observations.equals(person.observations);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = names.hashCode();
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (dateOfBirth != null ? dateOfBirth.hashCode() : 0);
        result = 31 * result + addresses.hashCode();
        result = 31 * result + contactPoints.hashCode();
        result = 31 * result + observations.hashCode();
        return result;
    }
}
