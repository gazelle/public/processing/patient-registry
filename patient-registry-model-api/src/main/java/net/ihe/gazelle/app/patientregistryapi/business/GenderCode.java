
package net.ihe.gazelle.app.patientregistryapi.business;


/**
 * Possible gender codes for the patient
 */
public enum GenderCode {

    MALE,
    FEMALE,
    UNDEFINED,
    OTHER;

    /**
     * get enum member name
     * @return enum member name
     */
    public String value() {
        return name();
    }

}
