
package net.ihe.gazelle.app.patientregistryapi.business;


/**
 * Patient contact point
 */
public class ContactPoint {

    protected ContactPointType type;
    protected ContactPointUse use;
    protected String value;

    /**
     * default constructor
     */
    public ContactPoint() {
    }

    /**
     * complete constructor
     * @param type contact point type
     * @param use contact point use
     * @param value contact point value
     */
    public ContactPoint(ContactPointType type, ContactPointUse use, String value) {
        this.setType(type);
        this.setUse(use);
        this.setValue(value);
    }

    /**
     * get contact type
     * 
     * @return
     *     possible object is
     *     {@link ContactPointType }
     *     
     */
    public ContactPointType getType() {
        return type;
    }

    /**
     * set contact type
     * 
     * @param value
     *     allowed object is
     *     {@link ContactPointType }
     *     
     */
    public void setType(ContactPointType value) {
        this.type = value;
    }

    /**
     * get contact use
     * 
     * @return
     *     possible object is
     *     {@link ContactPointUse }
     *     
     */
    public ContactPointUse getUse() {
        return use;
    }

    /**
     * set contact use
     * 
     * @param value
     *     allowed object is
     *     {@link ContactPointUse }
     *     
     */
    public void setUse(ContactPointUse value) {
        this.use = value;
    }

    /**
     * get contact value
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * set contact value
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ContactPoint that = (ContactPoint) o;

        if (type != that.type) {
            return false;
        }
        if (use != that.use) {
            return false;
        }
        return value != null ? value.equals(that.value) : that.value == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (use != null ? use.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
