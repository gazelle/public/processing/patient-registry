
package net.ihe.gazelle.app.patientregistryapi.business;


/**
 * observations to add to the patient
 */
public class Observation {

    protected ObservationType observationType;
    protected String value;

    /**
     * complete constructor
     * @param observationType observation type
     * @param value observation value
     */
    public Observation(ObservationType observationType, String value) {
        this.setObservationType(observationType);
        this.setValue(value);
    }

    /**
     * default constructor
     */
    public Observation() {
    }

    /**
     * get the observation type
     * 
     * @return
     *     possible object is
     *     {@link ObservationType }
     *     
     */
    public ObservationType getObservationType() {
        return observationType;
    }

    /**
     * set the observation type
     * 
     * @param value
     *     allowed object is
     *     {@link ObservationType }
     *     
     */
    public void setObservationType(ObservationType value) {
        this.observationType = value;
    }

    /**
     * get the observation value
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * set the observation value
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Observation that = (Observation) o;

        if (observationType != that.observationType) {
            return false;
        }
        return value != null ? value.equals(that.value) : that.value == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = observationType != null ? observationType.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
