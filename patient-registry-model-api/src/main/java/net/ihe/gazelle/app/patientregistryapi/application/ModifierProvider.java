package net.ihe.gazelle.app.patientregistryapi.application;

public interface ModifierProvider {
    String CONTAINS = "contains";
    String NOT_EQUALS = "ne";
    String LT = "lt";
    String EB = "eb";
    String LE = "le";
    String GT = "gt";
    String SA = "sa";
    String GE = "ge";
    String AP = "ap";
    String DEFAULT_VALUE = "=";

    String getQueryOperator(String modifier);

}
