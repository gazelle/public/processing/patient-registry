
package net.ihe.gazelle.app.patientregistryapi.business;


/**
 * Possible address use for the patient
 */
public enum AddressUse {

    BIRTH_PLACE,
    BIRTH_DELIVERY,
    HOME,
    PRIMARY_HOME,
    WORK,
    TEMPORARY,
    VACATION_HOME,
    BAD,
    BILLING,
    DIRECT,
    PUBLIC,
    COUNTRY_OF_ORIGIN,
    LEGAL,
    MAILING,
    BIRTH_RESIDENCE,
    SERVICE,
    SHIPPING;

    /**
     * get enum member name
     * @return enum member name
     */
    public String value() {
        return name();
    }


}
