package net.ihe.gazelle.app.patientregistryapi.application;

public interface PatientFieldsMatcherService {
    public String getMapValue(String key);
}
