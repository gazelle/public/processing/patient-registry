
package net.ihe.gazelle.app.patientregistryapi.business;

import java.util.ArrayList;
import java.util.List;


/**
 * Representation of a person name
 */
public class PersonName {

    private String family;
    private List<String> givens = new ArrayList<>();
    private String prefix;
    private String suffix;
    private String use;

    /**
     * Default constructor for the class.
     */
    public PersonName() {
    }

    /**
     * Constructor for the class.
     *
     * @param family value to set to the family property
     * @param prefix value to set to the prefix property
     * @param suffix value to set to the suffix property
     * @param use    value to set to the use property
     */
    public PersonName(String family, String prefix, String suffix, String use) {
        this.setFamily(family);
        this.setPrefix(prefix);
        this.setSuffix(suffix);
        this.setUse(use);
    }

    /**
     * get the family name
     *
     * @return possible object is
     * {@link String }
     */
    public String getFamily() {
        return family;
    }

    /**
     * set the family name
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFamily(String value) {
        this.family = value;
    }

    /**
     * Gets the value of the givens property.
     *
     * <p>
     * This accessor method returns a snapshot to the live list.
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     *
     * @return a copy of the list of Given Names.
     */
    public List<String> getGivens() {
        return new ArrayList<>(this.givens);
    }

    /**
     * add a given name to a patient
     *
     * @param given the given name to add
     */
    public void addGiven(String given) {
        this.givens.add(given);
    }

    /**
     * add a given name to a patient
     *
     * @param given the given name to add
     * @param index the index to insert the given name
     */
    public void addGiven(String given, int index) {
        this.givens.add(index, given);
    }

    /**
     * remove a given name to a patient
     *
     * @param given the given name to remove
     */
    public void removeGiven(String given) {
        this.givens.remove(given);
    }

    /**
     * get the person name prefix
     *
     * @return possible object is
     * {@link String }
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * set the person name prefix
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPrefix(String value) {
        this.prefix = value;
    }

    /**
     * get the person name suffix
     *
     * @return possible object is
     * {@link String }
     */
    public String getSuffix() {
        return suffix;
    }

    /**
     * Set a person name suffix
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSuffix(String value) {
        this.suffix = value;
    }

    /**
     * Get person name
     *
     * @return possible object is
     * {@link String }
     */
    public String getUse() {
        return use;
    }

    /**
     * Set person name usage
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setUse(String value) {
        this.use = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PersonName that = (PersonName) o;

        if (family != null ? !family.equals(that.family) : that.family != null) {
            return false;
        }
        if (!givens.equals(that.givens)) {
            return false;
        }
        if (prefix != null ? !prefix.equals(that.prefix) : that.prefix != null) {
            return false;
        }
        if (suffix != null ? !suffix.equals(that.suffix) : that.suffix != null) {
            return false;
        }
        return use != null ? use.equals(that.use) : that.use == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = family != null ? family.hashCode() : 0;
        result = 31 * result + givens.hashCode();
        result = 31 * result + (prefix != null ? prefix.hashCode() : 0);
        result = 31 * result + (suffix != null ? suffix.hashCode() : 0);
        result = 31 * result + (use != null ? use.hashCode() : 0);
        return result;
    }
}
