package net.ihe.gazelle.app.patientregistryapi.business;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Representation of a patient
 */
public class Patient extends Person {

    private String uuid;
    private List<EntityIdentifier> identifiers = new ArrayList<>();
    private boolean active;
    private Date dateOfDeath;
    private Integer multipleBirthOrder;
    private List<Person> contacts = new ArrayList<>();

    /**
     * default constructor
     */
    public Patient() {
    }

    /**
     * Complete constructor
     *
     * @param uuid               the gazelle id of the patient
     * @param active             the status of the patient
     * @param dateOfBirth        the date of birth of the patient
     * @param dateOfDeath        the date of death of the patient
     * @param gender             the gender of the patient
     * @param multipleBirthOrder the birth order in case of multiple birth
     */
    public Patient(String uuid, boolean active, Date dateOfBirth, Date dateOfDeath, GenderCode gender, Integer multipleBirthOrder) {
        this.setUuid(uuid);
        this.setActive(active);
        this.setDateOfBirth(dateOfBirth);
        this.setDateOfDeath(dateOfDeath);
        this.setGender(gender);
        this.setMultipleBirthOrder(multipleBirthOrder);
    }


    /**
     * get patient gazelle identifier
     *
     * @return gazelle identifier
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * set patient gazelle identifier
     *
     * @param uuid value to set to the uuid property
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * is patient still active
     *
     * @return patient activity
     */
    public boolean isActive() {
        return this.active;
    }

    /**
     * set patient activity
     *
     * @param active true if patient still active
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Obtient la valeur de la propriété dateOfDeath.
     *
     * @return possible object is
     * {@link Date }
     */
    public Date getDateOfDeath() {
        if (dateOfDeath != null) {
            return (Date) dateOfDeath.clone();
        }
        return null;
    }

    /**
     * Définit la valeur de la propriété dateOfDeath.
     *
     * @param value allowed object is
     *              {@link Date }
     */
    public void setDateOfDeath(Date value) {
        if (value != null) {
            this.dateOfDeath = (Date) value.clone();
        } else {
            this.dateOfDeath = null;
        }
    }

    /**
     * Gets the value of the identifiers property.
     *
     * <p>
     * This accessor method returns a snapshot to the live list.
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EntityIdentifier }
     *
     * @return the list of identifier of the Patient
     */
    public List<EntityIdentifier> getIdentifiers() {
        return new ArrayList<>(this.identifiers);
    }

    /**
     * add an identifier to a patient
     *
     * @param identifier the identifier to add
     */
    public void addIdentifier(EntityIdentifier identifier) {
        this.identifiers.add(identifier);
    }

    /**
     * add an identifier to a patient
     *
     * @param identifier the identifier to add
     * @param index      the index to insert the identifier
     */
    public void addIdentifier(EntityIdentifier identifier, int index) {
        this.identifiers.add(index, identifier);
    }

    /**
     * remove an identifier to a patient
     *
     * @param identifier the identifier to remove
     */
    public void removeIdentifier(EntityIdentifier identifier) {
        this.identifiers.remove(identifier);
    }

    /**
     * Obtient la valeur de la propriété multipleBirthOrder.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public Integer getMultipleBirthOrder() {
        return multipleBirthOrder;
    }

    /**
     * Définit la valeur de la propriété multipleBirthOrder.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setMultipleBirthOrder(Integer value) {
        this.multipleBirthOrder = value;
    }

    /**
     * Gets the value of the contacts property.
     *
     * <p>
     * This accessor method returns a snapshot to the live list.
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Person }
     *
     * @return the list of Contacts for the Patient
     */
    public List<Person> getContacts() {
        return new ArrayList<>(this.contacts);
    }

    /**
     * add a contact to a patient
     *
     * @param person the person to add
     */
    public void addContact(Person person) {
        this.contacts.add(person);
    }

    /**
     * add a person to a patient
     *
     * @param person the person to add
     * @param index  the index to insert the person
     */
    public void addContact(Person person, int index) {
        this.contacts.add(index, person);
    }

    /**
     * remove a person to a patient
     *
     * @param person the identifier to remove
     */
    public void removeContact(Person person) {
        this.contacts.remove(person);
    }

    /**
     * Checks identity-equality between two {@link Patient} entities.
     *
     * @param patient : patient to compare to this entity
     *
     * @return if both entity are equals.
     */
    public boolean identityEquals(Patient patient) {
        return this.uuid != null && this.uuid.equals(patient.getUuid());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Patient patient = (Patient) o;

        if (active != patient.active) {
            return false;
        }
        if (uuid != null ? !uuid.equals(patient.uuid) : patient.uuid != null) {
            return false;
        }
        if (!identifiers.equals(patient.identifiers)) {
            return false;
        }
        if (dateOfDeath != null ? !dateOfDeath.equals(patient.dateOfDeath) : patient.dateOfDeath != null) {
            return false;
        }
        if (multipleBirthOrder != null ? !multipleBirthOrder.equals(patient.multipleBirthOrder) : patient.multipleBirthOrder != null) {
            return false;
        }
        return contacts.equals(patient.contacts);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (uuid != null ? uuid.hashCode() : 0);
        result = 31 * result + identifiers.hashCode();
        result = 31 * result + (active ? 1 : 0);
        result = 31 * result + (dateOfDeath != null ? dateOfDeath.hashCode() : 0);
        result = 31 * result + (multipleBirthOrder != null ? multipleBirthOrder.hashCode() : 0);
        result = 31 * result + contacts.hashCode();
        return result;
    }
}
