package net.ihe.gazelle.app.patientregistryapi.business;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * test Obeservation class
 */
class ObservationTest {

    /**
     * test default constructor
     */
    @Test
    public void observationDefaultConstructorTest() {
        Observation observation = new Observation();
        assertNotNull(observation);
    }

    /**
     * test complete constructor
     */
    @Test
    public void observationCompleteConstructorTest() {
        ObservationType observationType = ObservationType.ACCOUNT_NUMBER;
        String value = "value";

        Observation observation = new Observation(observationType, value);

        assertEquals(observationType, observation.getObservationType());
        assertEquals(observationType.value(), observation.getObservationType().value());
        assertEquals(value, observation.getValue());
    }

    /**
     * Test equals on Observations and null
     */
    @Test
    public void equals_Observation_null(){
        Observation observation = new Observation();
        assertFalse(observation.equals(null),"Observation shall not be equal to null!");
    }

    /**
     * Test equals on same Observations
     */
    @Test
    public void equals_Observation_same(){
        Observation observation = new Observation();
        assertTrue(observation.equals(observation),"Observation shall be equal!");
    }

    /**
     * Test equals on Observations and anotherClass
     */
    @Test
    public void equals_Observation_other_class(){
        Observation observation = new Observation();
        assertFalse(observation.equals("Merci."),"Observation shall not be equal to a string !");
    }

    /**
     * Test equals on Observations value equals
     */
    @Test
    public void equals_Observation_value_true(){
        String value= "1.2.3.4";
        Observation observation = new Observation();
        observation.setValue(value);
        Observation observation1 = new Observation();
        observation1.setValue(value);
        assertTrue(observation.equals(observation1),"Observation shall be equal !");
    }

    /**
     * Test equals on Observations value not equals
     */
    @Test
    public void equals_Observation_value_false(){
        String value= "1.2.3.4";
        String value1= "5.6.7.8";
        Observation observation = new Observation();
        observation.setValue(value);
        Observation observation1 = new Observation();
        observation1.setValue(value1);
        assertFalse(observation.equals(observation1),"Observation shall not be equal !");
    }

    /**
     * Test equals on Observations observationType equals
     */
    @Test
    public void equals_Observation_observationType_true(){
        ObservationType observationType= ObservationType.ACCOUNT_NUMBER;
        Observation observation = new Observation();
        observation.setObservationType(observationType);
        Observation observation1 = new Observation();
        observation1.setObservationType(observationType);
        assertTrue(observation.equals(observation1),"Observation shall be equal !");
    }

    /**
     * Test equals on Observations observationType not equals
     */
    @Test
    public void equals_Observation_observationType_false(){
        ObservationType observationType= ObservationType.ACCOUNT_NUMBER;
        ObservationType observationType1= ObservationType.HEIGHT;
        Observation observation = new Observation();
        observation.setObservationType(observationType);
        Observation observation1 = new Observation();
        observation1.setObservationType(observationType1);
        assertFalse(observation.equals(observation1),"Observation shall not be equal !");
    }
}