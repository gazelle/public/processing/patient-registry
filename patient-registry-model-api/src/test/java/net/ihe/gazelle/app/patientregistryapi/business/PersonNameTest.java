package net.ihe.gazelle.app.patientregistryapi.business;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * test person name class
 */
class PersonNameTest {

    /**
     * test default constructor
     */
    @Test
    public void personNameDefaultConstructorTest() {
        PersonName name = new PersonName();
        assertTrue(name.getGivens().isEmpty());
    }


    /**
     * test complete constructor
     */
    @Test
    public void personNameCompleteConstructorTest() {
        String family = "family";
        String prefix = "prefix";
        String suffix = "suffix";
        String use = "use";

        PersonName name = new PersonName(family, prefix, suffix, use);

        assertTrue(name.getGivens().isEmpty());
        assertEquals(family, name.getFamily());
        assertEquals(prefix, name.getPrefix());
        assertEquals(suffix, name.getSuffix());
        assertEquals(use, name.getUse());
    }


    /**
     * test add given to list
     */
    @Test
    public void personNameListsTest() {
        String given = "given";

        PersonName name = new PersonName();

        name.addGiven(given);

        assertTrue(name.getGivens().contains(given));
    }

    /**
     * test given name list add given name to index
     */
    @Test
    public void addGivenNameToIndexTest() {
        PersonName personName = new PersonName();
        for(int i=0; i<5; i++) {
            personName.addGiven("");
        }
        int index = 3;
        String givenToAdd = "givenToAdd";
        personName.addGiven(givenToAdd,index);

        assertEquals(givenToAdd,personName.getGivens().get(index));
    }

    /**
     * test address list remove address
     */
    @Test
    public void removeAddressToIndexTest() {
        PersonName personName = new PersonName();
        for(int i=0; i<5; i++) {
            personName.addGiven("");
        }
        int index = 3;
        String givenToAddAndRemove = "givenToAddAndRemove";
        personName.addGiven(givenToAddAndRemove,index);
        personName.removeGiven(givenToAddAndRemove);

        assertFalse(personName.getGivens().contains(givenToAddAndRemove));
    }

    /**
     * Test equals on PersonName and null
     */
    @Test
    public void equals_PersonName_null(){
        PersonName personName = new PersonName();
        assertFalse(personName.equals(null),"PersonName shall not be equal to null!");
    }

    /**
     * Test equals on same PersonName
     */
    @Test
    public void equals_PersonName_same(){
        PersonName personName = new PersonName();
        assertTrue(personName.equals(personName),"PersonNamees shall be equal!");
    }

    /**
     * Test equals on PersonName and another class
     */
    @Test
    public void equals_PersonName_other_class(){
        PersonName personName = new PersonName();
        assertFalse(personName.equals("Jean-Jacques"),"PersonName shall not be equal to a string !");
    }

    /**
     * Test equals on PersonName family equals
     */
    @Test
    public void equals_PersonName_family_true(){
        String family= "Bars";
        PersonName personName = new PersonName();
        personName.setFamily(family);
        PersonName personName1 = new PersonName();
        personName1.setFamily(family);
        assertTrue(personName.equals(personName1),"PersonName shall be equal !");
    }

    /**
     * Test equals on PersonName family not equals
     */
    @Test
    public void equals_PersonName_family_false(){
        String family= "Bars";
        String family1= "Srab";
        PersonName personName = new PersonName();
        personName.setFamily(family);
        PersonName personName1 = new PersonName();
        personName1.setFamily(family1);
        assertFalse(personName.equals(personName1),"PersonName shall not be equal !");
    }

    /**
     * Test equals on PersonName prefix equals
     */
    @Test
    public void equals_PersonName_prefix_true(){
        String prefix= "de";
        PersonName personName = new PersonName();
        personName.setPrefix(prefix);
        PersonName personName1 = new PersonName();
        personName1.setPrefix(prefix);
        assertTrue(personName.equals(personName1),"PersonName shall be equal !");
    }

    /**
     * Test equals on PersonName prefix not equals
     */
    @Test
    public void equals_PersonName_prefix_false(){
        String prefix= "de";
        String prefix1= "von";
        PersonName personName = new PersonName();
        personName.setPrefix(prefix);
        PersonName personName1 = new PersonName();
        personName1.setPrefix(prefix1);
        assertFalse(personName.equals(personName1),"PersonName shall not be equal !");
    }

    /**
     * Test equals on PersonName suffix equals
     */
    @Test
    public void equals_PersonName_suffix_true(){
        String suffix= "de";
        PersonName personName = new PersonName();
        personName.setSuffix(suffix);
        PersonName personName1 = new PersonName();
        personName1.setSuffix(suffix);
        assertTrue(personName.equals(personName1),"PersonName shall be equal !");
    }

    /**
     * Test equals on PersonName suffix not equals
     */
    @Test
    public void equals_PersonName_suffix_false(){
        String suffix= "de";
        String suffix1= "von";
        PersonName personName = new PersonName();
        personName.setSuffix(suffix);
        PersonName personName1 = new PersonName();
        personName1.setSuffix(suffix1);
        assertFalse(personName.equals(personName1),"PersonName shall not be equal !");
    }

    /**
     * Test equals on PersonName use equals
     */
    @Test
    public void equals_PersonName_use_true(){
        String use= "use1";
        PersonName personName = new PersonName();
        personName.setUse(use);
        PersonName personName1 = new PersonName();
        personName1.setUse(use);
        assertTrue(personName.equals(personName1),"PersonName shall be equal !");
    }

    /**
     * Test equals on PersonName use not equals
     */
    @Test
    public void equals_PersonName_use_false(){
        String use= "use1";
        String use1= "use2";
        PersonName personName = new PersonName();
        personName.setUse(use);
        PersonName personName1 = new PersonName();
        personName1.setUse(use1);
        assertFalse(personName.equals(personName1),"PersonName shall not be equal !");
    }

    /**
     * Test equals on PersonName given equals
     */
    @Test
    public void equals_PersonName_given_true(){
        String given= "Wylem";
        PersonName personName = new PersonName();
        personName.addGiven(given);
        PersonName personName1 = new PersonName();
        personName1.addGiven(given);
        assertTrue(personName.equals(personName1),"Addresses shall be equal !");
    }

    /**
     * Test equals on PersonName given not equals
     */
    @Test
    public void equals_PersonName_given_false(){
        String given= "Wylem";
        String given1= "Alexandre";
        PersonName personName = new PersonName();
        personName.addGiven(given);
        PersonName personName1 = new PersonName();
        personName1.addGiven(given1);
        assertFalse(personName.equals(personName1),"Addresses shall not be equal !");
    }

    /**
     * Test equals on PersonName given with different size
     */
    @Test
    public void equals_PersonName_given_different_size(){
        String given= "Wylem";
        PersonName personName = new PersonName();
        personName.addGiven(given);
        PersonName personName1 = new PersonName();
        assertFalse(personName.equals(personName1),"Addresses shall not be equal !");
    }

    /**
     * Test equals on PersonName givens in different order
     */
    @Test
    public void equals_PersonName_given_different_order(){
        String given= "Wylem";
        String given1= "Alexandre";
        PersonName personName = new PersonName();
        personName.addGiven(given);
        personName.addGiven(given1);
        PersonName personName1 = new PersonName();
        personName1.addGiven(given1);
        personName1.addGiven(given);
        assertFalse(personName.equals(personName1),"Addresses shall not be equal !");
    }
}