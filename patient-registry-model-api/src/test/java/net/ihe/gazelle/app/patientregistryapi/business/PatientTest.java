package net.ihe.gazelle.app.patientregistryapi.business;


import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * tests for Patient class
 */
class PatientTest {

    /**
     * test default constructor
     */
    @Test
    public void patientDefaultConstructorTest() {
        Patient patient = new Patient();

        assertTrue(patient.getNames().isEmpty());
        assertTrue(patient.getAddresses().isEmpty());
        assertTrue(patient.getContactPoints().isEmpty());
        assertTrue(patient.getIdentifiers().isEmpty());
        assertTrue(patient.getObservations().isEmpty());
    }

    /**
     * test complete constructor
     */
    @Test
    public void patientCompleteConstructorTest() {
        String uuid = "uuid";
        Date dateOfBirth = new Date();
        Date dateOfDeath = new Date();
        GenderCode gender = GenderCode.FEMALE;
        Integer multipleBirthOrder = Integer.valueOf("1");

        Patient patient = new Patient(uuid, true, dateOfBirth, dateOfDeath, gender, multipleBirthOrder);


        assertTrue(patient.getNames().isEmpty());
        assertTrue(patient.getAddresses().isEmpty());
        assertTrue(patient.getContactPoints().isEmpty());
        assertTrue(patient.getIdentifiers().isEmpty());
        assertTrue(patient.getObservations().isEmpty());
        assertEquals(uuid, patient.getUuid());
        assertEquals(dateOfBirth, patient.getDateOfBirth());
        assertEquals(dateOfDeath, patient.getDateOfDeath());
        assertEquals(gender, patient.getGender());
        assertEquals(gender.value(), patient.getGender().value());
        assertEquals(multipleBirthOrder, patient.getMultipleBirthOrder());
    }

    /**
     * test name list add name
     */
    @Test
    public void addNameTest() {
        Patient patient = new Patient();
        patient.addName(new PersonName());

        assertFalse(patient.getNames().isEmpty());
    }

    /**
     * test name list add name to index
     */
    @Test
    public void addNameToIndexTest() {
        Patient patient = new Patient();
        for(int i=0; i<5; i++) {
            patient.addName(new PersonName());
        }
        int index = 3;
        PersonName personNameToAdd = new PersonName();
        patient.addName(personNameToAdd,index);

        assertEquals(personNameToAdd,patient.getNames().get(index));
    }

    /**
     * test name list remove name
     */
    @Test
    public void removeNameToIndexTest() {
        Patient patient = new Patient();
        for(int i=0; i<5; i++) {
            patient.addName(new PersonName());
        }
        int index = 3;
        PersonName personNameToAddAndRemove = new PersonName();
        personNameToAddAndRemove.setFamily("Family");
        patient.addName(personNameToAddAndRemove,index);
        patient.removeName(personNameToAddAndRemove);

        assertFalse(patient.getNames().contains(personNameToAddAndRemove));
    }

    /**
     * test identifiers list add identifier
     */
    @Test
    public void addIdentifierTest() {
        Patient patient = new Patient();
        patient.addIdentifier(new EntityIdentifier());

        assertFalse(patient.getIdentifiers().isEmpty());
    }

    /**
     * test identifier list add identifier to index
     */
    @Test
    public void addIdentifierToIndexTest() {
        Patient patient = new Patient();
        for(int i=0; i<5; i++) {
            patient.addIdentifier(new EntityIdentifier());
        }
        int index = 3;
        EntityIdentifier identifierToAdd = new EntityIdentifier();
        patient.addIdentifier(identifierToAdd,index);

        assertEquals(identifierToAdd,patient.getIdentifiers().get(index));
    }

    /**
     * test identifier list remove identifier
     */
    @Test
    public void removeIdentifierToIndexTest() {
        Patient patient = new Patient();
        for(int i=0; i<5; i++) {
            patient.addIdentifier(new EntityIdentifier());
        }
        int index = 3;
        EntityIdentifier identifierToAddAndRemove = new EntityIdentifier();
        identifierToAddAndRemove.setValue("1.2.3.4");
        patient.addIdentifier(identifierToAddAndRemove,index);
        patient.removeIdentifier(identifierToAddAndRemove);

        assertFalse(patient.getIdentifiers().contains(identifierToAddAndRemove));
    }

    /**
     * test Address list add Address
     */
    @Test
    public void addAddressTest() {
        Patient patient = new Patient();
        patient.addAddress(new Address());

        assertFalse(patient.getAddresses().isEmpty());
    }

    /**
     * test address list add address to index
     */
    @Test
    public void addAddressToIndexTest() {
        Patient patient = new Patient();
        for(int i=0; i<5; i++) {
            patient.addAddress(new Address());
        }
        int index = 3;
        Address addressToAdd = new Address();
        patient.addAddress(addressToAdd,index);

        assertEquals(addressToAdd,patient.getAddresses().get(index));
    }

    /**
     * test address list remove address
     */
    @Test
    public void removeAddressToIndexTest() {
        Patient patient = new Patient();
        for(int i=0; i<5; i++) {
            patient.addAddress(new Address());
        }
        int index = 3;
        Address addressToAddAndRemove = new Address();
        addressToAddAndRemove.addLine("line");
        patient.addAddress(addressToAddAndRemove,index);
        patient.removeAddress(addressToAddAndRemove);

        assertFalse(patient.getAddresses().contains(addressToAddAndRemove));
    }

    /**
     * test ContactPoint list add ContactPoint
     */
    @Test
    public void addContactPointTest() {
        Patient patient = new Patient();
        patient.addContactPoint(new ContactPoint());

        assertFalse(patient.getContactPoints().isEmpty());
    }

    /**
     * test contactPoint list add contactPoint to index
     */
    @Test
    public void addContactPointToIndexTest() {
        Patient patient = new Patient();
        for(int i=0; i<5; i++) {
            patient.addContactPoint(new ContactPoint());
        }
        int index = 3;
        ContactPoint contactPointToAdd = new ContactPoint();
        patient.addContactPoint(contactPointToAdd,index);

        assertEquals(contactPointToAdd,patient.getContactPoints().get(index));
    }

    /**
     * test contactPoint list remove contactPoint
     */
    @Test
    public void removeContactPointToIndexTest() {
        Patient patient = new Patient();
        for(int i=0; i<5; i++) {
            patient.addContactPoint(new ContactPoint());
        }
        int index = 3;
        ContactPoint contactPointToAddAndRemove = new ContactPoint();
        contactPointToAddAndRemove.setValue("wbs@tortueninja.de");
        patient.addContactPoint(contactPointToAddAndRemove,index);
        patient.removeContactPoint(contactPointToAddAndRemove);

        assertFalse(patient.getContactPoints().contains(contactPointToAddAndRemove));
    }

    /**
     * test Observation list add Observation
     */
    @Test
    public void addObservationTest() {
        Patient patient = new Patient();
        patient.addObservation(new Observation());

        assertFalse(patient.getObservations().isEmpty());
    }

    /**
     * test observation list add observation to index
     */
    @Test
    public void addObservationToIndexTest() {
        Patient patient = new Patient();
        for(int i=0; i<5; i++) {
            patient.addObservation(new Observation());
        }
        int index = 3;
        Observation observationToAdd = new Observation();
        patient.addObservation(observationToAdd,index);

        assertEquals(observationToAdd,patient.getObservations().get(index));
    }

    /**
     * test observation list remove observation
     */
    @Test
    public void removeObservationToIndexTest() {
        Patient patient = new Patient();
        for(int i=0; i<5; i++) {
            patient.addObservation(new Observation());
        }
        int index = 3;
        Observation observationToAddAndRemove = new Observation();
        observationToAddAndRemove.setValue("Test");
        patient.addObservation(observationToAddAndRemove,index);
        patient.removeObservation(observationToAddAndRemove);

        assertFalse(patient.getObservations().contains(observationToAddAndRemove));
    }

    /**
     * Test equals on Patient and Null
     */
    @Test
    public void equals_Patient_null(){
        Patient patient = new Patient();
        assertFalse(patient.equals(null),"Patient shall not be equal to null!");
    }

    /**
     * Test equals on same Patient
     */
    @Test
    public void equals_Patient_same(){
        Patient patient = new Patient();
        assertTrue(patient.equals(patient),"Patientes shall be equal!");
    }

    /**
     * Test equals on Patient and another class
     */
    @Test
    public void equals_Patient_other_class(){
        Patient patient = new Patient();
        assertFalse(patient.equals("Malade"),"Patient shall not be equal to city string !");
    }

    /**
     * Test equals on Patient uuid equals
     */
    @Test
    public void equals_Patient_uuid_true(){
        String uuid= "uuid1";
        Patient patient = new Patient();
        patient.setUuid(uuid);
        Patient patient1 = new Patient();
        patient1.setUuid(uuid);
        assertTrue(patient.equals(patient1),"Patient shall be equal !");
    }

    /**
     * Test equals on Patient uuid not equals
     */
    @Test
    public void equals_Patient_uuid_false(){
        String uuid= "uuid1";
        String uuid1= "uuid2";
        Patient patient = new Patient();
        patient.setUuid(uuid);
        Patient patient1 = new Patient();
        patient1.setUuid(uuid1);
        assertFalse(patient.equals(patient1),"Patient shall not be equal !");
    }

    /**
     * Test equals on Patient active equals
     */
    @Test
    public void equals_Patient_active_true(){
        Patient patient = new Patient();
        patient.setActive(true);
        Patient patient1 = new Patient();
        patient1.setActive(true);
        assertTrue(patient.equals(patient1),"Patient shall be equal !");
    }

    /**
     * Test equals on Patient active not equals
     */
    @Test
    public void equals_Patient_active_false(){
        Patient patient = new Patient();
        patient.setActive(true);
        Patient patient1 = new Patient();
        patient1.setActive(false);
        assertFalse(patient.equals(patient1),"Patient shall not be equal !");
    }

    /**
     * Test equals on Patient dateOfDeath equals
     */
    @Test
    public void equals_Patient_dateOfDeath_true(){
        Date dateOfDeath= new Date();
        Patient patient = new Patient();
        patient.setDateOfDeath(dateOfDeath);
        Patient patient1 = new Patient();
        patient1.setDateOfDeath(dateOfDeath);
        assertTrue(patient.equals(patient1),"Patient shall be equal !");
    }

    /**
     * Test equals on Patient dateOfDeath not equals
     */
    @Test
    public void equals_Patient_dateOfDeath_false(){
        Date dateOfDeath= new Date();
        Date dateOfDeath1= new Date(Long.valueOf("2112121211212"));
        Patient patient = new Patient();
        patient.setDateOfDeath(dateOfDeath);
        Patient patient1 = new Patient();
        patient1.setDateOfDeath(dateOfDeath1);
        assertFalse(patient.equals(patient1),"Patient shall not be equal !");
    }

    /**
     * Test equals on Patient multipleBirthOrder equals
     */
    @Test
    public void equals_Patient_multipleBirthOrder_true(){
        Integer multipleBirthOrder= Integer.valueOf("1");
        Patient patient = new Patient();
        patient.setMultipleBirthOrder(multipleBirthOrder);
        Patient patient1 = new Patient();
        patient1.setMultipleBirthOrder(multipleBirthOrder);
        assertTrue(patient.equals(patient1),"Patient shall be equal !");
    }

    /**
     * Test equals on Patient multipleBirthOrder not equals
     */
    @Test
    public void equals_Patient_multipleBirthOrder_false(){
        Integer multipleBirthOrder= Integer.valueOf("1");
        Integer multipleBirthOrder1= Integer.valueOf("2");
        Patient patient = new Patient();
        patient.setMultipleBirthOrder(multipleBirthOrder);
        Patient patient1 = new Patient();
        patient1.setMultipleBirthOrder(multipleBirthOrder1);
        assertFalse(patient.equals(patient1),"Patient shall not be equal !");
    }

    /**
     * Test equals on Patient identifiers equals
     */
    @Test
    public void equals_Patient_identifier_true(){
        EntityIdentifier identifier= new EntityIdentifier();
        Patient patient = new Patient();
        patient.addIdentifier(identifier);
        Patient patient1 = new Patient();
        patient1.addIdentifier(identifier);
        assertTrue(patient.equals(patient1),"Identifieres shall be equal !");
    }

    /**
     * Test equals on Patient identifiers not equals
     */
    @Test
    public void equals_Patient_identifier_false(){
        EntityIdentifier identifier= new EntityIdentifier();
        EntityIdentifier identifier1= new EntityIdentifier();
        identifier1.setValue("Rennes");
        Patient patient = new Patient();
        patient.addIdentifier(identifier);
        Patient patient1 = new Patient();
        patient1.addIdentifier(identifier1);
        assertFalse(patient.equals(patient1),"Identifieres shall not be equal !");
    }

    /**
     * Test equals on Patient identifiers different size
     */
    @Test
    public void equals_Patient_identifier_different_size(){
        EntityIdentifier identifier= new EntityIdentifier();
        Patient patient = new Patient();
        patient.addIdentifier(identifier);
        Patient patient1 = new Patient();
        assertFalse(patient.equals(patient1),"Identifieres shall not be equal !");
    }

    /**
     * Test equals on Patient idnetifiers different order
     */
    @Test
    public void equals_Patient_identifier_different_order(){
        EntityIdentifier identifier= new EntityIdentifier();
        EntityIdentifier identifier1= new EntityIdentifier();
        identifier1.setValue("Rennes");
        Patient patient = new Patient();
        patient.addIdentifier(identifier);
        patient.addIdentifier(identifier1);
        Patient patient1 = new Patient();
        patient1.addIdentifier(identifier1);
        patient1.addIdentifier(identifier);
        assertFalse(patient.equals(patient1),"Identifieres shall not be equal !");
    }

    /**
     * Test equals on Patient contact equals
     */
    @Test
    public void equals_Patient_contact_true(){
        Person contact= new Person();
        Patient patient = new Patient();
        patient.addContact(contact);
        Patient patient1 = new Patient();
        patient1.addContact(contact);
        assertTrue(patient.equals(patient1),"Contactes shall be equal !");
    }

    /**
     * Test equals on Patient contact equals
     */
    @Test
    public void equals_Patient_contact_false(){
        Person contact= new Person();
        Person contact1= new Person();
        contact1.setGender(GenderCode.MALE);
        Patient patient = new Patient();
        patient.addContact(contact);
        Patient patient1 = new Patient();
        patient1.addContact(contact1);
        assertFalse(patient.equals(patient1),"Contactes shall not be equal !");
    }

    /**
     * Test equals on Patient contact different size
     */
    @Test
    public void equals_Patient_contact_different_size(){
        Person contact= new Person();
        Patient patient = new Patient();
        patient.addContact(contact);
        Patient patient1 = new Patient();
        assertFalse(patient.equals(patient1),"Contactes shall not be equal !");
    }

    /**
     * Test equals on Patient contact different order
     */
    @Test
    public void equals_Patient_contact_different_order(){
        Person contact= new Person();
        Person contact1= new Person();
        contact1.setGender(GenderCode.MALE);
        Patient patient = new Patient();
        patient.addContact(contact);
        patient.addContact(contact1);
        Patient patient1 = new Patient();
        patient1.addContact(contact1);
        patient1.addContact(contact);
        assertFalse(patient.equals(patient1),"Contactes shall not be equal !");
    }

    /**
     * Test get contact methods returns a copy of the list.
     */
    @Test
    public void get_contacts_duplicate_list(){
        Person contact= new Person();
        Patient patient = new Patient();
        patient.addContact(contact);

        List<Person> contacts = patient.getContacts();
        contacts.clear();

        assertFalse(patient.getContacts().isEmpty());
    }

    /**
     * Test add contact at the correct index
     */
    @Test
    public void add_contact_at_index(){
        Person contact= new Person();
        contact.setGender(GenderCode.FEMALE);
        Person contact1= new Person();
        contact1.setGender(GenderCode.MALE);
        Patient patient = new Patient();

        patient.addContact(contact);
        patient.addContact(contact1, 0);

        assertEquals(GenderCode.MALE, patient.getContacts().get(0).getGender());
    }

    /**
     * Test remove contact
     */
    @Test
    public void remove_contact(){
        Person contact= new Person();
        contact.setGender(GenderCode.FEMALE);
        Person contact1= new Person();
        contact1.setGender(GenderCode.MALE);
        Patient patient = new Patient();
        patient.addContact(contact);
        patient.addContact(contact1);

        patient.removeContact(contact1);

        assertEquals(1, patient.getContacts().size());
        assertEquals(GenderCode.FEMALE, patient.getContacts().get(0).getGender());
    }

    /**
     * Test identity Equal on two equal entity
     */
    @Test
    public void identity_equal_true(){
        String uuid = "uuid";
        Patient patient = new Patient();
        patient.setUuid(uuid);
        patient.setActive(true);
        Patient patient1 = new Patient();
        patient1.setUuid(uuid);
        patient1.setActive(false);

        assertTrue(patient.identityEquals(patient1));
    }

    /**
     * Test identity Equal on two not equal entity
     */
    @Test
    public void identity_equal_false(){
        Patient patient = new Patient();
        patient.setUuid("uuid");
        patient.setActive(true);
        Patient patient1 = new Patient();
        patient1.setUuid("uuid1");
        patient1.setActive(true);

        assertFalse(patient.identityEquals(patient1));
    }
}