package net.ihe.gazelle.app.patientregistryapi.business;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * test ContactPoint class
 */
class ContactPointTest {

    /**
     * test default constructor
     */
    @Test
    public void contactPointDefaultConstructorTest() {
        ContactPoint contactPoint = new ContactPoint();
        assertNotNull(contactPoint);
    }

    /**
     * test complete constructor
     */
    @Test
    public void contactPointCompleteConstructorTest() {
        ContactPointType type = ContactPointType.BEEPER;
        ContactPointUse use = ContactPointUse.ANSWERING_SERVICE;
        String value = "value";

        ContactPoint contactPoint = new ContactPoint(type, use, value);

        assertEquals(type, contactPoint.getType());
        assertEquals(use, contactPoint.getUse());
        assertEquals(type.value(), contactPoint.getType().value());
        assertEquals(use.value(), contactPoint.getUse().value());
        assertEquals(value, contactPoint.getValue());
    }

    /**
     * Test equals on ContactPoint and null
     */
    @Test
    public void equals_ContactPoint_null(){
        assertFalse(new ContactPoint().equals(null),"ContactPoint shall not be equal to null!");
    }

    /**
     * Test equals on same ContactPoint
     */
    @Test
    public void equals_ContactPoint_same(){
        ContactPoint contactPoint = new ContactPoint();
        assertTrue(contactPoint.equals(contactPoint),"ContactPoint shall be equal!");
    }

    /**
     * Test equals on ContactPoint and another class
     */
    @Test
    public void equals_ContactPoint_other_class(){
        ContactPoint contactPoint = new ContactPoint();
        assertFalse(contactPoint.equals("Tarte"),"Address shall not be equal to a string !");
    }

    /**
     * Test equals on ContactPoint type equals
     */
    @Test
    public void equals_ContactPoint_type_true(){
        ContactPointType type= ContactPointType.EMAIL;
        ContactPoint contactPoint = new ContactPoint();
        contactPoint.setType(type);
        ContactPoint contactPoint1 = new ContactPoint();
        contactPoint1.setType(type);
        assertTrue(contactPoint.equals(contactPoint1),"ContactPoint shall be equal !");
    }

    /**
     * Test equals on ContactPoint type notequals
     */
    @Test
    public void equals_ContactPoint_type_false(){
        ContactPointType type= ContactPointType.EMAIL;
        ContactPointType type1= ContactPointType.PHONE;
        ContactPoint contactPoint = new ContactPoint();
        contactPoint.setType(type);
        ContactPoint contactPoint1 = new ContactPoint();
        contactPoint1.setType(type1);
        assertFalse(contactPoint.equals(contactPoint1),"ContactPoint shall not be equal !");
    }

    /**
     * Test equals on ContactPoint use equals
     */
    @Test
    public void equals_ContactPoint_use_true(){
        ContactPointUse use= ContactPointUse.EMAIL;
        ContactPoint contactPoint = new ContactPoint();
        contactPoint.setUse(use);
        ContactPoint contactPoint1 = new ContactPoint();
        contactPoint1.setUse(use);
        assertTrue(contactPoint.equals(contactPoint1),"ContactPoint shall be equal !");
    }

    /**
     * Test equals on ContactPoint use not equals
     */
    @Test
    public void equals_ContactPoint_use_false(){
        ContactPointUse use= ContactPointUse.EMAIL;
        ContactPointUse use1= ContactPointUse.BEEPER;
        ContactPoint contactPoint = new ContactPoint();
        contactPoint.setUse(use);
        ContactPoint contactPoint1 = new ContactPoint();
        contactPoint1.setUse(use1);
        assertFalse(contactPoint.equals(contactPoint1),"ContactPoint shall not be equal !");
    }

    /**
     * Test equals on ContactPoint value equals
     */
    @Test
    public void equals_ContactPoint_value_true(){
        String value= "wbs@tortueninja.it";
        ContactPoint contactPoint = new ContactPoint();
        contactPoint.setValue(value);
        ContactPoint contactPoint1 = new ContactPoint();
        contactPoint1.setValue(value);
        assertTrue(contactPoint.equals(contactPoint1),"ContactPoint shall be equal !");
    }

    /**
     * Test equals on ContactPoint value not equals
     */
    @Test
    public void equals_ContactPoint_value_false(){
        String value= "wbs@tortueninja.it";
        String value1= "0701236499";
        ContactPoint contactPoint = new ContactPoint();
        contactPoint.setValue(value);
        ContactPoint contactPoint1 = new ContactPoint();
        contactPoint1.setValue(value1);
        assertFalse(contactPoint.equals(contactPoint1),"ContactPoint shall not be equal !");
    }

}