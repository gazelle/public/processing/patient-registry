package net.ihe.gazelle.app.patientregistryapi.business;

import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test for the Person business object
 */
public class PersonTest {

    /**
     * Test equals on Person and null
     */
    @Test
    public void equals_Person_null(){
        Person person = new Person();
        assertFalse(person.equals(null),"Person shall not be equal to null!");
    }

    /**
     * Test equals on same Person
     */
    @Test
    public void equals_Person_same(){
        Person person = new Person();
        assertTrue(person.equals(person),"Persones shall be equal!");
    }

    /**
     * Test equals on Person and another class
     */
    @Test
    public void equals_Person_other_class(){
        Person person = new Person();
        assertFalse(person.equals("Malade"),"Person shall not be equal to city string !");
    }

    /**
     * Test equals on Person dateOfBirth equals
     */
    @Test
    public void equals_Person_dateOfBirth_true(){
        Date dateOfBirth= new Date();
        Person person = new Person();
        person.setDateOfBirth(dateOfBirth);
        Person person1 = new Person();
        person1.setDateOfBirth(dateOfBirth);
        assertTrue(person.equals(person1),"Person shall be equal !");
    }

    /**
     * Test equals on Person dateOfBirth not equals
     */
    @Test
    public void equals_Person_dateOfBirth_false(){
        Date dateOfBirth= new Date();
        Date dateOfBirth1= new Date(Long.valueOf("22332232232332"));
        Person person = new Person();
        person.setDateOfBirth(dateOfBirth);
        Person person1 = new Person();
        person1.setDateOfBirth(dateOfBirth1);
        assertFalse(person.equals(person1),"Person shall not be equal !");
    }

    /**
     * Test equals on Person gender equals
     */
    @Test
    public void equals_Person_gender_true(){
        GenderCode gender= GenderCode.MALE;
        Person person = new Person();
        person.setGender(gender);
        Person person1 = new Person();
        person1.setGender(gender);
        assertTrue(person.equals(person1),"Person shall be equal !");
    }

    /**
     * Test equals on Person gender not equals
     */
    @Test
    public void equals_Person_gender_false(){
        GenderCode gender= GenderCode.MALE;
        GenderCode gender1= GenderCode.FEMALE;
        Person person = new Person();
        person.setGender(gender);
        Person person1 = new Person();
        person1.setGender(gender1);
        assertFalse(person.equals(person1),"Person shall not be equal !");
    }

    /**
     * Test equals on Person name equals
     */
    @Test
    public void equals_Person_name_true(){
        PersonName name= new PersonName();
        Person person = new Person();
        person.addName(name);
        Person person1 = new Person();
        person1.addName(name);
        assertTrue(person.equals(person1),"Addresses shall be equal !");
    }

    /**
     * Test equals on Person name not equals
     */
    @Test
    public void equals_Person_name_false(){
        PersonName name= new PersonName();
        PersonName name1= new PersonName();
        name1.setFamily("True");
        Person person = new Person();
        person.addName(name);
        Person person1 = new Person();
        person1.addName(name1);
        assertFalse(person.equals(person1),"Addresses shall not be equal !");
    }

    /**
     * Test equals on Person name different size
     */
    @Test
    public void equals_Person_name_different_size(){
        PersonName name= new PersonName();
        Person person = new Person();
        person.addName(name);
        Person person1 = new Person();
        assertFalse(person.equals(person1),"Addresses shall not be equal !");
    }

    /**
     * Test equals on Person name different order
     */
    @Test
    public void equals_Person_name_different_order(){
        PersonName name= new PersonName();
        PersonName name1= new PersonName();
        name1.setFamily("True");
        Person person = new Person();
        person.addName(name);
        person.addName(name1);
        Person person1 = new Person();
        person1.addName(name1);
        person1.addName(name);
        assertFalse(person.equals(person1),"Addresses shall not be equal !");
    }

    /**
     * Test equals on Person address equals
     */
    @Test
    public void equals_Person_address_true(){
        Address address= new Address();
        Person person = new Person();
        person.addAddress(address);
        Person person1 = new Person();
        person1.addAddress(address);
        assertTrue(person.equals(person1),"Addresses shall be equal !");
    }

    /**
     * Test equals on Person address not equals
     */
    @Test
    public void equals_Person_address_false(){
        Address address= new Address();
        Address address1= new Address();
        address1.setCity("Rennes");
        Person person = new Person();
        person.addAddress(address);
        Person person1 = new Person();
        person1.addAddress(address1);
        assertFalse(person.equals(person1),"Addresses shall not be equal !");
    }

    /**
     * Test equals on Person address different size
     */
    @Test
    public void equals_Person_address_different_size(){
        Address address= new Address();
        Person person = new Person();
        person.addAddress(address);
        Person person1 = new Person();
        assertFalse(person.equals(person1),"Addresses shall not be equal !");
    }

    /**
     * Test equals on Person address different order
     */
    @Test
    public void equals_Person_address_different_order(){
        Address address= new Address();
        Address address1= new Address();
        address1.setCity("Rennes");
        Person person = new Person();
        person.addAddress(address);
        person.addAddress(address1);
        Person person1 = new Person();
        person1.addAddress(address1);
        person1.addAddress(address);
        assertFalse(person.equals(person1),"Addresses shall not be equal !");
    }

    /**
     * Test equals on Person contactPoint equals
     */
    @Test
    public void equals_Person_contactPoint_true(){
        ContactPoint contactPoint= new ContactPoint();
        Person person = new Person();
        person.addContactPoint(contactPoint);
        Person person1 = new Person();
        person1.addContactPoint(contactPoint);
        assertTrue(person.equals(person1),"ContactPointes shall be equal !");
    }

    /**
     * Test equals on Person ContactPoints not equals
     */
    @Test
    public void equals_Person_contactPoint_false(){
        ContactPoint contactPoint= new ContactPoint();
        ContactPoint contactPoint1= new ContactPoint();
        contactPoint1.setValue("Rennes");
        Person person = new Person();
        person.addContactPoint(contactPoint);
        Person person1 = new Person();
        person1.addContactPoint(contactPoint1);
        assertFalse(person.equals(person1),"ContactPointes shall not be equal !");
    }

    /**
     * Test equals on Person contactPoints different size
     */
    @Test
    public void equals_Person_contactPoint_different_size(){
        ContactPoint contactPoint= new ContactPoint();
        Person person = new Person();
        person.addContactPoint(contactPoint);
        Person person1 = new Person();
        assertFalse(person.equals(person1),"ContactPointes shall not be equal !");
    }

    /**
     * Test equals on Person contactPoint different order
     */
    @Test
    public void equals_Person_contactPoint_different_order(){
        ContactPoint contactPoint= new ContactPoint();
        ContactPoint contactPoint1= new ContactPoint();
        contactPoint1.setValue("Rennes");
        Person person = new Person();
        person.addContactPoint(contactPoint);
        person.addContactPoint(contactPoint1);
        Person person1 = new Person();
        person1.addContactPoint(contactPoint1);
        person1.addContactPoint(contactPoint);
        assertFalse(person.equals(person1),"ContactPointes shall not be equal !");
    }

    /**
     * Test equals on Person observation equals
     */
    @Test
    public void equals_Person_observation_true(){
        Observation observation= new Observation();
        Person person = new Person();
        person.addObservation(observation);
        Person person1 = new Person();
        person1.addObservation(observation);
        assertTrue(person.equals(person1),"Observationes shall be equal !");
    }

    /**
     * Test equals on Person observation not equals
     */
    @Test
    public void equals_Person_observation_false(){
        Observation observation= new Observation();
        Observation observation1= new Observation();
        observation1.setValue("Rennes");
        Person person = new Person();
        person.addObservation(observation);
        Person person1 = new Person();
        person1.addObservation(observation1);
        assertFalse(person.equals(person1),"Observationes shall not be equal !");
    }

    /**
     * Test equals on Person observation different size
     */
    @Test
    public void equals_Person_observation_different_size(){
        Observation observation= new Observation();
        Person person = new Person();
        person.addObservation(observation);
        Person person1 = new Person();
        assertFalse(person.equals(person1),"Observationes shall not be equal !");
    }

    /**
     * Test equals on Person observation different order
     */
    @Test
    public void equals_Person_observation_different_order(){
        Observation observation= new Observation();
        Observation observation1= new Observation();
        observation1.setValue("Rennes");
        Person person = new Person();
        person.addObservation(observation);
        person.addObservation(observation1);
        Person person1 = new Person();
        person1.addObservation(observation1);
        person1.addObservation(observation);
        assertFalse(person.equals(person1),"Observationes shall not be equal !");
    }
}
