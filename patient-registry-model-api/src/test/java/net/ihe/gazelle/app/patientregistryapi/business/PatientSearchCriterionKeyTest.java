package net.ihe.gazelle.app.patientregistryapi.business;

import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.operator.StringSearchCriterionOperator;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.option.StringSearchCriterionOptions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class PatientSearchCriterionKeyTest {

    @Test
    public void test_uuid_key(){
        PatientSearchCriterionKey key = PatientSearchCriterionKey.UUID;

        assertEquals(String.class, key.getValueClass());
        assertEquals(StringSearchCriterionOperator.EXACT, key.getDefaultOperator());
        assertNotNull(key.getDefaultOptions());
        assertEquals(StringSearchCriterionOptions.class, key.getDefaultOptions().getClass());
    }

    @Test
    public void test_identifier_key(){
        PatientSearchCriterionKey key = PatientSearchCriterionKey.IDENTIFIER;

        assertEquals(String.class, key.getValueClass());
        assertEquals(StringSearchCriterionOperator.EXACT, key.getDefaultOperator());
        assertNotNull(key.getDefaultOptions());
        assertEquals(StringSearchCriterionOptions.class, key.getDefaultOptions().getClass());
    }

    @Test
    public void test_domain_key(){
        PatientSearchCriterionKey key = PatientSearchCriterionKey.DOMAIN;

        assertEquals(String.class, key.getValueClass());
        assertEquals(StringSearchCriterionOperator.EXACT, key.getDefaultOperator());
        assertNotNull(key.getDefaultOptions());
        assertEquals(StringSearchCriterionOptions.class, key.getDefaultOptions().getClass());
    }
}
