package net.ihe.gazelle.app.patientregistryfeedclient.adapter;

import com.gitb.core.AnyContent;
import com.gitb.ps.Void;
import com.gitb.ps.*;
import com.gitb.tr.ObjectFactory;
import com.gitb.tr.*;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PersonName;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperObjectToAnyContent;
import net.ihe.gazelle.lib.gitbutils.adapter.MappingException;

import java.math.BigInteger;

import static net.ihe.gazelle.app.patientregistryapi.adapter.ws.PatientProcessingWebserviceConstants.*;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Test implementation of processing service used to mock responses received by PatientFeedClient
 */
public class TestProcessingService implements ProcessingService {

    public static final String UUID = "TEST-uuid1";
    public static final String UUID_dupli="TEST-uuid2";
    public static final Patient PATIENT = new Patient();


    private ResponseTypes responseToReturn;

    /**
     * Default constructor for the class. By default, the service returns a valid success answer.
     */
    public TestProcessingService(){
        responseToReturn = ResponseTypes.CORRECT;
    }

    /**
     * Setter to the responseToReturn property
     * @param responseToReturn
     */
    public void setResponseToReturn(ResponseTypes responseToReturn){
        this.responseToReturn = responseToReturn;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GetModuleDefinitionResponse getModuleDefinition(Void aVoid) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProcessResponse process(ProcessRequest processRequest) {
        ProcessResponse processResponse;
        TAR report;
        BAR itemContent;
        com.gitb.tr.ObjectFactory objectFactory = new ObjectFactory();
        switch(responseToReturn){
            case CORRECT:
                return getCorrectProcessResponse();
            case MISSING_REPORT:
                return getResponseMissingReport();
            case FAILED_REPORT:
                return getResponseWithFailedReport("MyError");
            case MULTIPLE_OUTPUT:
                return getResponseWithMultipleOutputs();
            case INVALID_OUTPUT:
                return getResponseWithInvalidOutput();
            case NULL_RESPONSE:
                return null;
            case UNEXPECTED_STATUS:
                return getResponseWithUnexpectedReportStatus();
            case NO_REPORTS_IN_REPORT:
                return getResponseWithoutReports();
            case NO_ERROR_IN_FAILURE_REPORT:
                return getResponseWithoutErrorInFailureReport();
            case NO_ERROR_DESCRIPTION:
               return getResponseWithFailedReport(null);
            case ILLEGAL_ARGUMENT_EXCEPTION:
                throw new IllegalArgumentException();
            case UNSUPPORTED_OPERATION_EXCEPTION:
                throw new UnsupportedOperationException();
        /*---------------------DELETE METHODS CASES------------------------------*/
            case DELETE_STATUS_DONE:
                return getDoneDeleteProcessResponse();
            case DELETE_STATUS_GONE:
                return getGoneDeleteProcessResponse();
            case DELETE_STATUS_WRONG:
                return getWrongDeleteProcessResponse();
            case DELETE_FAILED_REPORT:
                return getDeleteResponseWithFailedReport("MyError");
            case DELETE_MISSING_REPORT:
                return getDeleteResponseMissingReport();
            case DELETE_MULTIPLE_OUTPUT:
                return getDeleteResponseWithMultipleOutputs();
            case DELETE_UNEXPECTED_STATUS:
                return getDeleteResponseWithUnexpectedReportStatus();
            case DELETE_NO_REPORTS_IN_REPORT:
                return getDeleteResponseWithoutReports();
            case DELETE_NO_ERROR_IN_FAILURE_REPORT:
                return getDeleteResponseWithoutErrorInFailureReport();
            case DELETE_NO_ERROR_DESCRIPTION:
                return getDeleteResponseWithFailedReport(null);
        /*---------------------UPDATE METHODS CASES------------------------------*/
            case UPDATE_CORRECT:
                return getUpdateCorrectProcessResponse();
            case UPDATE_MISSING_REPORT:
                return getUpdateResponseMissingReport();
            case UPDATE_FAILED_REPORT:
                return getUpdateResponseWithFailedReport("MyError");
            case UPDATE_MULTIPLE_OUTPUT:
                return getUpdateResponseWithMultipleOutputs();
            case UPDATE_UNEXPECTED_STATUS:
                return getUpdateResponseWithUnexpectedReportStatus();
            case UPDATE_NO_REPORTS_IN_REPORT:
                return getUpdateResponseWithoutReports();
            case UPDATE_NO_ERROR_IN_FAILURE_REPORT:
                return getUpdateResponseWithoutErrorInFailureReport();
            case UPDATE_NO_ERROR_DESCRIPTION:
                return getUpdateResponseWithFailedReport(null);
            /*---------------------MERGE METHODS CASES------------------------------*/

            case MERGE_CORRECT:
                return getMergeCorrectProcessResponse();
            case MERGE_MISSING_REPORT:
                return getMergeResponseMissingReport();
            case MERGE_FAILED_REPORT:
                return getMergeResponseWithFailedReport("MyError");
            case MERGE_MULTIPLE_OUTPUT:
                return getMergeResponseWithMultipleOutputs();
            case MERGE_UNEXPECTED_STATUS:
                return getMergeResponseWithUnexpectedReportStatus();
            case MERGE_NO_REPORTS_IN_REPORT:
                return getMergeResponseWithoutReports();
            case MERGE_NO_ERROR_IN_FAILURE_REPORT:
                return getMergeResponseWithoutErrorInFailureReport();
            case MERGE_NO_ERROR_DESCRIPTION:
                return getMergeResponseWithFailedReport(null);

            default:
                return null;
        }
    }

    /**
     * Return a correct {@link ProcessResponse} to a feed Process Request
     * @return a correct {@link ProcessResponse}
     */
    private ProcessResponse getCorrectProcessResponse(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.SUCCESS);
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(UUID_OUTPUT_NAME, UUID));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Return a {@link ProcessResponse} with no Report
     * @return a {@link ProcessResponse} with no report
     */
    private ProcessResponse getResponseMissingReport(){
        ProcessResponse processResponse = new ProcessResponse();
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(UUID_OUTPUT_NAME, UUID));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Return a valid {@link ProcessResponse} for a failed feed request.
     * @param errorDescription   Value of the Error's description for the report
     * @return the failed {@link ProcessResponse}
     */
    private ProcessResponse getResponseWithFailedReport(String errorDescription){
        com.gitb.tr.ObjectFactory objectFactory = new ObjectFactory();
        ProcessResponse processResponse = new ProcessResponse();

        TAR report = new TAR();
        report.setResult(TestResultType.FAILURE);
        report.setReports(new TestAssertionGroupReportsType());

        BAR itemContent = new BAR();
        itemContent.setDescription(errorDescription);
        report.getReports().getInfoOrWarningOrError().add(objectFactory.createTestAssertionGroupReportsTypeError(itemContent));

        report.setCounters(new ValidationCounters());
        report.getCounters().setNrOfAssertions(BigInteger.valueOf(0));
        report.getCounters().setNrOfWarnings(BigInteger.valueOf(0));
        report.getCounters().setNrOfErrors(BigInteger.valueOf(1));

        report.setContext(new AnyContent());
        report.getContext().getItem().add(new AnyContent());
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(UUID_OUTPUT_NAME, UUID));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} with multiple outputs
     * @return a {@link ProcessResponse} with multiple outputs
     */
    private ProcessResponse getResponseWithMultipleOutputs(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.SUCCESS);
        processResponse.setReport(report);
        try {
            AnyContent anyContent = new MapperObjectToAnyContent().getAnyContent(UUID_OUTPUT_NAME, UUID);
            processResponse.getOutput().add(anyContent);
            processResponse.getOutput().add(anyContent);
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} with an output not valid for a Patient Feed response.
     * @return a {@link ProcessResponse} with invalid output
     */
    private ProcessResponse getResponseWithInvalidOutput(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.SUCCESS);
        processResponse.setReport(report);
        PersonName personName = new PersonName();
        personName.setFamily("Bars");
        try {
            AnyContent anyContent = new MapperObjectToAnyContent().getAnyContent(PATIENT_INPUT_NAME, personName);
            processResponse.getOutput().add(anyContent);
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} with unexpected Report Status, id not SUCCESS or FAILURE.
     * @return a {@link ProcessResponse} with unexpected Report Status
     */
    private ProcessResponse getResponseWithUnexpectedReportStatus(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.WARNING);
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(UUID_OUTPUT_NAME, UUID));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} without Error reports in the TAR report.
     * @return a {@link ProcessResponse} without Error Reports
     */
    private ProcessResponse getResponseWithoutReports(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.FAILURE);
        report.setContext(new AnyContent());
        report.getContext().getItem().add(new AnyContent());
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(UUID_OUTPUT_NAME, UUID));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} without any Error thought with status FAILURE.
     * @return
     */
    private ProcessResponse getResponseWithoutErrorInFailureReport(){
        ProcessResponse processResponse = new ProcessResponse();

        TAR report = new TAR();
        report.setResult(TestResultType.FAILURE);
        report.setReports(new TestAssertionGroupReportsType());

        report.setCounters(new ValidationCounters());
        report.getCounters().setNrOfAssertions(BigInteger.valueOf(0));
        report.getCounters().setNrOfWarnings(BigInteger.valueOf(0));
        report.getCounters().setNrOfErrors(BigInteger.valueOf(0));

        report.setContext(new AnyContent());
        report.getContext().getItem().add(new AnyContent());
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(UUID_OUTPUT_NAME, UUID));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /*-----------------------------------getDelete methods-------------------------------------*/

    /**
     * Return a correct {@link ProcessResponse} to a feed Process Request
     * @return a correct {@link ProcessResponse}
     */
    private ProcessResponse getDoneDeleteProcessResponse(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.SUCCESS);
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(DELETE_STATUS_OUTPUT_NAME, DELETE_STATUS_DONE));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Return a correct {@link ProcessResponse} to a feed Process Request
     * @return a correct {@link ProcessResponse}
     */
    private ProcessResponse getGoneDeleteProcessResponse(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.SUCCESS);
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(DELETE_STATUS_OUTPUT_NAME, DELETE_STATUS_GONE));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Return a correct {@link ProcessResponse} to a feed Process Request
     * @return a correct {@link ProcessResponse}
     */
    private ProcessResponse getWrongDeleteProcessResponse(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.FAILURE);
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(DELETE_STATUS_OUTPUT_NAME, UUID));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Return a valid {@link ProcessResponse} for a failed feed request.
     * @param errorDescription   Value of the Error's description for the report
     * @return the failed {@link ProcessResponse}
     */
    private ProcessResponse getDeleteResponseWithFailedReport(String errorDescription){
        com.gitb.tr.ObjectFactory objectFactory = new ObjectFactory();
        ProcessResponse processResponse = new ProcessResponse();

        TAR report = new TAR();
        report.setResult(TestResultType.FAILURE);
        report.setReports(new TestAssertionGroupReportsType());

        BAR itemContent = new BAR();
        itemContent.setDescription(errorDescription);
        report.getReports().getInfoOrWarningOrError().add(objectFactory.createTestAssertionGroupReportsTypeError(itemContent));

        report.setCounters(new ValidationCounters());
        report.getCounters().setNrOfAssertions(BigInteger.valueOf(0));
        report.getCounters().setNrOfWarnings(BigInteger.valueOf(0));
        report.getCounters().setNrOfErrors(BigInteger.valueOf(1));

        report.setContext(new AnyContent());
        report.getContext().getItem().add(new AnyContent());
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(UUID_OUTPUT_NAME, UUID));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }


    /**
     * Return a {@link ProcessResponse} with no Report
     * @return a {@link ProcessResponse} with no report
     */
    private ProcessResponse getDeleteResponseMissingReport(){
        ProcessResponse processResponse = new ProcessResponse();
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(DELETE_STATUS_OUTPUT_NAME, DELETE_STATUS_DONE));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} with multiple outputs
     * @return a {@link ProcessResponse} with multiple outputs
     */
    private ProcessResponse getDeleteResponseWithMultipleOutputs(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.SUCCESS);
        processResponse.setReport(report);
        try {
            AnyContent anyContent = new MapperObjectToAnyContent().getAnyContent(DELETE_STATUS_OUTPUT_NAME, DELETE_STATUS_DONE);
            processResponse.getOutput().add(anyContent);
            processResponse.getOutput().add(anyContent);
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} with unexpected Report Status, id not SUCCESS or FAILURE.
     * @return a {@link ProcessResponse} with unexpected Report Status
     */
    private ProcessResponse getDeleteResponseWithUnexpectedReportStatus(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.WARNING);
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(DELETE_STATUS_OUTPUT_NAME, DELETE_STATUS_DONE));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} without Error reports in the TAR report.
     * @return a {@link ProcessResponse} without Error Reports
     */
    private ProcessResponse getDeleteResponseWithoutReports(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.FAILURE);
        report.setContext(new AnyContent());
        report.getContext().getItem().add(new AnyContent());
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(DELETE_STATUS_OUTPUT_NAME, DELETE_STATUS_DONE));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} without any Error thought with status FAILURE.
     * @return
     */
    private ProcessResponse getDeleteResponseWithoutErrorInFailureReport(){
        ProcessResponse processResponse = new ProcessResponse();

        TAR report = new TAR();
        report.setResult(TestResultType.FAILURE);
        report.setReports(new TestAssertionGroupReportsType());

        report.setCounters(new ValidationCounters());
        report.getCounters().setNrOfAssertions(BigInteger.valueOf(0));
        report.getCounters().setNrOfWarnings(BigInteger.valueOf(0));
        report.getCounters().setNrOfErrors(BigInteger.valueOf(0));

        report.setContext(new AnyContent());
        report.getContext().getItem().add(new AnyContent());
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(DELETE_STATUS_OUTPUT_NAME, DELETE_STATUS_DONE));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /*-----------------------------------getUpdate methods-------------------------------------*/

    /**
     * Return a correct {@link ProcessResponse} to a feed Process Request
     * @return a correct {@link ProcessResponse}
     */
    private ProcessResponse getUpdateCorrectProcessResponse(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.SUCCESS);
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(PATIENT_OUTPUT_NAME, PATIENT));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Return a {@link ProcessResponse} with no Report
     * @return a {@link ProcessResponse} with no report
     */
    private ProcessResponse getUpdateResponseMissingReport(){
        ProcessResponse processResponse = new ProcessResponse();
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(PATIENT_OUTPUT_NAME, PATIENT));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Return a valid {@link ProcessResponse} for a failed feed request.
     * @param errorDescription   Value of the Error's description for the report
     * @return the failed {@link ProcessResponse}
     */
    private ProcessResponse getUpdateResponseWithFailedReport(String errorDescription){
        com.gitb.tr.ObjectFactory objectFactory = new ObjectFactory();
        ProcessResponse processResponse = new ProcessResponse();

        TAR report = new TAR();
        report.setResult(TestResultType.FAILURE);
        report.setReports(new TestAssertionGroupReportsType());

        BAR itemContent = new BAR();
        itemContent.setDescription(errorDescription);
        report.getReports().getInfoOrWarningOrError().add(objectFactory.createTestAssertionGroupReportsTypeError(itemContent));

        report.setCounters(new ValidationCounters());
        report.getCounters().setNrOfAssertions(BigInteger.valueOf(0));
        report.getCounters().setNrOfWarnings(BigInteger.valueOf(0));
        report.getCounters().setNrOfErrors(BigInteger.valueOf(1));

        report.setContext(new AnyContent());
        report.getContext().getItem().add(new AnyContent());
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(PATIENT_OUTPUT_NAME, PATIENT));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} with multiple outputs
     * @return a {@link ProcessResponse} with multiple outputs
     */
    private ProcessResponse getUpdateResponseWithMultipleOutputs(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.SUCCESS);
        processResponse.setReport(report);
        try {
            AnyContent anyContent = new MapperObjectToAnyContent().getAnyContent(PATIENT_OUTPUT_NAME, PATIENT);
            processResponse.getOutput().add(anyContent);
            processResponse.getOutput().add(anyContent);
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} with unexpected Report Status, id not SUCCESS or FAILURE.
     * @return a {@link ProcessResponse} with unexpected Report Status
     */
    private ProcessResponse getUpdateResponseWithUnexpectedReportStatus(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.WARNING);
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(PATIENT_OUTPUT_NAME, PATIENT));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} without Error reports in the TAR report.
     * @return a {@link ProcessResponse} without Error Reports
     */
    private ProcessResponse getUpdateResponseWithoutReports(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.FAILURE);
        report.setContext(new AnyContent());
        report.getContext().getItem().add(new AnyContent());
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(PATIENT_OUTPUT_NAME, PATIENT));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} without any Error thought with status FAILURE.
     * @return
     */
    private ProcessResponse getUpdateResponseWithoutErrorInFailureReport(){
        ProcessResponse processResponse = new ProcessResponse();

        TAR report = new TAR();
        report.setResult(TestResultType.FAILURE);
        report.setReports(new TestAssertionGroupReportsType());

        report.setCounters(new ValidationCounters());
        report.getCounters().setNrOfAssertions(BigInteger.valueOf(0));
        report.getCounters().setNrOfWarnings(BigInteger.valueOf(0));
        report.getCounters().setNrOfErrors(BigInteger.valueOf(0));

        report.setContext(new AnyContent());
        report.getContext().getItem().add(new AnyContent());
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(PATIENT_OUTPUT_NAME, PATIENT));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /*-----------------------------------getMerge methods-------------------------------------*/

    /**
     * Return a correct {@link ProcessResponse} to a feed Process Request
     * @return a correct {@link ProcessResponse}
     */
    private ProcessResponse getMergeCorrectProcessResponse(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.SUCCESS);
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(UUID_OUTPUT_NAME, UUID));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Return a {@link ProcessResponse} with no Report
     * @return a {@link ProcessResponse} with no report
     */
    private ProcessResponse getMergeResponseMissingReport(){
        ProcessResponse processResponse = new ProcessResponse();
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(UUID_OUTPUT_NAME, UUID));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Return a valid {@link ProcessResponse} for a failed feed request.
     * @param errorDescription   Value of the Error's description for the report
     * @return the failed {@link ProcessResponse}
     */
    private ProcessResponse getMergeResponseWithFailedReport(String errorDescription){
        com.gitb.tr.ObjectFactory objectFactory = new ObjectFactory();
        ProcessResponse processResponse = new ProcessResponse();

        TAR report = new TAR();
        report.setResult(TestResultType.FAILURE);
        report.setReports(new TestAssertionGroupReportsType());

        BAR itemContent = new BAR();
        itemContent.setDescription(errorDescription);
        report.getReports().getInfoOrWarningOrError().add(objectFactory.createTestAssertionGroupReportsTypeError(itemContent));

        report.setCounters(new ValidationCounters());
        report.getCounters().setNrOfAssertions(BigInteger.valueOf(0));
        report.getCounters().setNrOfWarnings(BigInteger.valueOf(0));
        report.getCounters().setNrOfErrors(BigInteger.valueOf(1));

        report.setContext(new AnyContent());
        report.getContext().getItem().add(new AnyContent());
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(UUID_OUTPUT_NAME, UUID));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} with multiple outputs
     * @return a {@link ProcessResponse} with multiple outputs
     */
    private ProcessResponse getMergeResponseWithMultipleOutputs(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.SUCCESS);
        processResponse.setReport(report);
        try {
            AnyContent anyContent = new MapperObjectToAnyContent().getAnyContent(UUID_OUTPUT_NAME, UUID);
            processResponse.getOutput().add(anyContent);
            processResponse.getOutput().add(anyContent);
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }


    /**
     * Returns a {@link ProcessResponse} with unexpected Report Status, id not SUCCESS or FAILURE.
     * @return a {@link ProcessResponse} with unexpected Report Status
     */
    private ProcessResponse getMergeResponseWithUnexpectedReportStatus(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.WARNING);
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(UUID_OUTPUT_NAME, UUID));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} without Error reports in the TAR report.
     * @return a {@link ProcessResponse} without Error Reports
     */
    private ProcessResponse getMergeResponseWithoutReports(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.FAILURE);
        report.setContext(new AnyContent());
        report.getContext().getItem().add(new AnyContent());
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(UUID_OUTPUT_NAME, UUID));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} without any Error thought with status FAILURE.
     * @return
     */
    private ProcessResponse getMergeResponseWithoutErrorInFailureReport(){
        ProcessResponse processResponse = new ProcessResponse();

        TAR report = new TAR();
        report.setResult(TestResultType.FAILURE);
        report.setReports(new TestAssertionGroupReportsType());

        report.setCounters(new ValidationCounters());
        report.getCounters().setNrOfAssertions(BigInteger.valueOf(0));
        report.getCounters().setNrOfWarnings(BigInteger.valueOf(0));
        report.getCounters().setNrOfErrors(BigInteger.valueOf(0));

        report.setContext(new AnyContent());
        report.getContext().getItem().add(new AnyContent());
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(UUID_OUTPUT_NAME, UUID));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BeginTransactionResponse beginTransaction(BeginTransactionRequest beginTransactionRequest) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Void endTransaction(BasicRequest basicRequest) {
        return null;
    }

    /**
     * Enumeration of all types of response this fake service can return
     */
    public enum  ResponseTypes{
        CORRECT,
        MISSING_REPORT,
        FAILED_REPORT,
        MULTIPLE_OUTPUT,
        INVALID_OUTPUT,
        NULL_RESPONSE,
        UNEXPECTED_STATUS,
        NO_REPORTS_IN_REPORT,
        NO_ERROR_IN_FAILURE_REPORT,
        NO_ERROR_DESCRIPTION,
        ILLEGAL_ARGUMENT_EXCEPTION,
        UNSUPPORTED_OPERATION_EXCEPTION,
        DELETE_STATUS_DONE,
        DELETE_STATUS_GONE,
        DELETE_STATUS_WRONG,
        DELETE_FAILED_REPORT,
        DELETE_MISSING_REPORT,
        DELETE_MULTIPLE_OUTPUT,
        DELETE_UNEXPECTED_STATUS,
        DELETE_NO_REPORTS_IN_REPORT,
        DELETE_NO_ERROR_IN_FAILURE_REPORT,
        DELETE_NO_ERROR_DESCRIPTION,
        UPDATE_CORRECT,
        UPDATE_MISSING_REPORT,
        UPDATE_FAILED_REPORT,
        UPDATE_MULTIPLE_OUTPUT,
        UPDATE_UNEXPECTED_STATUS,
        UPDATE_NO_REPORTS_IN_REPORT,
        UPDATE_NO_ERROR_IN_FAILURE_REPORT,
        UPDATE_NO_ERROR_DESCRIPTION,
        MERGE_CORRECT,
        MERGE_MISSING_REPORT,
        MERGE_FAILED_REPORT,
        MERGE_MULTIPLE_OUTPUT,
        MERGE_UNEXPECTED_STATUS,
        MERGE_NO_REPORTS_IN_REPORT,
        MERGE_NO_ERROR_IN_FAILURE_REPORT,
        MERGE_NO_ERROR_DESCRIPTION;

    }
}
