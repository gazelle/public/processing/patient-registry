package net.ihe.gazelle.app.patientregistryfeedclient.adapter;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedException;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for {@link PatientFeedClient}
 */
@Feature("Patient Feed Client Java")
class PatientFeedClientTest {

    private PatientFeedClient patientFeedClient;
    private TestProcessingService testProcessingService;

    /**
     * Initialization of the tested {@link PatientFeedClient}.
     * It is configured to use the mocked remote service {@link TestProcessingService}
     */
    @BeforeEach
    void init() {
        testProcessingService = new TestProcessingService();
        patientFeedClient = new PatientFeedClient(testProcessingService);
    }

    /*------------------------------------CREATE METHOD TESTS---------------------------------------------------*/

    /**
     * Test Create method from client getting a successful response
     *
     * @throws PatientFeedException
     */
    @Test
    @Description("Test Create method from client getting a successful response")
    @Covers(requirements = {"PATREG-20", "PATREG-21", "PATREG-22", "PATREG-23", "PATREG-24"})
    void create_With_Success() throws PatientFeedException {
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");
        String uuid = patientFeedClient.createPatient(patient);
        assertEquals(TestProcessingService.UUID, uuid);
    }

    /**
     * Test Create method from client getting a response with no report
     */
    @Test
    @Description("Test Create method from client getting a response with no report")
    @Covers(requirements = "PATREG-114")
    void create_With_Success_missing_report() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.MISSING_REPORT);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.createPatient(patient));
        assertEquals("Response from the distant PatientFeedProcessingService shall contain a report with " +
                "the success status of the operation !", exception.getCause().getMessage());
    }

    /**
     * Test Create method from client getting a response with failed report
     */
    @Test
    @Description("Test Create method from client getting a response with Failed Report")
    @Covers(requirements = "PATREG-115")
    void create_With_Success_failed_report() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.FAILED_REPORT);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.createPatient(patient));
        assertEquals("MyError", exception.getMessage());
    }

    /**
     * Test Create method from client getting a response with multiple outputs
     */
    @Test
    @Description("Test Create method from client getting a response with multiple outputs")
    @Covers(requirements = "PATREG-116")
    void create_With_Success_multiple_output() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.MULTIPLE_OUTPUT);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.createPatient(patient));
        assertEquals("Response from the distant PatientFeedProcessingService shall contain a single " +
                "output for fed Patient UUID !", exception.getCause().getMessage());
    }

    /**
     * Test Create method from client getting a response with Invalid output
     */
    @Test
    @Description("Test Create method from client getting a response with Invalid output")
    @Covers(requirements = "PATREG-28")
    void create_With_Success_invalid_output() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.INVALID_OUTPUT);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.createPatient(patient));
        assertEquals("Error while mapping processing output from distant PatientFeedProcessingService " +
                "to String !", exception.getCause().getMessage());
    }

    /**
     * Test Create method from client getting a null response
     */
    @Test
    @Description("Test Create method from client getting a null response")
    @Covers(requirements = "PATREG-25")
    void create_null_response() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.NULL_RESPONSE);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.createPatient(patient));
        assertEquals("Empty Response from the distant PatientFeedProcessingService !", exception.getCause().getMessage());
    }

    /**
     * Test Create method from client getting a response with an unexpected status
     */
    @Test
    @Description("Test Create method from client getting a response with an unexpected status")
    void create_unexpected_processing_status() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.UNEXPECTED_STATUS);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.createPatient(patient));
        assertEquals("Processing response with unexpected type WARNING ! Expected type is " +
                "either SUCCESS or FAILURE.", exception.getCause().getMessage());
    }

    /**
     * Test Create method from client getting a response with no error report in report
     */
    @Test
    @Description("Test Create method from client getting a response with no error report in report")
    @Covers(requirements = "PATREG-26")
    void create_no_reports_in_report() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.NO_REPORTS_IN_REPORT);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.createPatient(patient));
        assertEquals("The report from the ProcessResponse shall not be null and shall contain a single error.", exception.getCause().getMessage());
    }

    /**
     * Test Create method from client getting a response with no error in a failure report
     */
    @Test
    @Description("Test Create method from client getting a response with no error in a failure report")
    @Covers(requirements = "PATREG-26")
    void create_no_error_in_failure_report() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.NO_ERROR_IN_FAILURE_REPORT);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.createPatient(patient));
        assertEquals("The report from the ProcessResponse shall not be null and shall contain a single error.", exception.getCause().getMessage());
    }

    /**
     * Test Create method from client getting a response with an error with no description
     */
    @Test
    @Description("Test Create method from client getting a response with an error with no description")
    @Covers(requirements = "PATREG-27")
    void create_no_error_description() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.NO_ERROR_DESCRIPTION);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.createPatient(patient));
        assertEquals("Error from ProcessResponse report must have a valid description !", exception.getCause().getMessage());
    }

    /**
     * Test Create method from client getting an IllegalArgumentException
     */
    @Test
    @Description("Test Create method from client getting an IllegalArgumentException")
    @Covers(requirements = "PATREG-29")
    void create_illegalArgumentException() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.ILLEGAL_ARGUMENT_EXCEPTION);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.createPatient(patient));
        assertEquals("Invalid Request sent to distant PatientFeedProcessingService !", exception.getMessage());
    }

    /**
     * Test Create method from client getting an UnsupportedOperationException
     */
    @Test
    @Description("Test Create Method getting an UnsupportedOperationException")
    @Covers(requirements = "PATREG-117")
    void create_unsupportedOperationException() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.UNSUPPORTED_OPERATION_EXCEPTION);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.createPatient(patient));
        assertEquals("Invalid operation used on distant PatientFeedProcessingService !", exception.getMessage());
    }

    /*------------------------------------DELETE METHOD TESTS---------------------------------------------------*/

    /**
     * Test Delete method from client getting successful response with DONE Status
     *
     * @throws PatientFeedException
     */
    @Test
    @Description("Test Delete method from client getting successful response with DONE Status")
    void delete_With_Done_Status() throws PatientFeedException {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.DELETE_STATUS_DONE);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");
        boolean isDeleted = patientFeedClient.deletePatient(patient.getUuid());
        assertTrue(isDeleted);
    }

    /**
     * Test Delete method from client getting successful response with GONE Status
     *
     * @throws PatientFeedException
     */
    @Test
    @Description("Test Delete method from client getting successful response with GONE Status")
    void delete_With_Gone_Status() throws PatientFeedException {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.DELETE_STATUS_GONE);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");
        boolean isDeleted = patientFeedClient.deletePatient(patient.getUuid());
        assertFalse(isDeleted);
    }

    /**
     * Test Delete method from client with WRONG status getting a PatientFeedException
     *
     */
    @Test
    @Description("Test Delete method from client with WRONG status getting a PatientFeedException")
    void delete_With_Wrong_Status() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.DELETE_STATUS_WRONG);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");
        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.deletePatient(patient.getUuid()));
        assertEquals("The report from the ProcessResponse shall not be null and shall contain a single error.", exception.getCause().getMessage());
    }


    /**
     * Test Delete method from client getting a response with failed report
     */
    @Test
    @Description("Test Delete method from client getting a response with failed report")
    void delete_With_Success_failed_report() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.DELETE_FAILED_REPORT);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.deletePatient(patient.getUuid()));
        assertEquals("MyError", exception.getMessage());
    }


    /**
     * Test Delete method from client getting a response with no report
     */
    @Test
    @Description("Test Delete method from client getting a response with no report")
    void delete_With_Success_missing_report() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.DELETE_MISSING_REPORT);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.deletePatient(patient.getUuid()));
        assertEquals("Response from the distant PatientFeedProcessingService shall contain a report with " +
                "the success status of the operation !", exception.getCause().getMessage());
    }

    /**
     * Test Delete method from client getting a response with multiple outputs
     */
    @Test
    @Description("Test Delete method from client getting a response with multiple output")
    void delete_With_Delete_multiple_output() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.DELETE_MULTIPLE_OUTPUT);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.deletePatient(patient.getUuid()));
        assertEquals("Response from the distant PatientFeedProcessingService shall contain a single " +
                "output for deleted Patient !", exception.getCause().getMessage());
    }

    /**
     * Test Delete method from client getting a response with invalid output
     */
    @Test
    @Description("Test Delete method from client getting a response with invalid output")
    void delete_With_Success_invalid_output() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.INVALID_OUTPUT);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.deletePatient(patient.getUuid()));
        assertEquals("Error while mapping processing output from distant PatientFeedProcessingService " +
                "to String !", exception.getCause().getMessage());
    }

    /**
     * Test Delete method from client getting a null response
     */
    @Test
    @Description("Test Delete method from client getting null response")
    void delete_null_response() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.NULL_RESPONSE);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.deletePatient(patient.getUuid()));
        assertEquals("Empty Response from the distant PatientFeedProcessingService !", exception.getCause().getMessage());
    }

    /**
     * Test Delete method from client getting a response with an unexpected status
     */
    @Test
    @Description("Test Delete method from client getting a response with an unexpected status")
    void delete_unexpected_processing_status() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.DELETE_UNEXPECTED_STATUS);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.deletePatient(patient.getUuid()));
        assertEquals("Processing response with unexpected type WARNING ! Expected type is " +
                "either SUCCESS or FAILURE.", exception.getCause().getMessage());
    }

    /**
     * Test Delete  method from client getting a response with no error report in report
     */
    @Test
    @Description("Test Delete  method from client  getting a response with no error report in report")
    void delete_no_reports_in_report() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.DELETE_NO_REPORTS_IN_REPORT);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.deletePatient(patient.getUuid()));
        assertEquals("The report from the ProcessResponse shall not be null and shall contain a single error.", exception.getCause().getMessage());
    }

    /**
     * Test Delete  method from client getting a response with no error in a failure report
     */
    @Test
    @Description("Test Delete  method from client getting a response with no error in a failure report")
    void delete_no_error_in_failure_report() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.DELETE_NO_ERROR_IN_FAILURE_REPORT);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.deletePatient(patient.getUuid()));
        assertEquals("The report from the ProcessResponse shall not be null and shall contain a single error.", exception.getCause().getMessage());
    }

    /**
     * Test Delete method from client getting a response with an error with no description
     */
    @Test
    @Description("Test Delete method from client  with an error with no description")
    void delete_no_error_description() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.DELETE_NO_ERROR_DESCRIPTION);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.deletePatient(patient.getUuid()));
        assertEquals("Error from ProcessResponse report must have a valid description !", exception.getCause().getMessage());
    }

    /**
     * Test Delete  method from client getting an IllegalArgumentException
     */
    @Test
    @Description("Test Delete  method from client getting an IllegalArgumentException")
    void delete_illegalArgumentException() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.ILLEGAL_ARGUMENT_EXCEPTION);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.deletePatient(patient.getUuid()));
        assertEquals("Invalid Request sent to distant PatientFeedProcessingService !", exception.getMessage());
    }

    /**
     * Test Delete method from client getting an UnsupportedOperationException
     */
    @Test
    @Description("Test Delete  method from client  getting an UnsupportedOperationException")
    void delete_unsupportedOperationException() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.UNSUPPORTED_OPERATION_EXCEPTION);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.deletePatient(patient.getUuid()));
        assertEquals("Invalid operation used on distant PatientFeedProcessingService !", exception.getMessage());
    }

    /*------------------------------------UPDATE METHOD TESTS---------------------------------------------------*/

    /**
     * Test Update method from client getting successful response
     *
     * @throws PatientFeedException
     */
    @Test
    @Description("Test Update method from client getting successful response")
    void update_With_Success() throws PatientFeedException {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.UPDATE_CORRECT);
        Patient patient = new Patient();
        Patient patientToReturn = patientFeedClient.updatePatient(patient, new EntityIdentifier());
        assertEquals(patient, patientToReturn);
    }

    /**
     * Test Update method from client getting a response with no report
     */
    @Test
    @Description("Test Update method from client getting a response with no report")
    void update_With_Success_missing_report() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.MISSING_REPORT);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.updatePatient(patient, new EntityIdentifier()));
        assertEquals("Response from the distant PatientFeedProcessingService shall contain a report with " +
                "the success status of the operation !", exception.getCause().getMessage());
    }

    /**
     * Test Update method from client getting a response with failed report
     */
    @Test
    @Description("Test Update method from client getting a response with failed report")
    void update_With_Success_failed_report() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.FAILED_REPORT);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.updatePatient(patient,new EntityIdentifier()));
        assertEquals("MyError", exception.getMessage());
    }

    /**
     * Test Update method from client getting a response with multiple outputs
     */
    @Test
    @Description("Test Update method from client getting a response with multiple outputs")
    void update_With_Success_multiple_output() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.MULTIPLE_OUTPUT);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.updatePatient(patient, new EntityIdentifier()));
        assertEquals("Response from the distant PatientFeedProcessingService shall contain a single output for updated Patient !", exception.getCause().getMessage());
    }

    /**
     * Test Update method from client getting a response with invalid output (not a String)
     */
    @Test
    @Description("Test Update method from client getting a response with invalid output")
    void update_With_Success_invalid_output() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.INVALID_OUTPUT);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.updatePatient(patient, new EntityIdentifier()));
        assertEquals("Error while mapping processing output from distant PatientFeedProcessingService " +
                "to Patient !", exception.getCause().getMessage());
    }

    /**
     * Test Update method from client getting a null response
     */
    @Test
    @Description("Test Update method from client getting a null response")
    void update_null_response() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.NULL_RESPONSE);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.updatePatient(patient, new EntityIdentifier()));
        assertEquals("Empty Response from the distant PatientFeedProcessingService !", exception.getCause().getMessage());
    }

    /**
     * Test Update method from client getting a response with an unexpected status
     */
    @Test
    @Description("Test Update method from client getting a response with an unexcepted status")
    void update_unexpected_processing_status() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.UPDATE_UNEXPECTED_STATUS);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.updatePatient(patient, new EntityIdentifier()));
        assertEquals("Processing response with unexpected type WARNING ! Expected type is " +
                "either SUCCESS or FAILURE.", exception.getCause().getMessage());
    }

    /**
     * Test Update method from client getting a response with no error report in report
     */
    @Test
    @Description("Test Update method from client getting a response with no error report in report")
    void update_no_reports_in_report() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.UPDATE_NO_REPORTS_IN_REPORT);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.updatePatient(patient, new EntityIdentifier()));
        assertEquals("The report from the ProcessResponse shall not be null and shall contain a single error.", exception.getCause().getMessage());
    }

    /**
     * Test Update method from client getting a response with no error in a failure report
     */
    @Test
    @Description("Test Update method from client getting a response with no error in a failure report")
    void update_no_error_in_failure_report() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.UPDATE_NO_ERROR_IN_FAILURE_REPORT);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.updatePatient(patient, new EntityIdentifier()));
        assertEquals("The report from the ProcessResponse shall not be null and shall contain a single error.", exception.getCause().getMessage());
    }

    /**
     * Test Update method from client getting a response with an error with no description
     */
    @Test
    @Description("Test Update method from client getting a response with an error with no description")
    void update_no_error_description() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.UPDATE_NO_ERROR_DESCRIPTION);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.updatePatient(patient, new EntityIdentifier()));
        assertEquals("Error from ProcessResponse report must have a valid description !", exception.getCause().getMessage());
    }

    /**
     * Test Update method from client getting an IllegalArgumentException
     */
    @Test
    @Description("Test Update method from client getiing an IllegalArgumentException")
    void update_illegalArgumentException() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.ILLEGAL_ARGUMENT_EXCEPTION);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.updatePatient(patient, new EntityIdentifier()));
        assertEquals("Invalid Request sent to distant PatientFeedProcessingService !", exception.getMessage());
    }

    /**
     * Test update method from client getting an UnsupportedOperationException
     */
    @Test
    @Description("Test update method from client getting an UnsupportedOperationException")
    void update_unsupportedOperationException() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.UNSUPPORTED_OPERATION_EXCEPTION);
        Patient patient = new Patient();
        patient.setUuid("TEST-uuid1");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.updatePatient(patient, new EntityIdentifier()));
        assertEquals("Invalid operation used on distant PatientFeedProcessingService !", exception.getMessage());
    }

    /*------------------------------------MERGE CLIENT METHOD TESTS---------------------------------------------------*/

    /**
     * Test Merge method from client getting successful response
     *
     * @throws PatientFeedException
     */
    @Test
    @Description("Test Merge method from client getting successful response")
    void merge_With_Success() throws PatientFeedException {
        Patient patientOriginal = new Patient();
        patientOriginal.setUuid("TEST-uuid1");
        Patient patientDuplicated = new Patient();
        patientDuplicated.setUuid("TEST-uuid2");
        String uuid = patientFeedClient.mergePatient(patientOriginal.getUuid(), patientDuplicated.getUuid());
        assertEquals(TestProcessingService.UUID, uuid);
    }

    /**
     * Test Merge method from client getting a response with no report
     */
    @Test
    @Description("Test Merge method from client getting a response with no report")
    void merge_With_Success_missing_report() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.MERGE_MISSING_REPORT);
        Patient patientOriginal = new Patient();
        patientOriginal.setUuid("TEST-uuid1");
        Patient patientDuplicated = new Patient();
        patientDuplicated.setUuid("TEST-uuid2");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.mergePatient(patientOriginal.getUuid(), patientDuplicated.getUuid()));
        assertEquals("Response from the distant PatientFeedProcessingService shall contain a report with " +
                "the success status of the operation !", exception.getCause().getMessage());
    }

    /**
     * Test Merge method from client getting a response with failed report
     */
    @Test
    @Description("Test Merge method from client getting a response with failed report")
    void merge_With_Success_failed_report() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.DELETE_FAILED_REPORT);
        Patient patientOriginal = new Patient();
        patientOriginal.setUuid("TEST-uuid1");
        Patient patientDuplicated = new Patient();
        patientDuplicated.setUuid("TEST-uuid2");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.mergePatient(patientOriginal.getUuid(), patientDuplicated.getUuid()));
        assertEquals("MyError", exception.getMessage());
    }

    /**
     * Test Merge method from client getting a response with multiple outputs
     */
    @Test
    @Description("Test Merge method from client getting a response with multiple outputs")
    void merge_With_Success_multiple_output() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.MERGE_MULTIPLE_OUTPUT);
        Patient patientOriginal = new Patient();
        patientOriginal.setUuid("TEST-uuid1");
        Patient patientDuplicated = new Patient();
        patientDuplicated.setUuid("TEST-uuid2");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.mergePatient(patientOriginal.getUuid(), patientDuplicated.getUuid()));
        assertEquals("Response from the distant PatientFeedProcessingService shall contain a single " +
                "output for fed Patient UUID !", exception.getCause().getMessage());
    }

    /**
     * Test Merge method from client getting a response with invalid output
     */
    @Test
    @Description("Test Merge method from client getting a response with invalid output")
    void merge_With_Success_invalid_output() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.INVALID_OUTPUT);
        Patient patientOriginal = new Patient();
        patientOriginal.setUuid("TEST-uuid1");
        Patient patientDuplicated = new Patient();
        patientDuplicated.setUuid("TEST-uuid2");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.mergePatient(patientOriginal.getUuid(), patientDuplicated.getUuid()));
        assertEquals("Error while mapping processing output from distant PatientFeedProcessingService " +
                "to String !", exception.getCause().getMessage());
    }

    /**
     * Test Merge method from client getting a null response
     */
    @Test
    @Description("Test Merge method from client getting a null response")
    void merge_null_response() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.NULL_RESPONSE);
        Patient patientOriginal = new Patient();
        patientOriginal.setUuid("TEST-uuid1");
        Patient patientDuplicated = new Patient();
        patientDuplicated.setUuid("TEST-uuid2");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.mergePatient(patientOriginal.getUuid(), patientDuplicated.getUuid()));
        assertEquals("Empty Response from the distant PatientFeedProcessingService !", exception.getCause().getMessage());
    }

    /**
     * Test Merge method from client getting a response with an unexpected status
     */
    @Test
    @Description("Test Merge method from client getting a response with an unexpected status")
    void merge_unexpected_processing_status() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.MERGE_UNEXPECTED_STATUS);
        Patient patientOriginal = new Patient();
        patientOriginal.setUuid("TEST-uuid1");
        Patient patientDuplicated = new Patient();
        patientDuplicated.setUuid("TEST-uuid2");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.mergePatient(patientOriginal.getUuid(), patientDuplicated.getUuid()));
        assertEquals("Processing response with unexpected type WARNING ! Expected type is " +
                "either SUCCESS or FAILURE.", exception.getCause().getMessage());
    }

    /**
     * Test Merge method from client getting a response with no error report in report
     */
    @Test
    @Description("Test Merge method from client getting a response with no error report in report")
    void merge_no_reports_in_report() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.MERGE_NO_REPORTS_IN_REPORT);
        Patient patientOriginal = new Patient();
        patientOriginal.setUuid("TEST-uuid1");
        Patient patientDuplicated = new Patient();
        patientDuplicated.setUuid("TEST-uuid2");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.mergePatient(patientOriginal.getUuid(), patientDuplicated.getUuid()));
        assertEquals("The report from the ProcessResponse shall not be null and shall contain a single error.", exception.getCause().getMessage());
    }

    /**
     * Test Merge method from client getting a response with no error in a failure report
     */
    @Test
    @Description("Test Merge method from client getting a response with no error in a failure report")
    void merge_no_error_in_failure_report() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.MERGE_NO_ERROR_IN_FAILURE_REPORT);
        Patient patientOriginal = new Patient();
        patientOriginal.setUuid("TEST-uuid1");
        Patient patientDuplicated = new Patient();
        patientDuplicated.setUuid("TEST-uuid2");


        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.mergePatient(patientOriginal.getUuid(), patientDuplicated.getUuid()));
        assertEquals("The report from the ProcessResponse shall not be null and shall contain a single error.", exception.getCause().getMessage());
    }

    /**
     * Test merge method from client getting a response with an error with no description
     */
    @Test
    @Description("Test Merge method from client getting a response with an error with no description")
    void merge_no_error_description() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.MERGE_NO_ERROR_DESCRIPTION);
        Patient patientOriginal = new Patient();
        patientOriginal.setUuid("TEST-uuid1");
        Patient patientDuplicated = new Patient();
        patientDuplicated.setUuid("TEST-uuid2");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.mergePatient(patientOriginal.getUuid(), patientDuplicated.getUuid()));
        assertEquals("Error from ProcessResponse report must have a valid description !", exception.getCause().getMessage());
    }

    /**
     * Test Merge method from client getting an IllegalArgumentException
     */
    @Test
    @Description("Test Merge method from client getting an IllegalArgumentException")
    void merge_illegalArgumentException() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.ILLEGAL_ARGUMENT_EXCEPTION);
        Patient patientOriginal = new Patient();
        patientOriginal.setUuid("TEST-uuid1");
        Patient patientDuplicated = new Patient();
        patientDuplicated.setUuid("TEST-uuid2");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.mergePatient(patientOriginal.getUuid(), patientDuplicated.getUuid()));
        assertEquals("Invalid Request sent to distant PatientFeedProcessingService !", exception.getMessage());
    }

    /**
     * Test Merge method from client getting an UnsupportedOperationException
     */
    @Test
    @Description("Test Merge method from client getting an UnsupportedOperationException")
    void merge_unsupportedOperationException() {
        testProcessingService.setResponseToReturn(TestProcessingService.ResponseTypes.UNSUPPORTED_OPERATION_EXCEPTION);
        Patient patientOriginal = new Patient();
        patientOriginal.setUuid("TEST-uuid1");
        Patient patientDuplicated = new Patient();
        patientDuplicated.setUuid("TEST-uuid2");

        PatientFeedException exception = assertThrows(PatientFeedException.class, () -> patientFeedClient.mergePatient(patientOriginal.getUuid(), patientDuplicated.getUuid()));
        assertEquals("Invalid operation used on distant PatientFeedProcessingService !", exception.getMessage());
    }

}
