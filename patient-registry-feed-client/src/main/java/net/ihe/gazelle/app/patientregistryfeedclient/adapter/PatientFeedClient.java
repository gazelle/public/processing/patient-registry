package net.ihe.gazelle.app.patientregistryfeedclient.adapter;

import com.gitb.core.AnyContent;
import com.gitb.ps.ProcessRequest;
import com.gitb.ps.ProcessResponse;
import com.gitb.ps.ProcessingService;
import com.gitb.tr.BAR;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import net.ihe.gazelle.app.patientregistryapi.adapter.ws.PatientProcessingWebserviceConstants;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedException;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedService;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.lib.gitbprocessingclient.adapter.GITBClientProcessImpl;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperAnyContentToObject;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperObjectToAnyContent;
import net.ihe.gazelle.lib.gitbutils.adapter.MappingException;

import java.net.URL;
import java.util.List;

import static net.ihe.gazelle.app.patientregistryapi.adapter.ws.PatientProcessingWebserviceConstants.*;

/**
 * PatientFeed Client to be used by any application that wants to feed a patient to a remote Patient Registry.
 * Given an url, this client will be able to feed the remote registry using GITB Process Service while exposing the
 * PatientFeedService interface.
 *
 * @author wbars
 */
public class PatientFeedClient implements PatientFeedService {

    public static final String EXCEPTION_WHILE_MAPPING_WITH_GITB_ELEMENTS = "Exception while Mapping with GITB elements !";
    public static final String PATIENT_FEED_PROCESSING_SERVICE = "PatientFeedProcessingService !";
    public static final String INVALID_RESPONSE_FROM_DISTANT_PATIENT_FEED_PROCESSING_SERVICE = "Invalid Response from distant " +
            PATIENT_FEED_PROCESSING_SERVICE;
    public static final String INVALID_OPERATION_USED_ON_DISTANT_PATIENT_FEED_PROCESSING_SERVICE = "Invalid operation used on distant " +
            PATIENT_FEED_PROCESSING_SERVICE;
    public static final String INVALID_REQUEST_SENT_TO_DISTANT_PATIENT_FEED_PROCESSING_SERVICE = "Invalid Request sent to distant " +
            PATIENT_FEED_PROCESSING_SERVICE;
    public static final String EMPTY_RESPONSE_FROM_THE_DISTANT_PATIENT_FEED_PROCESSING_SERVICE = "Empty Response from the distant " +
            PATIENT_FEED_PROCESSING_SERVICE;
    public static final String INVALID_RESPONSE = "Response from the distant PatientFeedProcessingService shall contain a report with " +
            "the success status of the operation !";
    public static final String PROCESSING_RESPONSE_WITH_UNEXPECTED_TYPE_S_EXPECTED_TYPE_IS_EITHER = "Processing response with unexpected type %s ! Expected type is either %s or %s.";
    public static final String ERROR_WHILE_MAPPING_PROCESSING_OUTPUT_FROM_DISTANT_PATIENT_FEED_PROCESSING_SERVICE = "Error while mapping processing output from distant PatientFeedProcessingService ";
    public static final String RESPONSE_FROM_THE_DISTANT_PATIENT_FEED_PROCESSING_SERVICE_SHALL_CONTAIN_A_SINGLE = "Response from the distant PatientFeedProcessingService shall contain a single ";
    private ProcessingService processingClient;

    /**
     * Constructor used for test purposes.
     *
     * @param processingService : processing service to be used by the PatientFeedClient.
     */
    public PatientFeedClient(ProcessingService processingService) {
        this.processingClient = processingService;
    }

    /**
     * Default constructor for the class. The Client will create the GITB Processing Service client based on the URL.
     *
     * @param processingServiceURL : URL of the remote Processing Service.
     */
    public PatientFeedClient(URL processingServiceURL) {
        this.processingClient = new GITBClientProcessImpl(processingServiceURL, PATIENT_PROCESSING_SERVICE, PATIENT_PROCESSING_SERVICE_PORT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String createPatient(Patient patient) throws PatientFeedException {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_CREATE_OPERATION);
        AnyContent patientToFeedAnyContent = null;
        try {
            patientToFeedAnyContent = new MapperObjectToAnyContent().getAnyContent(PATIENT_INPUT_NAME, patient);
            processRequest.getInput().add(patientToFeedAnyContent);
            ProcessResponse processResponse = processingClient.process(processRequest);
            return extractUUIDFromProcessResponse(processResponse);
        } catch (MappingException e) {
            throw new PatientFeedException(EXCEPTION_WHILE_MAPPING_WITH_GITB_ELEMENTS, e);
        } catch (PatientFeedProcessResponseException e) {
            throw new PatientFeedException(INVALID_RESPONSE_FROM_DISTANT_PATIENT_FEED_PROCESSING_SERVICE, e);
        } catch (UnsupportedOperationException e) {
            throw new PatientFeedException(INVALID_OPERATION_USED_ON_DISTANT_PATIENT_FEED_PROCESSING_SERVICE, e);
        } catch (IllegalArgumentException e) {
            throw new PatientFeedException(INVALID_REQUEST_SENT_TO_DISTANT_PATIENT_FEED_PROCESSING_SERVICE, e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Patient updatePatient(Patient patient, EntityIdentifier identifier) throws PatientFeedException {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_UPDATE_OPERATION);
        AnyContent patientToFeedAnyContent = null;
        try {
            patientToFeedAnyContent = new MapperObjectToAnyContent().getAnyContent(PATIENT_INPUT_NAME, patient);
            AnyContent patientIdentifier = new MapperObjectToAnyContent().getAnyContent(PATIENT_IDENTIFIER_INPUT_NAME, identifier);
            processRequest.getInput().add(patientToFeedAnyContent);
            processRequest.getInput().add(patientIdentifier);
            ProcessResponse processResponse = processingClient.process(processRequest);
            return extractPatientFromProcessResponse(processResponse);
        } catch (MappingException e) {
            throw new PatientFeedException(EXCEPTION_WHILE_MAPPING_WITH_GITB_ELEMENTS, e);
        } catch (PatientFeedProcessResponseException e) {
            throw new PatientFeedException(INVALID_RESPONSE_FROM_DISTANT_PATIENT_FEED_PROCESSING_SERVICE, e);
        } catch (UnsupportedOperationException e) {
            throw new PatientFeedException(INVALID_OPERATION_USED_ON_DISTANT_PATIENT_FEED_PROCESSING_SERVICE, e);
        } catch (IllegalArgumentException e) {
            throw new PatientFeedException(INVALID_REQUEST_SENT_TO_DISTANT_PATIENT_FEED_PROCESSING_SERVICE, e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String mergePatient(String uuidOfOriginalPatient, String uuidOfDuplicatedPatient) throws PatientFeedException {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_MERGE_OPERATION);
        AnyContent patientUUIDDuplicated = null;
        AnyContent patientUUIDOriginal = null;
        try {
            patientUUIDDuplicated = new MapperObjectToAnyContent().getAnyContent(UUID_OF_DUPLICATED_PATIENT_INPUT_NAME, uuidOfDuplicatedPatient);
            patientUUIDOriginal = new MapperObjectToAnyContent().getAnyContent(UUID_OF_ORIGINAL_PATIENT_INPUT_NAME, uuidOfOriginalPatient);
            processRequest.getInput().add(patientUUIDOriginal);
            processRequest.getInput().add(patientUUIDDuplicated);
            ProcessResponse processResponse = processingClient.process(processRequest);
            return extractUUIDFromProcessResponse(processResponse);
        } catch (MappingException e) {
            throw new PatientFeedException(EXCEPTION_WHILE_MAPPING_WITH_GITB_ELEMENTS, e);
        } catch (PatientFeedProcessResponseException e) {
            throw new PatientFeedException(INVALID_RESPONSE_FROM_DISTANT_PATIENT_FEED_PROCESSING_SERVICE, e);
        } catch (UnsupportedOperationException e) {
            throw new PatientFeedException(INVALID_OPERATION_USED_ON_DISTANT_PATIENT_FEED_PROCESSING_SERVICE, e);
        } catch (IllegalArgumentException e) {
            throw new PatientFeedException(INVALID_REQUEST_SENT_TO_DISTANT_PATIENT_FEED_PROCESSING_SERVICE, e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean deletePatient(String uuid) throws PatientFeedException {
        ProcessResponse processResponse = deletePatient(UUID_PATIENT_TO_DELETE_INPUT_NAME, uuid);
        try {
            return extractDeletionResultFromProcessResponse(processResponse);
        } catch (PatientFeedProcessResponseException e) {
            throw new PatientFeedException(e);
        }
    }

    @Override
    public EntityIdentifier deletePatient(EntityIdentifier identifier) throws PatientFeedException {
        ProcessResponse processResponse = deletePatient(IDENTIFIER_PATIENT_TO_DELETE_INPUT_NAME, identifier);
        try {
            return extractPatientIdentifierFromResponse(processResponse.getOutput());
        } catch (PatientFeedProcessResponseException e) {
            throw new PatientFeedException(e);
        }
    }


    private ProcessResponse deletePatient(String fieldName, Object value) throws PatientFeedException {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PATIENT_IDENTIFIER_DELETE_OPERATION);
        AnyContent patientToFeedAnyContent = null;

        try {
            patientToFeedAnyContent = new MapperObjectToAnyContent().getAnyContent(fieldName, value);
            processRequest.getInput().add(patientToFeedAnyContent);
            return processingClient.process(processRequest);
        } catch (MappingException e) {
            throw new PatientFeedException(EXCEPTION_WHILE_MAPPING_WITH_GITB_ELEMENTS, e);
        } catch (UnsupportedOperationException e) {
            throw new PatientFeedException(INVALID_OPERATION_USED_ON_DISTANT_PATIENT_FEED_PROCESSING_SERVICE, e);
        } catch (IllegalArgumentException e) {
            throw new PatientFeedException(INVALID_REQUEST_SENT_TO_DISTANT_PATIENT_FEED_PROCESSING_SERVICE, e);
        }
    }

    /**
     * Extract the UUID from the ProcessResponse received from the remote ProcessingService.
     *
     * @param processResponse : response received.
     * @return literal value of the UUID assigned to the fed patient by the remote Repository.
     * @throws PatientFeedProcessResponseException : if the ProcessResponse is not valid for a PatientFeed request.
     * @throws PatientFeedException                : if the Patient Feed raised an exception in the remote Repository.
     */
    private String extractUUIDFromProcessResponse(ProcessResponse processResponse) throws PatientFeedProcessResponseException, PatientFeedException {
        if (processResponse == null) {
            throw new PatientFeedProcessResponseException(EMPTY_RESPONSE_FROM_THE_DISTANT_PATIENT_FEED_PROCESSING_SERVICE);
        } else {
            if (processResponse.getReport() != null) {
                if (TestResultType.SUCCESS.equals(processResponse.getReport().getResult())) {
                    return extractUUIDFromOutputs(processResponse.getOutput());
                } else if (TestResultType.FAILURE.equals(processResponse.getReport().getResult())) {
                    throw createExceptionFromProcessResponseReport(processResponse.getReport());
                } else {
                    throw new PatientFeedProcessResponseException(String.format(PROCESSING_RESPONSE_WITH_UNEXPECTED_TYPE_S_EXPECTED_TYPE_IS_EITHER, processResponse.getReport().getResult(), TestResultType.SUCCESS, TestResultType.FAILURE));
                }
            } else {
                throw new PatientFeedProcessResponseException(INVALID_RESPONSE);
            }
        }
    }

    /**
     * Extract the UUID from the ProcessResponse received from the remote ProcessingService.
     *
     * @param processResponse : response received.
     * @return values of the updated Patient by the remote Repository.
     * @throws PatientFeedProcessResponseException : if the ProcessResponse is not valid for a PatientFeed request.
     * @throws PatientFeedException                : if the Patient Feed raised an exception in the remote Repository.
     */
    private Patient extractPatientFromProcessResponse(ProcessResponse processResponse) throws PatientFeedProcessResponseException,
            PatientFeedException {
        if (processResponse == null) {
            throw new PatientFeedProcessResponseException(EMPTY_RESPONSE_FROM_THE_DISTANT_PATIENT_FEED_PROCESSING_SERVICE);
        } else {
            if (processResponse.getReport() != null) {
                if (TestResultType.SUCCESS.equals(processResponse.getReport().getResult())) {
                    return extractPatientFromOutputs(processResponse.getOutput());
                } else if (TestResultType.FAILURE.equals(processResponse.getReport().getResult())) {
                    throw createExceptionFromProcessResponseReport(processResponse.getReport());
                } else {
                    throw new PatientFeedProcessResponseException(String.format(PROCESSING_RESPONSE_WITH_UNEXPECTED_TYPE_S_EXPECTED_TYPE_IS_EITHER, processResponse.getReport().getResult(), TestResultType.SUCCESS, TestResultType.FAILURE));
                }
            } else {
                throw new PatientFeedProcessResponseException(INVALID_RESPONSE);
            }
        }
    }

    /**
     * Extract the UUID from the ProcessResponse received from the remote ProcessingService.
     *
     * @param processResponse : response received.
     * @return values of the updated Patient by the remote Repository.
     * @throws PatientFeedProcessResponseException : if the ProcessResponse is not valid for a PatientFeed request.
     * @throws PatientFeedException                : if the Patient Feed raised an exception in the remote Repository.
     */
    private boolean extractDeletionResultFromProcessResponse(ProcessResponse processResponse) throws PatientFeedProcessResponseException,
            PatientFeedException {
        if (processResponse == null) {
            throw new PatientFeedProcessResponseException(EMPTY_RESPONSE_FROM_THE_DISTANT_PATIENT_FEED_PROCESSING_SERVICE);
        } else {
            if (processResponse.getReport() != null) {
                if (TestResultType.SUCCESS.equals(processResponse.getReport().getResult())) {
                    return extractDeleteStatusFromOutputs(processResponse.getOutput());
                } else if (TestResultType.FAILURE.equals(processResponse.getReport().getResult())) {
                    throw createExceptionFromProcessResponseReport(processResponse.getReport());
                } else {
                    throw new PatientFeedProcessResponseException(String.format(PROCESSING_RESPONSE_WITH_UNEXPECTED_TYPE_S_EXPECTED_TYPE_IS_EITHER, processResponse.getReport().getResult(), TestResultType.SUCCESS, TestResultType.FAILURE));
                }
            } else {
                throw new PatientFeedProcessResponseException(INVALID_RESPONSE);
            }
        }
    }

    /**
     * Extract the UUID from the ProcessResponse.output received from the remote ProcessingService.
     *
     * @param outputs : list of received outputs.
     * @return literal value of the UUID assigned to the fed patient by the remote Repository.
     * @throws PatientFeedProcessResponseException : if the ProcessResponse.output is not valid for a PatientFeed request.
     */
    private String extractUUIDFromOutputs(List<AnyContent> outputs) throws PatientFeedProcessResponseException {
        if (outputs.size() == 1) {
            try {
                return new MapperAnyContentToObject().getObject(outputs.get(0), String.class);
            } catch (MappingException e) {
                throw new PatientFeedProcessResponseException(ERROR_WHILE_MAPPING_PROCESSING_OUTPUT_FROM_DISTANT_PATIENT_FEED_PROCESSING_SERVICE +
                        "to String !");
            }
        } else {
            throw new PatientFeedProcessResponseException(RESPONSE_FROM_THE_DISTANT_PATIENT_FEED_PROCESSING_SERVICE_SHALL_CONTAIN_A_SINGLE +
                    "output for fed Patient UUID !");
        }
    }

    /**
     * Extract the Patient from the ProcessResponse.Output received from the remote ProcessingService.
     *
     * @param outputs : list of received outputs.
     * @return literal value of the Patient assigned to the fed patient by the remote Repository.
     * @throws PatientFeedProcessResponseException : if the ProcessResponse.Output is not valid for a PatientFeed request.
     */
    private Patient extractPatientFromOutputs(List<AnyContent> outputs) throws PatientFeedProcessResponseException {
        if (outputs.size() == 1) {
            try {
                return new MapperAnyContentToObject().getObject(outputs.get(0), Patient.class);
            } catch (MappingException e) {
                throw new PatientFeedProcessResponseException(ERROR_WHILE_MAPPING_PROCESSING_OUTPUT_FROM_DISTANT_PATIENT_FEED_PROCESSING_SERVICE +
                        "to Patient !");
            }
        } else {
            throw new PatientFeedProcessResponseException(RESPONSE_FROM_THE_DISTANT_PATIENT_FEED_PROCESSING_SERVICE_SHALL_CONTAIN_A_SINGLE +
                    "output for updated Patient !");
        }
    }

    /**
     * Extract the Patient from the ProcessResponse.Output received from the remote ProcessingService.
     *
     * @param outputs : list of received outputs.
     * @return literal value of the Patient assigned to the fed patient by the remote Repository.
     * @throws PatientFeedProcessResponseException : if the ProcessResponse.Output is not valid for a PatientFeed request.
     */
    private boolean extractDeleteStatusFromOutputs(List<AnyContent> outputs) throws PatientFeedProcessResponseException {
        if (outputs.size() == 1) {
            try {
                String result = new MapperAnyContentToObject().getObject(outputs.get(0), String.class);
                return result.equals(DELETE_STATUS_DONE);
            } catch (MappingException e) {
                throw new PatientFeedProcessResponseException(ERROR_WHILE_MAPPING_PROCESSING_OUTPUT_FROM_DISTANT_PATIENT_FEED_PROCESSING_SERVICE +
                        "to String !");
            }
        } else {
            throw new PatientFeedProcessResponseException(RESPONSE_FROM_THE_DISTANT_PATIENT_FEED_PROCESSING_SERVICE_SHALL_CONTAIN_A_SINGLE +
                    "output for deleted Patient !");
        }
    }

    private EntityIdentifier extractPatientIdentifierFromResponse(List<AnyContent> outputs) throws PatientFeedProcessResponseException {
        if (outputs.size() == 1) {
            try {
                return new MapperAnyContentToObject().getObject(outputs.get(0), EntityIdentifier.class);
            } catch (MappingException e) {
                throw new PatientFeedProcessResponseException(ERROR_WHILE_MAPPING_PROCESSING_OUTPUT_FROM_DISTANT_PATIENT_FEED_PROCESSING_SERVICE +
                        "to Patient !");
            }
        } else {
            throw new PatientFeedProcessResponseException(RESPONSE_FROM_THE_DISTANT_PATIENT_FEED_PROCESSING_SERVICE_SHALL_CONTAIN_A_SINGLE +
                    "output for updated Patient !");
        }
    }


    /**
     * Extract PatientFeedException from ProcessResponse report.
     *
     * @param report : report found in the received response.
     * @return {@link PatientFeedException} to be thrown by the {@link PatientFeedClient}
     * @throws PatientFeedProcessResponseException : if the report is not well-formed.
     */
    private PatientFeedException createExceptionFromProcessResponseReport(TAR report) throws PatientFeedProcessResponseException {
        if (report.getReports() == null || report.getReports().getInfoOrWarningOrError().size() != 1) {
            throw new PatientFeedProcessResponseException("The report from the ProcessResponse shall not be null and shall contain a single error.");
        } else {
            try {
                BAR error = (BAR) report.getReports().getInfoOrWarningOrError().get(0).getValue();
                if (error.getDescription() != null && !error.getDescription().isEmpty()) {
                    return new PatientFeedException(error.getDescription());
                } else {
                    throw new PatientFeedProcessResponseException("Error from ProcessResponse report must have a valid description !");
                }
            } catch (ClassCastException e) {
                throw new PatientFeedProcessResponseException("Cannot decode error from ProcessResponse report !");
            }
        }
    }
}
